// ButtonGraph.cpp : implementation file
//

#include "stdafx.h"
#include "ButtonGraph.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CButtonGraph

CButtonGraph::CButtonGraph()
{
	m_fX = 0;
	m_fY = 0;
	m_nTimeBetweenSamples = 35;
	m_nType = 0;
	m_font.CreateFont(15, 0, 0, 0, FW_BOLD, FALSE, FALSE,0,0,0,0,0,0, "Arial");
}

CButtonGraph::~CButtonGraph()
{
}


BEGIN_MESSAGE_MAP(CButtonGraph, CButton)
	//{{AFX_MSG_MAP(CButtonGraph)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CButtonGraph message handlers

void CButtonGraph::OnPaint() 
{
	if(m_nType == PLOT_TYPE_GFORCE_CIRCLE) {
		PlotGForceCircle();
	}
	else if(m_nType == PLOT_TYPE_VOLTS) {
		PlotVoltageGraph();
	}

	// Do not call CButton::OnPaint() for painting messages
}

void CButtonGraph::Repaint()
{
	Invalidate();
}

void CButtonGraph::PlotGForceCircle()
{
	CRect rect;
	GetClientRect(&rect);
	int nWidth = rect.Width();
	int nHeight = rect.Height();
	int nX, nY;
	float fScale;

	CPaintDC dc(this); // device context for painting	
	dc.SetBkMode(TRANSPARENT);
	dc.SetViewportOrg(nWidth/2, nHeight/2);
	if(nWidth/2 > nHeight/2) {
		nWidth = nHeight;
	}
	else {
		nHeight = nWidth;
	}
	fScale = 0.65f*nHeight/2.0f/100.0f;	

	CPen PenWhite(PS_SOLID, 1, RGB(255, 255, 255));
	CPen PenBlack(PS_SOLID, 1, RGB(0, 0, 0));
	CPen PenGreen(PS_SOLID, 1, RGB(0, 135, 0));
	CPen PenGray(PS_SOLID, 1, RGB(240, 240, 240));
	CPen PenDarkGray(PS_SOLID, 1, RGB(190, 190, 190));
	CPen PenThick(PS_SOLID, 5, RGB(255, 0, 0));
    CBrush BrushWhite(RGB(255, 255, 255));

	dc.SelectObject(PenWhite);
    dc.SelectObject(BrushWhite);
    dc.Rectangle((int)(-160*fScale), (int)(-160*fScale), (int)(fScale*160), (int)(fScale*160));

    dc.SelectObject(PenBlack);
    dc.Rectangle((int)(-130*fScale), (int)(-130*fScale), (int)(fScale*130), (int)(fScale*130));

	dc.SelectObject(PenGreen);
	// A circle whose center is at the origin (0, 0)
	dc.Ellipse((int)(-100*fScale), (int)(-100*fScale), (int)(fScale*100), (int)(fScale*100));

	dc.SelectObject(PenGray);
	for(int y = (int)(-120*fScale); y < (int)(fScale*130); y+=(int)(fScale*10)) {
		dc.MoveTo((int)(-130*fScale), y);
		dc.LineTo((int)(fScale*130), y);	
		dc.MoveTo(y, (int)(-130*fScale));
		dc.LineTo(y, (int)(fScale*130));	
	}

	dc.SelectObject(PenDarkGray);
	for(int y = (int)(-100*fScale); y < (int)(fScale*110); y+=(int)(fScale*50)) {
		dc.MoveTo((int)(-130*fScale), y);
		dc.LineTo((int)(fScale*130), y);	
		dc.MoveTo(y, (int)(-130*fScale));
		dc.LineTo(y, (int)(fScale*130));	
	}

	dc.SelectObject(PenBlack);
	dc.MoveTo((int)(-130*fScale), 0);
	dc.LineTo((int)(fScale*130), 0);	
	dc.MoveTo(0, (int)(-130*fScale));
	dc.LineTo(0, (int)(fScale*130));	

	dc.SelectObject(&m_font);
	
	dc.SetTextColor(RGB(140, 140, 140));
	dc.TextOut((int)(-150*fScale), (int)(-110*fScale), "1");
	dc.TextOut((int)(-155*fScale), (int)(-60*fScale), "0.5");
	dc.TextOut((int)(-150*fScale), (int)(-10*fScale), "0");
	dc.TextOut((int)(-160*fScale), (int)(40*fScale), "-0.5");
	dc.TextOut((int)(-150*fScale), (int)(90*fScale), "-1");

	dc.TextOut((int)(-105*fScale), (int)(135*fScale), "-1");
	dc.TextOut((int)(-60*fScale), (int)(135*fScale), "-0.5");
	dc.TextOut((int)(-2*fScale), (int)(135*fScale), "0");
	dc.TextOut((int)(40*fScale), (int)(135*fScale), "0.5");
	dc.TextOut((int)(98*fScale), (int)(135*fScale), "1");

	dc.SetTextColor(RGB(0, 0, 0));
	dc.TextOut((int)(-60*fScale), (int)(-155*fScale), "Acceleration (G)");

	// draw dot
	nX = (int)(m_fX*100*fScale);
	nY = (int)(-m_fY*100*fScale);

	if(nX <= 126*fScale && nY <= 126*fScale && nX >= -126*fScale && nY >= -126*fScale) {
		dc.SelectObject(PenThick);
		dc.Ellipse(nX-2, nY-2, nX+2, nY+2);
	}
}

void CButtonGraph::PlotVoltageGraph()
{
	const int PLOT_VOLTAGE_MAX = 16;
	CRect rect;
	GetClientRect(&rect);
	int nWidth = rect.Width();
	int nHeight = rect.Height();
	static int nX, nY;
	float fDotPerV;
	float fDotPerSec;
	CString sTemp;

	fDotPerSec = 1000.0f/m_nTimeBetweenSamples;
	fDotPerV = (nHeight-40)/(float)PLOT_VOLTAGE_MAX;

	CPaintDC dc(this); // device context for painting	
	dc.SetViewportOrg(20, nHeight-20);
	dc.SelectObject(&m_font);

	CPen PenWhite(PS_SOLID, 1, RGB(255, 255, 255));
	CPen PenBlack(PS_SOLID, 2, RGB(0, 0, 0));
	CPen PenGreen(PS_SOLID, 1, RGB(0, 135, 0));
	CPen PenGray(PS_SOLID, 1, RGB(235, 235, 235));
	CPen PenDarkGray(PS_DOT, 1, RGB(190, 190, 190));
	CPen PenThick(PS_SOLID, 5, RGB(255, 0, 0));
	CPen PenRed(PS_SOLID, 1, RGB(255, 0, 0));
    CBrush BrushWhite(RGB(255, 255, 255));
	CBrush BrushGray(RGB(240, 240, 240));

	dc.SelectObject(PenBlack);

	if(m_fX==0) {
		// main axes
		dc.SelectObject(PenWhite);
		dc.SelectObject(BrushWhite);
		dc.Rectangle(-20, -nHeight+20, nWidth-20, 20);
		dc.SelectObject(PenBlack);
		dc.Rectangle(0, -nHeight+40, nWidth-40, 0);
				
		dc.SetTextColor(RGB(140, 140, 140));
		// vertical grid
		dc.SelectObject(PenGray);
		for(int x = 1; fDotPerSec * x < nWidth-40; x++) { 
			dc.MoveTo((int)(fDotPerSec * x), 0);
			dc.LineTo((int)(fDotPerSec * x), (int)(-nHeight+40));
			sTemp.Format("%i", x);
			dc.TextOut((int)(fDotPerSec * x - 5), 5, sTemp);
		}

		dc.SelectObject(PenDarkGray);
		
		// horizontal grid
		for(int y = 2; y < PLOT_VOLTAGE_MAX; y+=2) { 
			dc.MoveTo(0, (int)(-y * fDotPerV));
			dc.LineTo(nWidth - 40, (int)(-y * fDotPerV));
			sTemp.Format("%i", y);
			if(y>=10) {
				dc.TextOut(-20, (int)(-y * fDotPerV)-10, sTemp);
			}
			else {
				dc.TextOut(-15, (int)(-y * fDotPerV)-10, sTemp);
			}
		}

		dc.SetTextColor(RGB(0, 0, 0));
		dc.TextOut(-20, -nHeight+20, "Volts (V)");
		dc.TextOut(nWidth-75, 5, "  Time (s)");
		
		nX = 0;
	}

	if(++m_fX >= (nWidth-40)) {
		m_fX = 0;
	}

	dc.SelectObject(PenRed);
	dc.MoveTo(nX,nY);
	nX = (int)m_fX;
	nY = (int)(-m_fY * fDotPerV);
    dc.LineTo(nX, nY);
}
