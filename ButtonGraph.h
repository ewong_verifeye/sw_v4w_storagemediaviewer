#if !defined(AFX_BUTTONGRAPH_H__59DA22E7_BD11_483B_95CB_3C994AB29DC3__INCLUDED_)
#define AFX_BUTTONGRAPH_H__59DA22E7_BD11_483B_95CB_3C994AB29DC3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ButtonGraph.h : header file
//

#define PLOT_TYPE_GFORCE_CIRCLE		0x01
#define PLOT_TYPE_VOLTS				0x02

/////////////////////////////////////////////////////////////////////////////
// CButtonGraph window

class CButtonGraph : public CButton
{
// Construction
public:
	CButtonGraph();

// Attributes
public:

// Operations
public:
	void Repaint();
	inline void SetTimeBetweenSamples(int nMS) { m_nTimeBetweenSamples = nMS; }
	inline void SetType(int nType) { m_nType = nType; }
	inline void SetX(float fX) { m_fX = fX; }
	inline void SetY(float fY) { m_fY = fY; }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CButtonGraph)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CButtonGraph();

	// Generated message map functions
protected:
	void PlotGForceCircle();
	void PlotVoltageGraph();

private:
	//{{AFX_MSG(CButtonGraph)
	afx_msg void OnPaint();
	//}}AFX_MSG
	CFont m_font;
	int m_nType;
	float m_fX;
	float m_fY;
	int m_nTimeBetweenSamples;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BUTTONGRAPH_H__59DA22E7_BD11_483B_95CB_3C994AB29DC3__INCLUDED_)
