#include "stdafx.h"
#include "resource.h"
#include "CFViewerDlg.h"
#include "EventSearchMgr.h"

EventSearchMgr::EventSearchMgr(void)
	: m_hThread( NULL )
	, m_hShutdownEvent( ::CreateEvent( NULL, TRUE, FALSE, NULL ) )
{
	m_bForward = true;
}


EventSearchMgr::~EventSearchMgr(void)
{
   ShutdownThread( );
   ::CloseHandle( m_hShutdownEvent );
}

void EventSearchMgr::Pause( )
{
   if( NULL != m_hThread )
   {
      ::SuspendThread( m_hThread );
   }
}

void EventSearchMgr::Resume( )
{
   if( NULL != m_hThread )
   {
      ::ResumeThread( m_hThread );
   }
}

HRESULT EventSearchMgr::Start( HWND hWnd )
{
   HRESULT hr = S_OK;

   m_hWnd = hWnd;

   if( SUCCEEDED( hr = ShutdownThread( ) ) )
   {
      hr = CreateThread( );
   }
   return hr;
}

HRESULT EventSearchMgr::Stop( )
{
   return ShutdownThread( );
   }

HRESULT EventSearchMgr::ShutdownThread( )
{
   HRESULT hr = S_OK;

   // Close the worker thread
   if( NULL != m_hThread )
   {
      // Signal the thread to exit
      ::SetEvent( m_hShutdownEvent );

      // thread may be suspended, so resume before shutting down
      ::ResumeThread( m_hThread );

      // Wait for the thread to exit. If it doesn't shut down
      // on its own, force it closed with Terminate thread
      if ( WAIT_TIMEOUT == WaitForSingleObject( m_hThread, 1000 ) )
      {
         ::TerminateThread( m_hThread, -1000 );
         hr = S_FALSE;
      }

      // Close the handle and NULL it out
      ::CloseHandle( m_hThread );
      m_hThread = NULL;

   }

   // Reset the shutdown event
   ::ResetEvent( m_hShutdownEvent );

   return hr;
}

// Creates the secondary display thread
HRESULT EventSearchMgr::CreateThread( )
{
   // Fire off the thread
   if( NULL == (m_hThread = (HANDLE)_beginthreadex(
      NULL,
      0,
      ThreadProc,
      static_cast<LPVOID>( this ),
      0,
      NULL) ) )
   {
      return HRESULT_FROM_WIN32( ::GetLastError( ) );
   }

   return S_OK;
}

HANDLE& EventSearchMgr::GetShutdownEvent( )
{
   return m_hShutdownEvent;
}

void EventSearchMgr::NotifyUI( UINT uNotificationType )
{
   // Check if the hWnd is still valid before posting the message
   // Note: use PostMessage instead of SendMessage because
   // PostMessage performs an asynchronous post; whereas
   // SendMessage sends synchronously
   if( ::IsWindow( m_hWnd ) )
   {
      switch( uNotificationType )
      {
      case NOTIFY_INC_PROGRESS:
         ::PostMessage( m_hWnd, WM_USER_INC_PROGRESS, 0, 0 );
         break;
      case NOTIFY_THREAD_COMPLETED:
         ::PostMessage( m_hWnd, WM_USER_THREAD_COMPLETED, 0, 0 );
         break;
      default:
         ASSERT( 0 );
      }
   }
}

static UINT WINAPI ThreadProc( LPVOID lpContext )
{
   EventSearchMgr* pSearchMgr
      = reinterpret_cast< EventSearchMgr* >( lpContext );

   CCFViewerDlg* pCFViewer = (CCFViewerDlg*)pSearchMgr->m_pCFViewerDlg;

   // Loop to simulate a worker operation
   // for( UINT uCount = 0; uCount < 100; uCount++ )

	while (1)
	{
		if ( pCFViewer->SearchEvents(pSearchMgr->m_bForward) )
		{
			TRACE("Searching Completed\n");
			// Send the thread completed message to the UI
			pSearchMgr->NotifyUI( NOTIFY_THREAD_COMPLETED );
			return 1;
		}
		// If reached beginning or end of drive loop around
		// pCFViewer->HandleWrap();
		// This is handled in the SearchHeader() function

		// Check if the shutdown event has been set,
		// if so exit the thread
		if( WAIT_OBJECT_0 ==
			WaitForSingleObject( pSearchMgr->GetShutdownEvent( ), 0 ) )
		{
			return 1;
		}

		// Send the progress message to the UI
		pSearchMgr->NotifyUI( NOTIFY_INC_PROGRESS );

		// Sample code - delay the operation a bit
		Sleep( 75 );
	}

	// Send the thread completed message to the UI
	pSearchMgr->NotifyUI( NOTIFY_THREAD_COMPLETED );

	return 0;
}

/*
UINT CCFViewerDlg::SearchEventsFunction(LPVOID pParam)
{
	// CMyDlg* p_Dlg = static_cast<CMyDlg*>(pParam);
	bool forward = static_cast<bool*>(pParam);
	if (SearchEvents(forward)) {
		return 1; 
	}
	return 0;
}
bool CCFViewerDlg::SearchEvents(bool forward)
{
	bool bFoundHeader = false;
	while( !bFoundHeader ) {	// or user exits
		bFoundHeader = SearchHeader(forward, true);
	}
	if (bFoundHeader) {
		while(1) {
			CTime t = GetEntryTime();
			LVFINDINFO fi;
			fi.flags = LVFI_PARAM;
			fi.lParam = m_sector;
			if(m_events.FindItem(&fi, -1) == -1) {
				LVITEM item;
				item.mask = LVIF_PARAM;
				item.iItem = m_events.GetItemCount();
				item.iSubItem = 0;
				item.lParam = m_sector;
				int index = m_events.InsertItem(&item);

				item.mask = LVIF_TEXT;
				item.iItem = index;
				item.iSubItem = 0;
				item.pszText = m_pEntry->_data.event.complete ? "" : "*";
				m_events.SetItem(&item);

				CString str;
				item.mask = LVIF_TEXT;
				item.iSubItem = 1;
				str.Format("%d", GetArea(m_sector));
				item.pszText = (LPTSTR)(LPCTSTR)str;
				m_events.SetItem(&item);

				item.mask = LVIF_TEXT;
				item.iSubItem = 2;
				str.Format("0x%.08X", m_sector);
				item.pszText = (LPTSTR)(LPCTSTR)str;
				m_events.SetItem(&item);

				item.mask = LVIF_TEXT;
				item.iSubItem = 3;
				str.Format("%s.%.02d", t.Format(_T("%Y-%m-%d %H:%M:%S")), m_pEntry->header.time.hundredths);
				item.pszText = (LPTSTR)(LPCTSTR)str;
				m_events.SetItem(&item);

				item.mask = LVIF_TEXT;
				item.iSubItem = 4;
				str = m_pHeader->source == V4W_EVENT_BACKGROUND ? "Background" : EVENTS[m_pHeader->source];
				item.pszText = (LPTSTR)(LPCTSTR)str;
				m_events.SetItem(&item);
			}
			else {
				// we have looped around
				return true;
			}

			if(forward) {
				if(m_pHeader->next == m_sector) {
					AfxMessageBox("Error:  Next points to self.");
					return true;
				}
				m_sector = m_pHeader->next;
			}
			else {
				if(m_pHeader->prev == 0x00000000) {
					return true;
				}
				else {
					if(m_pHeader->prev == m_sector) {
						AfxMessageBox("Error:  Prev points to self.");
						return true;
					}
					m_sector = m_pHeader->prev;
				}
			}

			if(!ReadHeader()) {
				return true;
			}

			if(m_pHeader->type != V4W_FILESYS_ENTRY_TYPE_EVENT) {
				SetStatus("Error: event linked to non-event");
				return true;
			}
		}
	}
	else {
		return false;
	}
}



*/