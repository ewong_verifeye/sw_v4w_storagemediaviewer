#pragma once
// Based on parts from: http://www.ucancode.net/Visual_C_MFC_Example/WaitForSingleObject-_beginthreadex-SetEvent-Start-Stop-Thread-VC-MFC-Example.htm

// Determines which type of user defined message to post
enum	NotificationTypes { NOTIFY_INC_PROGRESS, NOTIFY_THREAD_COMPLETED };

class EventSearchMgr
{
public:
	EventSearchMgr(void);
	~EventSearchMgr(void);

	UINT_PTR m_pCFViewerDlg;
	bool m_bForward;				// search in forward direction

	void Pause( );
	void Resume( );
	HRESULT Start( HWND hWnd );
	HRESULT Stop( );
	HANDLE& GetShutdownEvent( );
	void NotifyUI( UINT uNotificationType );
	HRESULT ShutdownThread( );

private:
	HWND   m_hWnd;		// Window handle to the UI dialog (used to
						// send progress and completed messages)
	HANDLE m_hThread;			// Secondary thread handle
	HANDLE m_hShutdownEvent;	// Shutdown Event handle
								// (causes thread to exit)
	
	HRESULT CreateThread( );
};

static UINT WINAPI ThreadProc( LPVOID lpContext );