// ExtractDialog.cpp : implementation file
//

#include "stdafx.h"
#include "cfviewer.h"
#include "ExtractDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtractDialog dialog


CExtractDialog::CExtractDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CExtractDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CExtractDialog)
	m_folders = FALSE;
	m_type = 0;
	m_filter = FALSE;
	//}}AFX_DATA_INIT
}


void CExtractDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExtractDialog)
	DDX_Check(pDX, IDC_FOLDERS, m_folders);
	DDX_Radio(pDX, IDC_TYPE1, m_type);
	DDX_Check(pDX, IDC_FILTER, m_filter);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CExtractDialog, CDialog)
	//{{AFX_MSG_MAP(CExtractDialog)
	ON_BN_CLICKED(IDC_TYPE1, OnType1)
	ON_BN_CLICKED(IDC_TYPE2, OnType2)
	ON_BN_CLICKED(IDC_TYPE3, OnType3)
	ON_BN_CLICKED(IDC_TYPE4, OnType4)
	ON_BN_CLICKED(IDC_TYPE5, OnType5)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtractDialog message handlers

void CExtractDialog::OnType1() 
{
	GetDlgItem(IDC_FOLDERS)->EnableWindow();
	GetDlgItem(IDC_FILTER)->EnableWindow(FALSE);
}

void CExtractDialog::OnType2() 
{
	GetDlgItem(IDC_FOLDERS)->EnableWindow(FALSE);
	GetDlgItem(IDC_FILTER)->EnableWindow(FALSE);
}

void CExtractDialog::OnType3() 
{
	GetDlgItem(IDC_FOLDERS)->EnableWindow(FALSE);
	GetDlgItem(IDC_FILTER)->EnableWindow();
}

void CExtractDialog::OnType4() 
{
	GetDlgItem(IDC_FOLDERS)->EnableWindow(FALSE);
	GetDlgItem(IDC_FILTER)->EnableWindow(FALSE);
}

void CExtractDialog::OnType5() 
{
	GetDlgItem(IDC_FOLDERS)->EnableWindow(FALSE);
	GetDlgItem(IDC_FILTER)->EnableWindow(FALSE);
}
