#if !defined(AFX_EXTRACTDIALOG_H__96E6173F_4D77_4CBE_9857_6D13C525AA7A__INCLUDED_)
#define AFX_EXTRACTDIALOG_H__96E6173F_4D77_4CBE_9857_6D13C525AA7A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExtractDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CExtractDialog dialog

class CExtractDialog : public CDialog
{
// Construction
public:
	CExtractDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CExtractDialog)
	enum { IDD = IDD_EXTRACT_DIALOG };
	BOOL	m_folders;
	int		m_type;
	BOOL	m_filter;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExtractDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CExtractDialog)
	afx_msg void OnType1();
	afx_msg void OnType2();
	afx_msg void OnType3();
	afx_msg void OnType4();
	afx_msg void OnType5();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXTRACTDIALOG_H__96E6173F_4D77_4CBE_9857_6D13C525AA7A__INCLUDED_)
