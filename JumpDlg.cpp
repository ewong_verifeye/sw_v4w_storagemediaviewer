// JumpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "cfviewer.h"
#include "JumpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJumpDlg dialog


CJumpDlg::CJumpDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CJumpDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CJumpDlg)
	m_address = _T("");
	//}}AFX_DATA_INIT
}


void CJumpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CJumpDlg)
	DDX_Text(pDX, IDC_ADDRESS, m_address);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CJumpDlg, CDialog)
	//{{AFX_MSG_MAP(CJumpDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJumpDlg message handlers

BOOL CJumpDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_ADDRESS);
	pEdit->SetSel(0, -1);
	pEdit->SetFocus();
	
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
