#if !defined(AFX_JUMPDLG_H__318F0EF7_114D_4B61_9481_C6C291D4B106__INCLUDED_)
#define AFX_JUMPDLG_H__318F0EF7_114D_4B61_9481_C6C291D4B106__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// JumpDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CJumpDlg dialog

class CJumpDlg : public CDialog
{
// Construction
public:
	CJumpDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CJumpDlg)
	enum { IDD = IDD_JUMP_DIALOG };
	CString	m_address;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJumpDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CJumpDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_JUMPDLG_H__318F0EF7_114D_4B61_9481_C6C291D4B106__INCLUDED_)
