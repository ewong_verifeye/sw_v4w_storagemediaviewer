========================================================================
       MICROSOFT FOUNDATION CLASS LIBRARY : cfviewer
========================================================================


AppWizard has created this cfviewer application for you.  This application
not only demonstrates the basics of using the Microsoft Foundation classes
but is also a starting point for writing your application.

This file contains a summary of what you will find in each of the files that
make up your cfviewer application.

cfviewer.dsp
    This file (the project file) contains information at the project level and
    is used to build a single project or subproject. Other users can share the
    project (.dsp) file, but they should export the makefiles locally.

cfviewer.h
    This is the main header file for the application.  It includes other
    project specific headers (including Resource.h) and declares the
    CCFViewerApp application class.

cfviewer.cpp
    This is the main application source file that contains the application
    class CCFViewerApp.

cfviewer.rc
    This is a listing of all of the Microsoft Windows resources that the
    program uses.  It includes the icons, bitmaps, and cursors that are stored
    in the RES subdirectory.  This file can be directly edited in Microsoft
	Visual C++.

cfviewer.clw
    This file contains information used by ClassWizard to edit existing
    classes or add new classes.  ClassWizard also uses this file to store
    information needed to create and edit message maps and dialog data
    maps and to create prototype member functions.

res\cfviewer.ico
    This is an icon file, which is used as the application's icon.  This
    icon is included by the main resource file cfviewer.rc.

res\cfviewer.rc2
    This file contains resources that are not edited by Microsoft 
	Visual C++.  You should place all resources not editable by
	the resource editor in this file.




/////////////////////////////////////////////////////////////////////////////

AppWizard creates one dialog class:

cfviewerDlg.h, cfviewerDlg.cpp - the dialog
    These files contain your CCFViewerDlg class.  This class defines
    the behavior of your application's main dialog.  The dialog's
    template is in cfviewer.rc, which can be edited in Microsoft
	Visual C++.


/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named cfviewer.pch and a precompiled types file named StdAfx.obj.

Resource.h
    This is the standard header file, which defines new resource IDs.
    Microsoft Visual C++ reads and updates this file.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

If your application uses MFC in a shared DLL, and your application is 
in a language other than the operating system's current language, you
will need to copy the corresponding localized resources MFC42XXX.DLL
from the Microsoft Visual C++ CD-ROM onto the system or system32 directory,
and rename it to be MFCLOC.DLL.  ("XXX" stands for the language abbreviation.
For example, MFC42DEU.DLL contains resources translated to German.)  If you
don't do this, some of the UI elements of your application will remain in the
language of the operating system.

/////////////////////////////////////////////////////////////////////////////



2008 Feb 26: v4.5 EDW
- this version bundles the DLL with the app so that it can be  HASP enveloped
  as a single application.  The WaveletStatic class supports both old and new
  parameters.

2008 Apr 24: v4.5.1
- Display images in the correct area with the new camera notation, where the 
  higher nibble indicates the sub-port and the lower nibble indicates the main
  port.
- Adjusted the audio frequency being displayed to the correct value.
- Added power events to the event list.

2008 Apr 28: v4.5.2
- Revised all event labels

2008 Jul 31: v5.0.0
- added G-force plot
- added extract audio feature with low-pass filter capability
- added extract G-force feature
- added status for max G-force in each direction and max/min audio samples
- added slider bar to scroll through images
- edited "Jump To" feature to give a warning if address specified is beyond max LBA
- edited "Prev" to not pass address 0 and "Next" to not pass max LBA if max LBA is valid
- edited image searching to allow for searching 16MB at a time

2008 Sept 12: v5.0.3
- added support for color images and appropriate aspect ratio