// ReaderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "cfviewer.h"
#include "ReaderDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CReaderDlg dialog


CReaderDlg::CReaderDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CReaderDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReaderDlg)
	//}}AFX_DATA_INIT
}


void CReaderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReaderDlg)
	DDX_Control(pDX, IDC_DEVICE, m_device);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReaderDlg, CDialog)
	//{{AFX_MSG_MAP(CReaderDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CReaderDlg message handlers

POSITION CReaderDlg::AddString(LPCTSTR lpszItem)
{
	return m_list.AddTail(lpszItem);
}

void CReaderDlg::ResetContent()
{
	m_list.RemoveAll();
}

BOOL CReaderDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	POSITION p = m_list.GetHeadPosition();
	while(p) {
		CString& s = m_list.GetNext(p);
		int index = m_device.AddString(s);
		if(s.Find("usb_flash_card_reader#ata") != -1 || s.Find("cardreader_cf") != -1 || s.Find("cf_reader") != -1) {
			m_device.SetCurSel(index);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CReaderDlg::OnOK() 
{
	int i;
	if((i = m_device.GetCurSel()) != LB_ERR) {
		m_device.GetText(i, m_sel);
	}
	else {
		m_sel.Empty();
	}
	
	CDialog::OnOK();
}
