#if !defined(AFX_READERDLG_H__15DC8FDF_62C4_43EE_8A9C_5001437D71BB__INCLUDED_)
#define AFX_READERDLG_H__15DC8FDF_62C4_43EE_8A9C_5001437D71BB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReaderDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CReaderDlg dialog

class CReaderDlg : public CDialog
{
// Construction
public:
	CReaderDlg(CWnd* pParent = NULL);   // standard constructor
	POSITION AddString(LPCTSTR lpszItem);
	void ResetContent();

// Dialog Data
	//{{AFX_DATA(CReaderDlg)
	enum { IDD = IDD_READER_DIALOG };
	CListBox	m_device;
	//}}AFX_DATA
	CString m_sel;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReaderDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CStringList m_list;

	// Generated message map functions
	//{{AFX_MSG(CReaderDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_READERDLG_H__15DC8FDF_62C4_43EE_8A9C_5001437D71BB__INCLUDED_)
