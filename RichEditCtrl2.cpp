// RichEditCtrl2.cpp : implementation file
//

#include "stdafx.h"
#include "cfviewer.h"
#include "RichEditCtrl2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRichEditCtrl2

CRichEditCtrl2::CRichEditCtrl2()
{
}

CRichEditCtrl2::~CRichEditCtrl2()
{
}


BEGIN_MESSAGE_MAP(CRichEditCtrl2, CRichEditCtrl)
	//{{AFX_MSG_MAP(CRichEditCtrl2)
	ON_WM_LBUTTONDBLCLK()
/*	MPA 2011/01/05: 
		As of VS2005 OnNcHitTest returns LRESULT instead of UINT.
		Commenting out ON_WM_NCHITTEST() allows for clicking through offsets
		in SectorDialog with Windows7 (which uses the newer AFXMSG code)
		- Effects on XP unknown 
*/
//	ON_WM_NCHITTEST()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRichEditCtrl2 message handlers

void CRichEditCtrl2::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// do not call CRichEditCtrl::OnLButtonDblClk() to disable
	// double/triple-click selection
}

UINT CRichEditCtrl2::OnNcHitTest(CPoint point) 
{
	// set focus if user moves mouse over rich edit control so
	// parent receives WM_MOUSEMOVE notification messages
	UINT result = CRichEditCtrl::OnNcHitTest(point);
	if(result == HTCLIENT) {
		SetFocus();
	}

	return result;
}
