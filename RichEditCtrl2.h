#if !defined(AFX_RICHEDITCTRL2_H__5D30928D_446D_4CA8_906F_605272DCFF10__INCLUDED_)
#define AFX_RICHEDITCTRL2_H__5D30928D_446D_4CA8_906F_605272DCFF10__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RichEditCtrl2.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRichEditCtrl2 window

class CRichEditCtrl2 : public CRichEditCtrl
{
// Construction
public:
	CRichEditCtrl2();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRichEditCtrl2)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRichEditCtrl2();

	// Generated message map functions
protected:
	//{{AFX_MSG(CRichEditCtrl2)
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg UINT OnNcHitTest(CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RICHEDITCTRL2_H__5D30928D_446D_4CA8_906F_605272DCFF10__INCLUDED_)
