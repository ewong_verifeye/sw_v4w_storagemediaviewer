// SearchDlg.cpp : implementation file
//

#include "stdafx.h"
#include "cfviewer.h"
#include "CFViewerDlg.h"
#include "SearchDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSearchDlg dialog


CSearchDlg::CSearchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSearchDlg::IDD, pParent)
	, m_ThreadState( TS_STOPPED )
{
	//{{AFX_DATA_INIT(CSearchDlg)
	m_direction = 0;
	m_bFirstSearch = true;
	//}}AFX_DATA_INIT
}

CSearchDlg::~CSearchDlg(void)
{
	m_pEventSearchMgr.ShutdownThread();
	m_ThreadState = TS_STOPPED;
}


void CSearchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchDlg)
	DDX_Radio(pDX, IDC_DIRECTION1, m_direction);
	//}}AFX_DATA_MAP
	//  DDX_Control(pDX, IDOK, m_btnSearch);
	DDX_Control(pDX, IDSEARCH_STARTSTOP, m_btnStartStop);
	DDX_Control(pDX, IDC_PROGRESS_SEARCH, m_ctrlProgress);
}


BEGIN_MESSAGE_MAP(CSearchDlg, CDialog)
	//{{AFX_MSG_MAP(CSearchDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDSEARCH_STARTSTOP, &CSearchDlg::OnBnClickedStartstop)
	ON_MESSAGE( WM_USER_INC_PROGRESS, OnIncProgress )
	ON_MESSAGE( WM_USER_THREAD_COMPLETED,
			OnThreadCompleted )

//			ON_COMMAND(IDCLOSE, &CSearchDlg::OnIdclose)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSearchDlg message handlers

// Message received from the worker thread to increment
// the progress control
LRESULT CSearchDlg::OnIncProgress( WPARAM, LPARAM )
{
   // Increment the progress control
   m_ctrlProgress.StepIt( );
   return 1;
}

// Message received from the worker thread.
// Indicates the thread has completed
LRESULT CSearchDlg::OnThreadCompleted( WPARAM, LPARAM )
{
	// Reset the start button text and state
	m_bFirstSearch = false;
	m_ThreadState = TS_STOPPED;
	m_btnStartStop.SetWindowText( _T("Search Again") );
	m_ctrlProgress.SetPos( 100 );
	GetDlgItem(IDC_DIRECTION1)->EnableWindow(TRUE);
	GetDlgItem(IDC_DIRECTION2)->EnableWindow(TRUE);

	// Must set sector forward (or backwards) by 1 so that new searches can occur.

	return 1;
}

void CSearchDlg::OnBnClickedStartstop()
{
	UpdateData(TRUE);
	m_pEventSearchMgr.m_bForward = (m_direction == 0);

	m_btnStartStop.EnableWindow( FALSE );

	switch( ToggleStartStopState( ) )
	{
	case TS_START:
		GetDlgItem(IDC_DIRECTION1)->EnableWindow(FALSE);
		GetDlgItem(IDC_DIRECTION2)->EnableWindow(FALSE);
		// Start the thread
		m_pEventSearchMgr.m_pCFViewerDlg = m_pCFViewerDlg;
		m_pEventSearchMgr.Start( GetSafeHwnd( ) );
		break;
	case TS_PAUSE:
		GetDlgItem(IDC_DIRECTION1)->EnableWindow(TRUE);
		GetDlgItem(IDC_DIRECTION2)->EnableWindow(TRUE);
		// Pause the thread
		m_pEventSearchMgr.Pause( );
		break;
	case TS_RESUME:
		GetDlgItem(IDC_DIRECTION1)->EnableWindow(FALSE);
		GetDlgItem(IDC_DIRECTION2)->EnableWindow(FALSE);
		// Resume the thread
		m_pEventSearchMgr.Resume( );
		break;
	default:
		ASSERT( 0 );    // We shouldn't reach this
	}
	
	m_btnStartStop.EnableWindow( TRUE );
}

// Toggles the Start/Pause/Resume state and sets the button text
INT CSearchDlg::ToggleStartStopState( )
{
	if( !m_bFirstSearch && m_ThreadState == TS_STOPPED) {
		if (m_pEventSearchMgr.m_bForward) {
			((CCFViewerDlg*)m_pEventSearchMgr.m_pCFViewerDlg)->IncrementSector();
		}
		else {
			((CCFViewerDlg*)m_pEventSearchMgr.m_pCFViewerDlg)->DecrementSector();
		}
	}

	if( TS_RESUME == m_ThreadState )
	{
		m_ThreadState = TS_START;
	}

	m_ThreadState++;

	CString sButtonText = _T("");

	switch( m_ThreadState )
	{
	case TS_START:
		sButtonText = _T("Pause");
		break;
	case TS_PAUSE:
		sButtonText = _T("Search");	// "Resume"
		break;
	case TS_RESUME:
		sButtonText = _T("Pause");
		break;
	default:
		ASSERT( 0 );    // We shouldn't reach this
	}

	// Set button text
	m_btnStartStop.SetWindowText( sButtonText );

	return m_ThreadState;
}

// Reset the start/pause button and state
void CSearchDlg::ResetStartStopState( )
{
   m_ThreadState = TS_STOPPED;
   m_btnStartStop.SetWindowText( _T("Search") );	// "Start"
   m_ctrlProgress.SetStep( 0 );
}

// Reset the search dialog for future use
bool CSearchDlg::Reset( )
{
	if (m_pEventSearchMgr.ShutdownThread() == S_OK) {
		m_ThreadState = TS_STOPPED;
		return true;
	}
	else {
		m_ThreadState = TS_STOPPED;
		return false;
	}
	GetDlgItem(IDC_DIRECTION1)->EnableWindow(TRUE);
	GetDlgItem(IDC_DIRECTION2)->EnableWindow(TRUE);
}