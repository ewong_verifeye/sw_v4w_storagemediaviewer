#include "afxwin.h"
#include "afxcmn.h"
#if !defined(AFX_SEARCHDLG_H__6B06DF80_3E1B_4204_B84E_6A01F56F5191__INCLUDED_)
#define AFX_SEARCHDLG_H__6B06DF80_3E1B_4204_B84E_6A01F56F5191__INCLUDED_

#include "EventSearchMgr.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SearchDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSearchDlg dialog

class CSearchDlg : public CDialog
{
// Construction
public:
	CSearchDlg(CWnd* pParent = NULL);   // standard constructor
	~CSearchDlg(void);

	afx_msg LRESULT OnIncProgress( WPARAM, LPARAM );
	afx_msg LRESULT OnThreadCompleted( WPARAM, LPARAM );

	EventSearchMgr m_pEventSearchMgr;

// Dialog Data
	//{{AFX_DATA(CSearchDlg)
	enum { IDD = IDD_SEARCH_DIALOG };
	int		m_direction;
	bool	m_bFirstSearch;

	UINT_PTR	m_pCFViewerDlg;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSearchDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	enum	ThreadStatus { TS_STOPPED, TS_START, TS_PAUSE, TS_RESUME };
	INT		m_ThreadState;

	INT ToggleStartStopState( );
	void ResetStartStopState( );
	// Generated message map functions
	//{{AFX_MSG(CSearchDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CButton m_btnStartStop;
	CProgressCtrl m_ctrlProgress;
	afx_msg void OnBnClickedStartstop();
	bool Reset();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEARCHDLG_H__6B06DF80_3E1B_4204_B84E_6A01F56F5191__INCLUDED_)
