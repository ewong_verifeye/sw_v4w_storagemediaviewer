// SectorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "cfviewer.h"
#include "SectorDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SWAP_WORD(x)				((((x) & 0xFF) << 8) | (((x) >> 8) & 0xFF))
#define SWAP_DWORD(x)				((SWAP_WORD((x)) << 16) | SWAP_WORD((x) >> 16))

const char HEX_DIGITS[] = "0123456789ABCDEF";

#define HEADER_FIRST			0
	#define HEADER_PREV			0
	#define HEADER_NEXT			1
	#define HEADER_SIZE			2
	#define HEADER_TIME			3
	#define HEADER_GPS			4
	#define HEADER_TYPE			5
	#define HEADER_SOURCE		6
	#define HEADER_TRIGGER		7
	#define HEADER_ESN			8
	#define HEADER_UNIT_ID		9
	#define HEADER_RTC_STATUS	10
	#define HEADER_IMG_NUM		11
	#define HEADER_TRIG_STATE	12
	#define HEADER_GPS_STATUS	13
	#define HEADER_IMG_SKIPPED	14
#define HEADER_LAST				14

#define EVENT_FIRST				15
	#define EVENT_PREV_DATA		15
	#define EVENT_NEXT_DATA		16
	#define EVENT_FIRST_TIME	17
	#define EVENT_LAST_TIME		18
	#define EVENT_NUM			19
	#define EVENT_TOT_IMG		20
#define EVENT_LAST				20

#define AUDIO1_FIRST			21
	#define AUDIO1_SEQ1			21
	#define AUDIO1_SEQ2			22
	#define AUDIO1_SEQ3			23
	#define AUDIO1_SYNC1		24
	#define AUDIO1_SYNC2		25
	#define AUDIO1_SYNC3		26
#define AUDIO1_LAST				26

#define AUDIO2_FIRST			27
	#define AUDIO2_SEQ4			27
	#define AUDIO2_SEQ5			28
	#define AUDIO2_SEQ6			29
	#define AUDIO2_SEQ7			30
	#define AUDIO2_SYNC4		31
	#define AUDIO2_SYNC5		32
	#define AUDIO2_SYNC6		33
	#define AUDIO2_SYNC7		34
#define AUDIO2_LAST				34

#define GFORCE_FIRST			35
	#define GFORCE_SEQ1			35
	#define GFORCE_SEQ2			36
	#define GFORCE_SEQ3			37
	#define GFORCE_SEQ4			38
	#define GFORCE_SEQ5			39
	#define GFORCE_SEQ6			40
	#define GFORCE_SYNC1		41
	#define GFORCE_SYNC2		42
	#define GFORCE_SYNC3		43
	#define GFORCE_SYNC4		44
	#define GFORCE_SYNC5		45
	#define GFORCE_SYNC6		46
#define GFORCE_LAST				46

#define MARKER_FIRST			47
	#define MARKER_COMPLETE		47
	#define MARKER_PREV_MARKER	48
	#define MARKER_NEXT_MARKER	49
	#define MARKER_FIRST_TIME	50
	#define MARKER_LAST_TIME	51
#define MARKER_LAST				51


CHARRANGE FORMAT_RANGES[] = {
	{ 3, 14 },			// HEADER_PREV
	{ 15, 26 },			// HEADER_NEXT
	{ 27, 38 },			// HEADER_SIZE
	{ 39, 59 },			// HEADER_TIME
	{ 60, 107 },		// HEADER_GPS
	{ 108, 110 },		// HEADER_TYPE
	{ 111, 113 },		// HEADER_SOURCE
	{ 114, 116 },		// HEADER_TRIGGER
	{ 117, 134 },		// HEADER_ESN
	{ 135, 209 },		// HEADER_UNIT_ID
	{ 306, 308 },		// HEADER_RTC_STATUS
	{ 309, 314 },		// HEADER_IMG_NUM
	{ 315, 332 },		// HEADER_TRIG_STATE
	{ 333, 347 },		// HEADER_GPS_STATUS
	{ 348, 350 },		// HEADER_IMG_SKIPPED
	{ 387, 398 },		// EVENT_PREV_DATA
	{ 399, 410 },		// EVENT_NEXT_DATA
	{ 411, 431 },		// EVENT_FIRST_TIME
	{ 432, 452 },		// EVENT_LAST_TIME
	{ 465, 470 },		// EVENT_NUM
	{ 471, 476 },		// EVENT_TOT_IMG
	{ 384, 386 },		// AUDIO1_SEQ1
	{ 768, 770 },		// AUDIO1_SEQ2
	{ 1152, 1154 },		// AUDIO1_SEQ3
	{ 0, 2 },			// AUDIO2_SEQ4
	{ 384, 386 },		// AUDIO2_SEQ5
	{ 768, 770 },		// AUDIO2_SEQ6
	{ 1152, 1154 },		// AUDIO2_SEQ7
	{ 765, 767 },		// AUDIO1_SYNC1
	{ 1149, 1151 },		// AUDIO1_SYNC2
	{ 1533, 1535 },		// AUDIO1_SYNC3
	{ 381, 383 },		// AUDIO2_SYNC4
	{ 765, 767 },		// AUDIO2_SYNC5
	{ 1149, 1151 },		// AUDIO2_SYNC6
	{ 1533, 1535 },		// AUDIO2_SYNC7
	{ 384, 386 },		// GFORCE_SEQ1
	{ 576, 578 },		// GFORCE_SEQ2
	{ 768, 770 },		// GFORCE_SEQ3
	{ 960, 962 },		// GFORCE_SEQ4
	{ 1152, 1154 },		// GFORCE_SEQ5
	{ 1344, 1346 },		// GFORCE_SEQ6
	{ 573, 575 },		// GFORCE_SYNC1
	{ 765, 767 },		// GFORCE_SYNC2
	{ 957, 959 },		// GFORCE_SYNC3
	{ 1149, 1151 },		// GFORCE_SYNC4
	{ 1341, 1343 },		// GFORCE_SYNC5
	{ 1533, 1535 },		// GFORCE_SYNC6
	{ 384, 386 },		// MARKER_COMPLETE
	{ 387, 398 },		// MARKER_PREV_MARKER
	{ 399, 410 },		// MARKER_NEXT_MARKER
	{ 411, 431 },		// MARKER_FIRST_TIME
	{ 432, 452 },		// MARKER_LAST_TIME
};

/////////////////////////////////////////////////////////////////////////////
// CSectorDlg dialog

CSectorDlg::CSectorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSectorDlg::IDD, pParent)
	, m_statusMsg(_T(""))
	, m_statusMsg2(_T(""))
	, m_sectorInfo(_T(""))
{
	//{{AFX_DATA_INIT(CSectorDlg)
	m_lba = _T("");
	m_maxLBA = 0xFFFFFFFF;
	//}}AFX_DATA_INIT
	m_font.CreateFont(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_SWISS, "Courier");
	m_pHeader = (V4W_FILESYS_HEADER*)m_data;
	m_pEntry = (V4W_FILESYS_ENTRY*)m_data;
}

void CSectorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSectorDlg)
	DDX_Control(pDX, IDC_TEXT, m_text);
	DDX_Text(pDX, IDC_LBA, m_lba);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_SECTOR_STATUS, m_statusMsg);
	DDX_Text(pDX, IDC_SECTOR_STATUS2, m_statusMsg2);
	DDX_Text(pDX, IDC_SECTOR_INFO, m_sectorInfo);
}

BEGIN_MESSAGE_MAP(CSectorDlg, CDialog)
	//{{AFX_MSG_MAP(CSectorDlg)
	ON_BN_CLICKED(IDC_PREV, OnPrev)
	ON_BN_CLICKED(IDC_NEXT, OnNext)
	ON_BN_CLICKED(IDC_GOTO, OnGoto)
	ON_BN_CLICKED(IDC_LAST, OnLast)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSectorDlg message handlers

BOOL CSectorDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_bmPrev = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_PREV));
	((CWnd*)GetDlgItem(IDC_PREV))->SendMessage(BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)m_bmPrev);
	m_bmNext = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_NEXT));
	((CWnd*)GetDlgItem(IDC_NEXT))->SendMessage(BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)m_bmNext);

	m_text.SetBackgroundColor(FALSE, GetSysColor(COLOR_MENU));
	m_text.SetFont(&m_font);
	m_text.SendMessage(EM_SETEVENTMASK, 0, ENM_LINK);

	m_toolTip.Create(this, TTS_NOPREFIX);
	memset(&m_ti, 0, sizeof(TOOLINFO));
	m_ti.cbSize = sizeof(TOOLINFO);
	m_ti.uFlags = TTF_IDISHWND | TTF_TRACK | TTF_ABSOLUTE;
	m_ti.hwnd = GetSafeHwnd();
	m_ti.uId = (UINT)m_text.GetSafeHwnd();
	m_ti.lpszText = _T("Default text.");
	m_toolTip.SendMessage(TTM_ADDTOOL, 0, (LPARAM)&m_ti);

	DisplaySector();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSectorDlg::OnPrev() 
{
	m_last = m_sector;
//	m_sector--;
	m_sector = (m_sector > 0) ? m_sector-1 : (m_maxLBA);	// Max LBA represents the highest valid LBA, not the number of sectors

	DisplaySector();
	GetDlgItem(IDC_LAST)->EnableWindow(TRUE);
}

void CSectorDlg::OnNext() 
{
	m_last = m_sector;
//	m_sector++;
	m_sector = (m_sector+1) % (m_maxLBA + 1);	// Max LBA represents the highest valid LBA, not the number of sectors
	DisplaySector();
	GetDlgItem(IDC_LAST)->EnableWindow(TRUE);
}

void CSectorDlg::OnGoto() 
{
	UpdateData();

	m_last = m_sector;
	m_sector = strtoul(m_lba, NULL, 0);
	DisplaySector();
	GetDlgItem(IDC_LAST)->EnableWindow(TRUE);
}

void CSectorDlg::OnLast() 
{
	m_sector = m_last;
	DisplaySector();
	GetDlgItem(IDC_LAST)->EnableWindow(FALSE);
}

void CSectorDlg::DisplaySector()
{
	m_lba.Format(_T("0x%.08X"), m_sector);
	DWORD sector = m_sector-m_offset;

	LARGE_INTEGER i;
	i.QuadPart = (__int64)sector*IDE_SECTOR_SIZE;
	DWORD result = SetFilePointer(m_hFile, i.LowPart, &i.HighPart, FILE_BEGIN);
	if(result == 0xFFFFFFFF && GetLastError() != NO_ERROR) {
		return;
	}

	DWORD bytesRead;
	if(ReadFile(m_hFile, m_data, IDE_SECTOR_SIZE, &bytesRead, NULL) &&
		bytesRead == IDE_SECTOR_SIZE) {
		FormatText();
		UpdateSectorInfo();
	}
	else {
		LPVOID msg;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_IGNORE_INSERTS |
			FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), 0, (LPTSTR)&msg, 0, NULL);
		AfxMessageBox((LPCTSTR)msg);
		LocalFree(msg);
		return;
	}

	UpdateData(FALSE);
}

void CSectorDlg::FormatText()
{
	bool bValidChecksum = true;
	CString str;
	for(int i=0;i<IDE_SECTOR_SIZE;i++) {
		str += HEX_DIGITS[m_data[i] >> 4];
		str += HEX_DIGITS[m_data[i] & 0x0F];
		if((i % 16) == 15) {
			str += '\r';
		}
		else {
			str += ' ';
		}
	}
	m_text.SetWindowText(str);

	CHARFORMAT2 cf;
	cf.cbSize = sizeof(CHARFORMAT2);

	// set all text to default format first
	m_text.SendMessage(EM_GETCHARFORMAT, SCF_DEFAULT, (LPARAM)&cf);
	m_text.SendMessage(EM_SETCHARFORMAT, SCF_ALL, (LPARAM)&cf);

	if(m_pHeader->magic == V4W_FILESYS_HEADER_MAGIC &&
		m_pHeader->checksum != CalcChecksum(m_data, sizeof(V4W_FILESYS_HEADER)-1)) {
			// CHECKSUM MISMATCH due to one of two reasons:
			// 1) Magic character is coincidentally in the correct position
			// 2) Header is corrupt so checksums do not match
			// For sake of viewing corrupt headers more easily, format the sector anyways

			bValidChecksum = false;
//			m_statusMsg = "Header Checksum Error";
//			UpdateData(FALSE);
	}

	if(m_pHeader->magic == V4W_FILESYS_HEADER_MAGIC) {
		cf.dwMask = CFM_LINK;
		cf.dwEffects = CFE_LINK;
		cf.dwReserved = 0;

		// mark common fields
		for(int i=HEADER_FIRST;i<=HEADER_LAST;i++) {
			m_text.SetSel(FORMAT_RANGES[i]);
			m_text.SendMessage(EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&cf);
		}

		if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_EVENT) {
			// mark event fields
			for(int i=EVENT_FIRST;i<=EVENT_LAST;i++) {
				m_text.SetSel(FORMAT_RANGES[i]);
				m_text.SendMessage(EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&cf);
			}
			// m_statusMsg += "- EVENT";
		}
		else if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_MARKER) {
			// mark marker fields
			for(int i=MARKER_FIRST;i<=MARKER_LAST;i++) {
				m_text.SetSel(FORMAT_RANGES[i]);
				m_text.SendMessage(EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&cf);
			}
		}
		else if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_AUDIO) {
			cf.dwMask = CFM_COLOR;
			cf.dwEffects = 0;
			cf.crTextColor = RGB(255, 102, 0);

			// mark audio fields
			for(int i=AUDIO1_FIRST;i<=AUDIO1_LAST;i++) {
				m_text.SetSel(FORMAT_RANGES[i]);
				m_text.SendMessage(EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&cf);
			}

			m_lastAudio = m_sector;
		}
		else if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_GFORCE) {
			cf.dwMask = CFM_COLOR;
			cf.dwEffects = 0;
			cf.crTextColor = RGB(255, 102, 0);

			// mark G-force fields
			for(int i=GFORCE_FIRST;i<=GFORCE_LAST;i++) {
				m_text.SetSel(FORMAT_RANGES[i]);
				m_text.SendMessage(EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&cf);
			}
		}

/*		switch(m_pEntry->header.type) {
			case V4W_FILESYS_ENTRY_TYPE_IMAGE:
				str = _T("Image");
				break;
			case V4W_FILESYS_ENTRY_TYPE_AUDIO:
				str = _T("Audio (1/2)");
				break;
			case V4W_FILESYS_ENTRY_TYPE_GFORCE:
				str = _T("G-Force");
				break;
			case V4W_FILESYS_ENTRY_TYPE_EVENT:
				str = _T("Event");
				break;
			case V4W_FILESYS_ENTRY_TYPE_MARKER:
				str = _T("Marker");
				break;
			default:
				str = _T("<Unknown>");
				break;
		}
		m_statusMsg = str;
		if (!bValidChecksum) {
			m_statusMsg += " - Invalid Checksum";
		}
		UpdateData(FALSE);
		*/
	}
	else if(m_sector == m_lastAudio + 1) {
		cf.dwMask = CFM_COLOR;
		cf.dwEffects = 0;
		cf.crTextColor = RGB(255, 102, 0);

		// mark audio fields
		for(int i=AUDIO2_FIRST;i<=AUDIO2_LAST;i++) {
			m_text.SetSel(FORMAT_RANGES[i]);
			m_text.SendMessage(EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&cf);
		}

//		m_statusMsg = "Audio (2/2)";
//		UpdateData(FALSE);
	}
/*	else {
		m_statusMsg = "Data";
		UpdateData(FALSE);
	}
*/
	// clear selection
	m_text.SetSel(0, 0);
}

BYTE CSectorDlg::CalcChecksum(BYTE* buffer, int length)
{
	BYTE checksum = 0;
	while(length-- > 0) {
		checksum += *buffer++;
	}

	return checksum;
}

BOOL CSectorDlg::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	if(wParam == IDC_TEXT) {
		ENLINK* pEnlink = (ENLINK*)lParam;
		if(pEnlink->msg == WM_LBUTTONDOWN) {
			if(IsPointer(pEnlink->chrg)) {
				m_text.SetSel(pEnlink->chrg);
				CString s = m_text.GetSelText();
				s.Remove(_T(' '));
				DWORD d = strtoul(s, NULL, 16);
				m_last = m_sector;
				m_sector = SWAP_DWORD(d);
				DisplaySector();
				GetDlgItem(IDC_LAST)->EnableWindow(TRUE);
			}
			return 1;
		}
		else if(pEnlink->msg == WM_MOUSEMOVE) {
//			UpdateToolTipText(pEnlink->chrg);
/*			CPoint p;
			p.x = GET_X_LPARAM(pEnlink->lParam);
			p.y = GET_Y_LPARAM(pEnlink->lParam);
			p.Offset(15, 15);
			m_text.ClientToScreen(&p);
			m_toolTip.SendMessage(TTM_ACTIVATE, (WPARAM)TRUE, 0);
			m_toolTip.SendMessage(TTM_TRACKACTIVATE, (WPARAM)TRUE, (LPARAM)&m_ti);
			m_toolTip.SendMessage(TTM_TRACKPOSITION, 0, (LPARAM)MAKELONG(p.x, p.y));
			SetTimer(1, 1000, NULL);
*/		}
	}
	return true;

	BOOL bVal = CDialog::OnNotify(wParam, lParam, pResult);
	return bVal;
}

bool CSectorDlg::IsPointer(CHARRANGE chrg)
{
	if((chrg.cpMin == FORMAT_RANGES[HEADER_PREV].cpMin && chrg.cpMax == FORMAT_RANGES[HEADER_PREV].cpMax) ||
		(chrg.cpMin == FORMAT_RANGES[HEADER_NEXT].cpMin && chrg.cpMax == FORMAT_RANGES[HEADER_NEXT].cpMax)) {
		return true;
	}
	if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_EVENT &&
		((chrg.cpMin == FORMAT_RANGES[EVENT_PREV_DATA].cpMin && chrg.cpMax == FORMAT_RANGES[EVENT_PREV_DATA].cpMax) ||
		(chrg.cpMin == FORMAT_RANGES[EVENT_NEXT_DATA].cpMin && chrg.cpMax == FORMAT_RANGES[EVENT_NEXT_DATA].cpMax))) {
		return true;
	}
	if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_MARKER &&
		((chrg.cpMin == FORMAT_RANGES[MARKER_PREV_MARKER].cpMin && chrg.cpMax == FORMAT_RANGES[MARKER_PREV_MARKER].cpMax) ||
		(chrg.cpMin == FORMAT_RANGES[MARKER_NEXT_MARKER].cpMin && chrg.cpMax == FORMAT_RANGES[MARKER_NEXT_MARKER].cpMax))) {
		return true;
	}

	return false;
}

void CSectorDlg::UpdateToolTipText(CHARRANGE& cr)
{
	int field = -1;

	// check common fields
	for(int i=HEADER_FIRST;i<=HEADER_LAST;i++) {
		if(cr.cpMin == FORMAT_RANGES[i].cpMin) {
			field = i;
			break;
		}
	}

	if(field == -1 && m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_EVENT) {
		// check event fields
		for(int i=EVENT_FIRST;i<=EVENT_LAST;i++) {
			if(cr.cpMin == FORMAT_RANGES[i].cpMin) {
				field = i;
				break;
			}
		}
	}
	else if(field == -1 && m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_MARKER) {
		// check marker fields
		for(int i=MARKER_FIRST;i<=MARKER_LAST;i++) {
			if(cr.cpMin == FORMAT_RANGES[i].cpMin) {
				field = i;
				break;
			}
		}
	}

	CString str;
	CTime t;
	switch(field) {
	case HEADER_PREV:
		str.Format(_T("Previous: 0x%.08X"), m_pEntry->header.prev);
		break;
	case HEADER_NEXT:
		str.Format(_T("Next: 0x%.08X"), m_pEntry->header.next);
		break;
	case HEADER_SIZE:
		str.Format(_T("Data length: %d"), m_pEntry->header.size);
		break;
	case HEADER_TIME:
		t = ConvertTime(m_pEntry->header.time);
		str.Format(_T("Time: %s.%02d %s"), t.Format(_T("%a %b %d %H:%M:%S")), m_pEntry->header.time.hundredths, t.Format(_T("%Y")));
		break;
	case HEADER_GPS:
		str.Format(_T("GPS: %s"), ConvertGPS(m_pEntry->header.gps));
		break;
	case HEADER_TYPE:
		switch(m_pEntry->header.type) {
		case V4W_FILESYS_ENTRY_TYPE_IMAGE:
			str = _T("Type: Image");
			break;
		case V4W_FILESYS_ENTRY_TYPE_AUDIO:
			str = _T("Type: Audio");
			break;
		case V4W_FILESYS_ENTRY_TYPE_GFORCE:
			str = _T("Type: G-Force");
			break;
		case V4W_FILESYS_ENTRY_TYPE_EVENT:
			str = _T("Type: Event");
			break;
		case V4W_FILESYS_ENTRY_TYPE_MARKER:
			str = _T("Type: Marker");
			break;
		default:
			str = _T("Type: <unknown>");
			break;
		}
		break;
	case HEADER_SOURCE:
		str.Format(_T("Source: %d"), m_pEntry->header.source);
		break;
	case HEADER_TRIGGER:
		str.Format(_T("Trigger: 0x%.02X"), m_pEntry->header.trigger);
		break;
	case HEADER_ESN:
		str.Format(_T("ESN: %.02d.%.02d.%.02d.%.06d"), m_pEntry->header.esn[0], m_pEntry->header.esn[1], m_pEntry->header.esn[2],
			m_pEntry->header.esn[3]*10000+m_pEntry->header.esn[4]*100+m_pEntry->header.esn[5]);
		break;
	case HEADER_UNIT_ID:
		str.Format(_T("Unit ID: %s"), m_pEntry->header.unit_id);
		break;
	case HEADER_RTC_STATUS:
		str.Format(_T("RTC error: %s, Time sync within 24hrs: %s"), (m_pEntry->header.rtc_status & V4W_FILESYS_RTC_STATUS_ERROR_BIT)? "Y":"N", 
			(m_pEntry->header.rtc_status & V4W_FILESYS_RTC_STATUS_SYNC_24HRS_BIT)? "Y":"N");
		break;
	case HEADER_IMG_NUM:
		str.Format(_T("Image number: %i"), m_pEntry->header.img_num);
		break;
	case HEADER_TRIG_STATE:
		str.Format(_T("Trig1: %c, Trig2: %c, Trig3: %c, Trig4: %c, Trig5: %c, Trig6: %c"),
			GetStateChar(m_pEntry->header.trig_state[0]), GetStateChar(m_pEntry->header.trig_state[1]), GetStateChar(m_pEntry->header.trig_state[2]),
			GetStateChar(m_pEntry->header.trig_state[3]), GetStateChar(m_pEntry->header.trig_state[4]), GetStateChar(m_pEntry->header.trig_state[5]));
		break;
	case HEADER_GPS_STATUS:
		str.Format(_T("GPS - satellites: %i, HDOP: %.4f"), m_pEntry->header.gps_status.num_sat, m_pEntry->header.gps_status.HDOP);
		break;
	case HEADER_IMG_SKIPPED:
		str.Format(_T("Image skipped: %s"), (m_pEntry->header.image_skipped)? "Y":"N");
		break;
	case EVENT_PREV_DATA:
		str.Format(_T("Previous data: 0x%.08X"), m_pEntry->_data.event.prev_data);
		break;
	case EVENT_NEXT_DATA:
		str.Format(_T("Next data: 0x%.08X"), m_pEntry->_data.event.next_data);
		break;
	case EVENT_FIRST_TIME:
		t = ConvertTime(m_pEntry->_data.event.first);
		str.Format(_T("First entry: %s.%02d %s"), t.Format(_T("%a %b %d %H:%M:%S")), m_pEntry->_data.event.first.hundredths, t.Format(_T("%Y")));
		break;
	case EVENT_LAST_TIME:
		t = ConvertTime(m_pEntry->_data.event.last);
		str.Format(_T("Last entry: %s.%02d %s"), t.Format(_T("%a %b %d %H:%M:%S")), m_pEntry->_data.event.last.hundredths, t.Format(_T("%Y")));
		break;
	case EVENT_NUM:
		str.Format(_T("Event number: %i"), m_pEntry->_data.event.event_num);
		break;
	case EVENT_TOT_IMG:
		str.Format(_T("Images in event: %i"), m_pEntry->_data.event.total_img);
		break;
	case MARKER_COMPLETE:
		str.Format(_T("Complete: %s"), (m_pEntry->_data.marker.complete)? "Y":"N");
		break;
	case MARKER_PREV_MARKER:
		str.Format(_T("Previous marker: 0x%.08X"), m_pEntry->_data.marker.prev_marker);
		break;
	case MARKER_NEXT_MARKER:
		str.Format(_T("Next marker: 0x%.08X"), m_pEntry->_data.marker.next_marker);
		break;
	case MARKER_FIRST_TIME:
		t = ConvertTime(m_pEntry->_data.marker.first);
		str.Format(_T("First entry: %s.%02d %s"), t.Format(_T("%a %b %d %H:%M:%S")), m_pEntry->_data.marker.first.hundredths, t.Format(_T("%Y")));
		break;
	case MARKER_LAST_TIME:
		t = ConvertTime(m_pEntry->_data.marker.last);
		str.Format(_T("Last entry: %s.%02d %s"), t.Format(_T("%a %b %d %H:%M:%S")), m_pEntry->_data.marker.last.hundredths, t.Format(_T("%Y")));
		break;
	default:
		break;
	}

	m_toolTip.UpdateTipText(str, &m_text, 0);
//	m_statusMsg2 = str;
	UpdateData(FALSE);
}

void CSectorDlg::UpdateSectorInfo()
{
	CString strInfo, str;
	CTime t;
	bool bValidCheckSum = true;
	BYTE byCheckSum = CalcChecksum(m_data, sizeof(V4W_FILESYS_HEADER)-1);

	if(m_pHeader->magic == V4W_FILESYS_HEADER_MAGIC) {
		if (m_pHeader->checksum != byCheckSum) {
			// CHECKSUM MISMATCH due to one of two reasons:
			// 1) Magic character is coincidentally in the correct position
			// 2) Header is corrupt so checksums do not match
			bValidCheckSum = false;
			str.Format(_T("HEADER CHECKSUM FAILURE (0x%02X != 0x%02X) - "), m_pHeader->checksum, byCheckSum );
			strInfo += str;
		}
		switch(m_pEntry->header.type) {
		case V4W_FILESYS_ENTRY_TYPE_IMAGE:
			str = _T("Image");
			strInfo += str + "\n\n";
			break;
		case V4W_FILESYS_ENTRY_TYPE_AUDIO:
			str = _T("Audio (1/2)");
			strInfo += str + "\n\n";
			break;
		case V4W_FILESYS_ENTRY_TYPE_GFORCE:
			str = _T("G-Force");
			strInfo += str + "\n\n";
			break;
		case V4W_FILESYS_ENTRY_TYPE_EVENT:
			str = _T("Event");
			strInfo += str + "\n\n";

			// Event Specific Info
			str.Format(_T("Previous data: 0x%.08X"), m_pEntry->_data.event.prev_data);
			strInfo += str + "\n";

			str.Format(_T("Next data: 0x%.08X"), m_pEntry->_data.event.next_data);
			strInfo += str + "\n";

			t = ConvertTime(m_pEntry->_data.event.first);
			if (t != CTime()) {
				str.Format(_T("First entry: %s.%02d %s"), t.Format(_T("%a %b %d %H:%M:%S")), m_pEntry->_data.event.first.hundredths, t.Format(_T("%Y")));
			}
			else {
				str = _T("First entry: INVALID");
			}
			strInfo += str + "\n";

			t = ConvertTime(m_pEntry->_data.event.last);
			if (t != CTime()) {
				str.Format(_T("Last entry: %s.%02d %s"), t.Format(_T("%a %b %d %H:%M:%S")), m_pEntry->_data.event.last.hundredths, t.Format(_T("%Y")));
			}
			else {
				str = _T("Last entry: INVALID");
			}
			strInfo += str + "\n";

			str.Format(_T("Event number: %i"), m_pEntry->_data.event.event_num);
			strInfo += str + "\n";

			str.Format(_T("Images in event: %i"), m_pEntry->_data.event.total_img);
			strInfo += str + "\n\n";
			break;
		case V4W_FILESYS_ENTRY_TYPE_MARKER:
			str = _T("Marker");
			strInfo += str + "\n\n";

			// Marker Specific Info
			str.Format(_T("Complete: %s"), (m_pEntry->_data.marker.complete)? "Y":"N");
			strInfo += str + "\n";

			str.Format(_T("Previous marker: 0x%.08X"), m_pEntry->_data.marker.prev_marker);
			strInfo += str + "\n";

			str.Format(_T("Next marker: 0x%.08X"), m_pEntry->_data.marker.next_marker);
			strInfo += str + "\n";

			t = ConvertTime(m_pEntry->_data.marker.first);
			if (t != CTime()) {
				str.Format(_T("First entry: %s.%02d %s"), t.Format(_T("%a %b %d %H:%M:%S")), m_pEntry->_data.marker.first.hundredths, t.Format(_T("%Y")));
			}
			else {
				str = _T("First entry: INVALID");
			}
			strInfo += str + "\n";

			t = ConvertTime(m_pEntry->_data.marker.last);
			if (t != CTime()) {
				str.Format(_T("Last entry: %s.%02d %s"), t.Format(_T("%a %b %d %H:%M:%S")), m_pEntry->_data.marker.last.hundredths, t.Format(_T("%Y")));
			}
			else {
				str = _T("Last entry: INVALID");
			}
			strInfo += str + "\n\n";
			break;
		default:
			str = _T("<UNKNOWN>");
			strInfo += str + "\n\n";
			break;
		}
		

		// Pointers & Info for this event "type"
		str.Format(_T("Previous: 0x%.08X\n"), m_pEntry->header.prev);
		strInfo += str;

		str.Format(_T("Next: 0x%.08X\n"), m_pEntry->header.next);
		strInfo += str;

		str.Format(_T("Data length: %d"), m_pEntry->header.size);
		strInfo += str + "\n\n";

		// Time & Location
		t = ConvertTime(m_pEntry->header.time);
		if (t != CTime()) {
			str.Format(_T("Time: %s.%02d %s"), t.Format(_T("%a %b %d %H:%M:%S")), m_pEntry->header.time.hundredths, t.Format(_T("%Y")));
		}
		else {
			str = _T("Time: INVALID");
		}
		strInfo += str + "\n";

		str.Format(_T("GPS: %s"), ConvertGPS(m_pEntry->header.gps));
		strInfo += str + "\n\n";

		// Triggers
		str.Format(_T("Source: %d"), m_pEntry->header.source);
		strInfo += str + "\n";

		str.Format(_T("Trigger: 0x%.02X"), m_pEntry->header.trigger);
		strInfo += str + "\n";

		str.Format(_T("Trig1: %c, Trig2: %c, Trig3: %c, Trig4: %c, Trig5: %c, Trig6: %c"),
			GetStateChar(m_pEntry->header.trig_state[0]), GetStateChar(m_pEntry->header.trig_state[1]), GetStateChar(m_pEntry->header.trig_state[2]),
			GetStateChar(m_pEntry->header.trig_state[3]), GetStateChar(m_pEntry->header.trig_state[4]), GetStateChar(m_pEntry->header.trig_state[5]));
		strInfo += str + "\n\n";

		// Unit Info
		str.Format(_T("ESN: %.02d.%.02d.%.02d.%.06d"), m_pEntry->header.esn[0], m_pEntry->header.esn[1], m_pEntry->header.esn[2],
			m_pEntry->header.esn[3]*10000+m_pEntry->header.esn[4]*100+m_pEntry->header.esn[5]);
		strInfo += str + "\n";

		str.Format(_T("Unit ID: %s"), m_pEntry->header.unit_id);
		strInfo += str + "\n\n";

		// Unit Status
		str.Format(_T("RTC error: %s, Time sync within 24hrs: %s"), (m_pEntry->header.rtc_status & V4W_FILESYS_RTC_STATUS_ERROR_BIT)? "Y":"N", 
			(m_pEntry->header.rtc_status & V4W_FILESYS_RTC_STATUS_SYNC_24HRS_BIT)? "Y":"N");
		strInfo += str + "\n";

		str.Format(_T("GPS - satellites: %i, HDOP: %.4f"), m_pEntry->header.gps_status.num_sat, m_pEntry->header.gps_status.HDOP);
		strInfo += str + "\n";

		// Image Info
		if (m_pEntry->header.type ==V4W_FILESYS_ENTRY_TYPE_IMAGE) {
			str.Format(_T("Image number: %i"), m_pEntry->header.img_num);
			strInfo += str + "\n";

			str.Format(_T("Image skipped: %s"), (m_pEntry->header.image_skipped)? "Y":"N");
			strInfo += str + "\n";
		}
	}
	else if(m_sector == m_lastAudio + 1) {
		// Audio (part 2 of 2)
		strInfo = "Audio (2/2)";
	}
	else {
		strInfo = "Data";
	}
	m_sectorInfo = strInfo;
	UpdateData(FALSE);
}

CTime CSectorDlg::ConvertTime(const V4W_TIME& time)
{
	SYSTEMTIME st;
	st.wYear = 2000+time.year;
	st.wMonth = time.month;
	st.wDay = time.date;
	st.wHour = time.hours;
	st.wMinute = time.minutes;
	st.wSecond = time.seconds;
	st.wMilliseconds = 0;

	// 17 Feb 2007: EDW
	// Fix for GPS Issue: Old firmware in the Nemerix GPS from Taiwan
	// Examine the UTC Timestamp and apply the correction if the Day field is zero.

	if ( st.wDay == 0  &&  st.wYear >= 2000 && st.wMonth > 0 && st.wMonth <= 12 )
	{
		// Set Day to 1 using the current month
		CTime timeStamp = CTime( st.wYear, st.wMonth, 1, st.wHour,0,0);

		// Subtract 1 day: 24 hours
		CTimeSpan ts = CTimeSpan(1,0,0,0); 

		timeStamp -= ts;
		st.wYear = timeStamp.GetYear();
		st.wMonth = timeStamp.GetMonth();
		st.wDay = timeStamp.GetDay();
		st.wHour = timeStamp.GetHour();
	}

	FILETIME ft1;
	SystemTimeToFileTime(&st, &ft1);
	FILETIME ft2;
	FileTimeToLocalFileTime(&ft1, &ft2);
	FileTimeToSystemTime(&ft2, &st);

	// Ensure time is valid, otherwise ASSERTs in CTime will fail
	if ( st.wYear >= 1900 
		&& st.wMonth >= 1 && st.wMonth <= 12 
		&& st.wDay >= 1 && st.wDay <= 31 
		&& st.wHour >= 0 && st.wHour <= 23 
		&& st.wMinute >= 0 && st.wMinute <= 59 
		&& st.wSecond >= 0 && st.wSecond <= 59 ) {
		return CTime(st);
	}
	else {
		return CTime();
	}
}

CString CSectorDlg::ConvertGPS(const V4W_GPS& gps)
{
	struct {
		int deg;
		int min;
		float sec;
		char hem;
	} lat, lon;

	lat.hem = (gps.latitude >= 0) ? 'N' : 'S';
	float latitude = (float)fabs(gps.latitude);
	latitude /= 100.0;
	lat.deg = (int)latitude;
	latitude -= lat.deg;
	latitude *= 60;
	lat.min = (int)latitude;
	latitude -= lat.min;
	latitude *= 60;
	lat.sec = latitude;

	lon.hem = (gps.longitude >= 0) ? 'E' : 'W';
	float longitude = (float)fabs(gps.longitude);
	longitude /= 100.0;
	lon.deg = (int)longitude;
	longitude -= lon.deg;
	longitude *= 60;
	lon.min = (int)longitude;
	longitude -= lon.min;
	longitude *= 60;
	lon.sec = longitude;

	float kph = (float)(gps.speed*1.852);

	CString str;
	str.Format(_T("%d\xB0%.02d'%.2f\" %c, %d\xB0%.02d'%.2f\" %c, %.2f kph, %.2f\xB0"),
		lat.deg, lat.min, lat.sec, lat.hem, lon.deg, lon.min, lon.sec, lon.hem,
		kph, gps.heading);

	return str;
}

void CSectorDlg::OnTimer(UINT_PTR nIDEvent) 
{
	m_toolTip.SendMessage(TTM_TRACKACTIVATE, (WPARAM)FALSE, (LPARAM)&m_ti);
	m_toolTip.SendMessage(TTM_ACTIVATE, (WPARAM)FALSE, 0);
	KillTimer(nIDEvent);
}

char CSectorDlg::GetStateChar(int state)
{
	if(state == 2) 
		return 'H';
	else if(state == 1)
		return 'M';
	else if(state == 0)
		return 'L';
	else
		return '?';
}
