#if !defined(AFX_SECTORDLG_H__0E4A4211_2EE4_48B7_8416_46594D7F7C33__INCLUDED_)
#define AFX_SECTORDLG_H__0E4A4211_2EE4_48B7_8416_46594D7F7C33__INCLUDED_

#define IDE_SECTOR_SIZE		512

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "v4w.h"
#include "RichEditCtrl2.h"

// SectorDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSectorDlg dialog

class CSectorDlg : public CDialog
{
// Construction
public:
	CSectorDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSectorDlg)
	enum { IDD = IDD_SECTOR_DIALOG };
	CRichEditCtrl2	m_text;
	CString	m_lba;
	//}}AFX_DATA
	HANDLE m_hFile;
	DWORD m_sector;
	DWORD m_maxLBA;
	DWORD m_offset;
	DWORD m_last;
	CToolTipCtrl m_toolTip;
	TOOLINFO m_ti;

	static CTime ConvertTime(const V4W_TIME& time);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSectorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//}}AFX_VIRTUAL

// Implementation
protected:
	HBITMAP m_bmNext;
	HBITMAP m_bmPrev;
	CFont m_font;
	BYTE m_data[IDE_SECTOR_SIZE];
	V4W_FILESYS_HEADER* m_pHeader;
	V4W_FILESYS_ENTRY* m_pEntry;
	DWORD m_lastAudio;

	void DisplaySector();
	void FormatText();
	void UpdateToolTipText(CHARRANGE& cr);
	void UpdateSectorInfo();
	BYTE CalcChecksum(BYTE* buffer, int length);
	// CTime ConvertTime(const V4W_TIME& time);
	CString ConvertGPS(const V4W_GPS& gps);
	bool IsPointer(CHARRANGE chrg);
	char GetStateChar(int state);

	// Generated message map functions
	//{{AFX_MSG(CSectorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPrev();
	afx_msg void OnNext();
	afx_msg void OnGoto();
	afx_msg void OnLast();
	afx_msg void OnTimer(UINT_PTR nIDEvent);	// to work in x64 UINT must be UINT_PTR
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CString m_statusMsg;
	CString m_statusMsg2;
	CString m_sectorInfo;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SECTORDLG_H__0E4A4211_2EE4_48B7_8416_46594D7F7C33__INCLUDED_)
