// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__3334B920_85A2_43E5_84AF_D8201F08238D__INCLUDED_)
#define AFX_STDAFX_H__3334B920_85A2_43E5_84AF_D8201F08238D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#define WM_USER_INC_PROGRESS     WM_USER + 0x8001
#define WM_USER_THREAD_COMPLETED WM_USER + 0x8002

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <objbase.h>

#include <setupapi.h>
#include <winioctl.h>
#include <math.h>
#include <windowsx.h>

#include <process.h>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__3334B920_85A2_43E5_84AF_D8201F08238D__INCLUDED_)
