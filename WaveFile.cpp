// WaveFile.cpp: implementation of the CWaveFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "cfviewer.h"
#include "WaveFile.h"
#include "v4w.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWaveFile::CWaveFile()
{
	m_dwSampleRate = V4W_AUDIO_SAMPLE_RATE;
	m_wBitsPerSample = 16;
	m_nSamples = 0;
}

CWaveFile::~CWaveFile()
{

}

bool CWaveFile::Open(CString sFilePath)
{
	try {
		DWORD dwTemp;
		WORD wTemp;

		if(!m_file.Open((LPCTSTR)sFilePath, CFile::modeCreate | CFile::modeWrite)) {
			throw(0);
		}

		m_nSamples = 0;
	
		// ChunkID: "RIFF"
		dwTemp = 0x46464952;
		m_file.Write(&dwTemp, 4);
		// ChunkSize: 36 + SubChunk2Size
		dwTemp = 36;
		m_file.Write(&dwTemp, 4);
		// Format: "WAVE"
		dwTemp = 0x45564157;
		m_file.Write(&dwTemp, 4);
		// Subchunk1ID: "fmt "
		dwTemp = 0x20746d66;
		m_file.Write(&dwTemp, 4);
		// Subchunk1Size: 16 for PCM
		dwTemp = 16;
		m_file.Write(&dwTemp, 4);
		// AudioFormat: PCM = 1
		wTemp = 1;
		m_file.Write(&wTemp, 2);
		// NumChannels: Mono = 1
		wTemp = 1;
		m_file.Write(&wTemp, 2);
		// SampleRate: 8000, 44100, etc.
		dwTemp = m_dwSampleRate;
		m_file.Write(&dwTemp, 4);
		// ByteRate: SampleRate * NumChannels * BitsPerSample/8
		dwTemp = m_dwSampleRate * 1 * (m_wBitsPerSample/8);
		m_file.Write(&dwTemp, 4);
		// BlockAlign: NumChannels * BitsPerSample/8
		wTemp = 1 * (m_wBitsPerSample/8);
		m_file.Write(&wTemp, 2);
		// BitsPerSample: 8 bits = 8, 16 bits = 16
		wTemp = m_wBitsPerSample;
		m_file.Write(&wTemp, 2);
		// Subchunk2ID: "data"
		dwTemp = 0x61746164;
		m_file.Write(&dwTemp, 4);
		// Subchunk2Size: NumSamples * NumChannels * BitsPerSample/8
		dwTemp = m_nSamples * 1 * (m_wBitsPerSample/8);
		m_file.Write(&dwTemp, 4);

		return true;
	}

	catch(...) {
		return false;
	}
}

bool CWaveFile::AddSamples(BYTE *pSamples, int nLength)
{
	try {
		m_file.Write(pSamples, nLength);
		m_nSamples += nLength/sizeof(short);
		return true;
	}

	catch(...) {
		return false;
	}
}

bool CWaveFile::Close()
{
	try {
		DWORD dwTemp;

		// rewrite Subchunk2Size: NumSamples * NumChannels * BitsPerSample/8
		m_file.Seek(40, CFile::begin);
		dwTemp = m_nSamples * 1 * (m_wBitsPerSample/8);
		m_file.Write(&dwTemp, 4);
		// rewrite ChunkSize: 36 + SubChunk2Size
		dwTemp += 36;
		m_file.Seek(4, CFile::begin);
		m_file.Write(&dwTemp, 4);
		m_file.Close();
		return true;
	}
	
	catch(...) {
		return false;
	}
}