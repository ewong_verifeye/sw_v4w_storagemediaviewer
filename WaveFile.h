// WaveFile.h: interface for the CWaveFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WAVEFILE_H__63A8BDEC_C55A_41A0_B822_F92ACC02327D__INCLUDED_)
#define AFX_WAVEFILE_H__63A8BDEC_C55A_41A0_B822_F92ACC02327D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CWaveFile  
{
public:
	CWaveFile();
	virtual ~CWaveFile();

	bool Open(CString);
	bool AddSamples(BYTE *, int);
	bool Close();

	DWORD m_dwSampleRate;        
	WORD m_wBitsPerSample;
	CFile m_file;

private:
	DWORD m_nSamples;
};

#endif // !defined(AFX_WAVEFILE_H__63A8BDEC_C55A_41A0_B822_F92ACC02327D__INCLUDED_)
