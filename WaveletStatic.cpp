// WaveletStatic.cpp : implementation file
//

#include "stdafx.h"
#include <stdio.h>
#include <math.h>
#include "wavelet.h"
#include "WaveletStatic.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Conditions that distinguishes between image types to process correctly (ie ten wavelet blocks vs eleven)
#define WS_IMAGE_HALFRES		(m_oDataType == WS_TYPE_WAVELET2_YUV || (m_oDataType == WS_TYPE_WAVELET2_YUV_2 && m_width <= 360))

// Condition to check when image has color components
#define WS_IMAGE_COLOR			GetColorMode()

/////////////////////////////////////////////////////////////////////////////
// CWaveletStatic

CWaveletStatic::CWaveletStatic()
{
	m_width = 0;
	m_height = 0;

	m_bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	m_bmi.bmiHeader.biWidth = 0;
	m_bmi.bmiHeader.biHeight = 0;
	m_bmi.bmiHeader.biPlanes = 1;
	m_bmi.bmiHeader.biBitCount = 24;
	m_bmi.bmiHeader.biCompression = BI_RGB;
	m_bmi.bmiHeader.biSizeImage = 0;
	m_bmi.bmiHeader.biXPelsPerMeter = 0;
	m_bmi.bmiHeader.biYPelsPerMeter = 0;
	m_bmi.bmiHeader.biClrUsed = 0;
	m_bmi.bmiHeader.biClrImportant = 0;
	m_bmi.bmiColors[0].rgbRed = 0;
	m_bmi.bmiColors[0].rgbGreen = 0;
	m_bmi.bmiColors[0].rgbBlue = 0;
	m_bmi.bmiColors[0].rgbReserved = 0;

	m_oData = NULL;
	m_cData = NULL;
	m_uData = NULL;
	m_bData = NULL;
	m_DataYUV = NULL;
	m_DataY = NULL;
	m_DataU = NULL;
	m_DataV = NULL;

	m_quantTable = NULL;
	m_quantLevel = -1;
	m_displayMode = 0;
	m_displayYUV = 0;
	m_bColorMode = true;
	m_bDisplayTransform = false;
	m_bShowErrors = false;
	
	m_cBitLength = 0;

	// initialize color adjustment structure to defaults
	m_colorAdj.caSize = sizeof(m_colorAdj);
	m_colorAdj.caFlags = 0;
	m_colorAdj.caIlluminantIndex = ILLUMINANT_DEVICE_DEFAULT;
	m_colorAdj.caRedGamma = 10000;
	m_colorAdj.caGreenGamma = 10000;
	m_colorAdj.caBlueGamma = 10000;
	m_colorAdj.caReferenceBlack = 0;
	m_colorAdj.caReferenceWhite = 10000;
	m_colorAdj.caContrast = 0;
	m_colorAdj.caBrightness = 0;
	m_colorAdj.caColorfulness = 0;
	m_colorAdj.caRedGreenTint = 0;
}

CWaveletStatic::~CWaveletStatic()
{
	if (m_uData) {
		delete [] m_uData;
		m_uData = 0;
	}

	if (m_cData) {
		delete [] m_cData;
		m_cData = 0;
	}

	if (m_bData) {
		delete [] m_bData;
		m_bData = 0;
	}

	if (m_DataYUV) {
		delete [] m_DataYUV;
		m_DataYUV = 0;
	}
	if (m_DataY) {
		delete [] m_DataY;
		m_DataY = 0;
	}
	if (m_DataU) {
		delete [] m_DataU;
		m_DataU = 0;
	}
	if (m_DataV) {
		delete [] m_DataV;
		m_DataV = 0;
	}
}

BEGIN_MESSAGE_MAP(CWaveletStatic, CStatic)
	//{{AFX_MSG_MAP(CWaveletStatic)
	ON_WM_PAINT()
	ON_WM_WINDOWPOSCHANGING()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWaveletStatic message handlers

void CWaveletStatic::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	CRect r;
	GetClientRect(&r);

	int imageWidth;
	int imageHeight;
	CreateImage(imageWidth, imageHeight);
	if(imageWidth > 0 && imageHeight > 0) {
		int displayWidth = r.Width();
		int displayHeight = r.Height();

		CDC memDC;
		memDC.CreateCompatibleDC(&dc);

		CBitmap bitmap;
		bitmap.CreateCompatibleBitmap(&dc, displayWidth, displayHeight);
		CBitmap* pOldBitmap = (CBitmap*)memDC.SelectObject(&bitmap);
		
		m_bmi.bmiHeader.biWidth = imageWidth;
		m_bmi.bmiHeader.biHeight = -imageHeight;
		memDC.SetStretchBltMode(HALFTONE);
		memDC.SetColorAdjustment(&m_colorAdj);
		StretchDIBits(memDC, 0, 0, displayWidth, displayHeight, 0, 0, imageWidth, imageHeight,
			m_bData, &m_bmi, DIB_RGB_COLORS, SRCCOPY);
		dc.BitBlt(0, 0, displayWidth, displayHeight, &memDC, 0, 0, SRCCOPY);
		memDC.SelectObject(pOldBitmap);

		// MPA: Attempt to optimize by properly discarding objects
		memDC.DeleteDC();
		bitmap.DeleteObject();
	}
	else {
		dc.FillRect(&r, CBrush::FromHandle(GetSysColorBrush(COLOR_MENU)));
	}

	// Do not call CStatic::OnPaint() for painting messages
}

void CWaveletStatic::OnWindowPosChanging(WINDOWPOS FAR* lpwndpos) 
{
	CStatic::OnWindowPosChanging(lpwndpos);
	
	// MPA 2011-01-11: GetDC() causes GDI leak unless it is matched with ReleaseDC
	CRect r, r2;
	GetClientRect(&r);
	CDC* pDC = GetDC();
	if(lpwndpos->cx < r.Width()) {
		r2 = r;
		r2.left = lpwndpos->cx;
		pDC->FillRect(&r2, CBrush::FromHandle(GetSysColorBrush(COLOR_MENU)));
	}
	if(lpwndpos->cy < r.Height()) {
		r2 = r;
		r2.top = lpwndpos->cy;
		pDC->FillRect(&r2, CBrush::FromHandle(GetSysColorBrush(COLOR_MENU)));
	}

	ReleaseDC(pDC);		// Required to avoid GDI leaks
}

void CWaveletStatic::SetData(BYTE* data, int type, int length, int lengthU, int lengthV)
{
	m_oData = data;
	m_oDataType = type;
	m_oDataLength = length;
	m_oDataLengthU = lengthU;
	m_oDataLengthV = lengthV;

	// Most generic and failsafe way to determine if image has color is if we are using the U & V data
	if (lengthU + lengthV > 0) {
		SetColorMode(true);
	}
	else {
		SetColorMode(false);
	}
}

void CWaveletStatic::SetDisplayYUV(unsigned int yuv)
{
	if (yuv > 3) { yuv = 0; }
	m_displayYUV = yuv;
}

int CWaveletStatic::GetModeWidth()
{
	if(WS_IMAGE_HALFRES) {
		switch(m_displayMode) {
		case 0:
		case 4:
			return m_width;
		case 1:
		case 2:
		case 3:
			return (m_width >> (4-m_displayMode));
		default:
			return 0;
		}
	}
	else {
		switch(m_displayMode) {
		case 0:
		case 5:
			return m_width;
		case 1:
		case 2:
		case 3:
		case 4:
			return (m_width >> (5-m_displayMode));
		default:
			return 0;
		}
	}
}

int CWaveletStatic::GetModeHeight()
{
	switch(m_displayMode) {
	case 0:
	case 4:
	case 5:
		return m_height;
	case 1:
	case 2:
	case 3:
		return (m_height >> (4-m_displayMode));
	default:
		return 0;
	}
}

void CWaveletStatic::SetContrast(SHORT icontrast)
{
	m_colorAdj.caContrast = icontrast;
	Invalidate(FALSE);
}

void CWaveletStatic::SetBrightness(SHORT ibrightness)
{
	m_colorAdj.caBrightness = ibrightness;
	Invalidate(FALSE);
}

void CWaveletStatic::GetUncompressedData(short * &data)
{
	data = m_uData;
}

void CWaveletStatic::CreateImage(int& width, int& height)
{
	bool bSuccessY = false,bSuccessU = false, bSuccessV = false;

	if(!m_oData) {
		width = 0;
		height = 0;
		return;
	}

	width = GetModeWidth();
	height = GetModeHeight();
	if(width == 0 || height == 0) {
		return;
	}

	int size;
	if(m_displayMode == 0) {
		if(m_oDataType == WS_TYPE_RAW_Y) {
			if(m_uData) {
				delete [] m_uData;
			}
			size = m_width*m_height;
			m_uData = new short[size];
			for(int i=0;i<size;i++) {
				m_uData[i] = m_oData[i];
			}

			YUVToRGB(m_uData, width, height);
		}
		else {
			width = 0;
			height = 0;
			return;
		}
	}
	else if(m_displayMode >= 1 && m_displayMode <= 5)
	{
		if(m_oDataType == WS_TYPE_RAW_Y) {
			if(m_uData) {
				delete [] m_uData;
			}
			size = m_width*m_height;
			m_uData = new short[size];
			for(int i=0;i<size;i++) {
				m_uData[i] = m_oData[i];
			}

			if(m_cData) {
				delete [] m_cData;
			}
			int size = m_width*m_height*2;
			m_cData = new BYTE[size];
			memset(m_cData, 0, size);

			Compress(false);
			bSuccessY = Decompress(0, false);
			YUVToRGB(m_uData, width, height);
		}
		else if(m_oDataType == WS_TYPE_WAVELET_Y) {
			if(m_cData) {
				delete [] m_cData;
			}
			m_cData = new BYTE[m_oDataLength];
			memcpy(m_cData, m_oData, m_oDataLength);

			if(m_uData) {
				delete [] m_uData;
			}
			int size = m_width*m_height;
			m_uData = new short[size];
			memset(m_uData, 0, sizeof(short)*size);

			bSuccessY = Decompress(0, false);
			YUVToRGB(m_uData, width, height);
		}
		else if(m_oDataType == WS_TYPE_WAVELET2_Y) {
			if(m_cData) {
				delete [] m_cData;
			}
			m_cData = new BYTE[m_oDataLength];
			memcpy(m_cData, m_oData, m_oDataLength);

			if(m_uData) {
				delete [] m_uData;
			}
			int size = m_width*m_height;
			m_uData = new short[size];
			memset(m_uData, 0, sizeof(short)*size);

			bSuccessY = Decompress(0, true);
			YUVToRGB(m_uData, width, height);
		}
		else if(m_oDataType == WS_TYPE_RAW_YUV) {
			if(m_DataY) {
				delete [] m_DataY;
			}
			m_DataY = new short[m_oDataLength];
			memcpy(m_DataY, m_oData, m_oDataLength);

			if(m_DataU) {
				delete [] m_DataU;
			}
			m_DataU = new short[m_oDataLengthU];
			memcpy(m_DataU, m_oData + m_oDataLength, m_oDataLengthU);

			if(m_DataV) {
				delete [] m_DataV;
			}
			m_DataV = new short[m_oDataLengthV];
			memcpy(m_DataV, m_oData + m_oDataLength + m_oDataLengthU, m_oDataLengthV);

			Interpolate(true);
			YUVToRGB(m_DataYUV, width, height, true);
		}
		else if(m_oDataType == WS_TYPE_WAVELET2_YUV) {	
			//// Y ////
			if(m_cData) {
				delete [] m_cData;
			}
			m_cData = new BYTE[m_oDataLength];
			memcpy(m_cData, m_oData, m_oDataLength);

			if(m_uData) {
				delete [] m_uData;
			}
			int size = m_width*m_height;
			m_uData = new short[size];
			memset(m_uData, 0, sizeof(short)*size);
			bSuccessY = Decompress(0, true);
			
			if(m_DataY) {
				delete [] m_DataY;
			}
			m_DataY = new short[size];
			memcpy(m_DataY, m_uData, sizeof(short)*size);

			//// U ////
			if(m_cData) {
				delete [] m_cData;
			}
			m_cData = new BYTE[m_oDataLengthU];
			memcpy(m_cData, m_oData + m_oDataLength, m_oDataLengthU);

			if(m_uData) {
				delete [] m_uData;
			}
			size = m_width/2*m_height;
			m_uData = new short[size];
			memset(m_uData, 0, sizeof(short)*size);
			bSuccessU = Decompress(1, true);
			
			if(m_DataU) {
				delete [] m_DataU;
			}
			m_DataU = new short[size];
			memcpy(m_DataU, m_uData, sizeof(short)*size);
	
			//// V ////
			if(m_cData) {
				delete [] m_cData;
			}
			m_cData = new BYTE[m_oDataLengthV];
			memcpy(m_cData, m_oData + m_oDataLength + m_oDataLengthU, m_oDataLengthV);

			if(m_uData) {
				delete [] m_uData;
			}
			size = m_width/2*m_height;
			m_uData = new short[size];
			memset(m_uData, 0, sizeof(short)*size);
			bSuccessV = Decompress(2, true);
			
			if(m_DataV) {
				delete [] m_DataV;
			}
			m_DataV = new short[size];
			memcpy(m_DataV, m_uData, sizeof(short)*size);
			
			// Only use YUV if all components decompressed successfully 
			if (m_displayYUV != 0 || m_bShowErrors || (bSuccessY && bSuccessU && bSuccessV)) {
				Interpolate(false);
				YUVToRGB(m_DataYUV, width, height, false);
			}
			else {
				SetColorMode(false);
				YUVToRGB(m_DataY, width, height);
			}
		}
		else if(m_oDataType == WS_TYPE_WAVELET2_YUV_2) {
			
			if(!WS_IMAGE_COLOR) {
				if(m_cData) {
					delete [] m_cData;
				}
				m_cData = new BYTE[m_oDataLength];
				memcpy(m_cData, m_oData, m_oDataLength);

				if(m_uData) {
					delete [] m_uData;
				}
				int size = m_width*m_height;
				m_uData = new short[size];
				memset(m_uData, 0, sizeof(short)*size);

				bSuccessY = Decompress(0, true);
				YUVToRGB(m_uData, width, height);
			}
			else {			
				//// Y ////
				if(m_cData) {
					delete [] m_cData;
				}
				m_cData = new BYTE[m_oDataLength];
				memcpy(m_cData, m_oData, m_oDataLength);

				if(m_uData) {
					delete [] m_uData;
				}
				int size = m_width*m_height;
				m_uData = new short[size];
				memset(m_uData, 0, sizeof(short)*size);
				bSuccessY = Decompress(0, true);
				
				if(m_DataY) {
					delete [] m_DataY;
				}
				m_DataY = new short[size];
				memcpy(m_DataY, m_uData, sizeof(short)*size);
			
				//// U ////
				if(m_cData) {
					delete [] m_cData;
				}
				m_cData = new BYTE[m_oDataLengthU];
				memcpy(m_cData, m_oData + m_oDataLength, m_oDataLengthU);

				if(m_uData) {
					delete [] m_uData;
				}
				size = m_width/2*m_height;
				m_uData = new short[size];
				memset(m_uData, 0, sizeof(short)*size);
				bSuccessU = Decompress(1, true);
				
				if(m_DataU) {
					delete [] m_DataU;
				}
				m_DataU = new short[size];
				memcpy(m_DataU, m_uData, sizeof(short)*size);

		
				//// V ////
				if(m_cData) {
					delete [] m_cData;
				}
				m_cData = new BYTE[m_oDataLengthV];
				memcpy(m_cData, m_oData + m_oDataLength + m_oDataLengthU, m_oDataLengthV);

				if(m_uData) {
					delete [] m_uData;
				}
				size = m_width/2*m_height;
				m_uData = new short[size];
				memset(m_uData, 0, sizeof(short)*size);
				bSuccessV = Decompress(2, true);
				
				if(m_DataV) {
					delete [] m_DataV;
				}
				m_DataV = new short[size];
				memcpy(m_DataV, m_uData, sizeof(short)*size);
				

				// Only use YUV if all components decompressed successfully 
				if (m_displayYUV != 0 || m_bShowErrors || (bSuccessY && bSuccessU && bSuccessV)) {
					Interpolate(false);
					YUVToRGB(m_DataYUV, width, height, false);
				}
				else {
					SetColorMode(false);
					YUVToRGB(m_DataY, width, height);
				}
			}
		}
	}
}

void CWaveletStatic::YUVToRGB(short* src, int width, int height, bool raw)
{
	int  R,G,B;
	short U,V;    
    short Y;
	short *pSrc;
	int size = (width*3+(width%4))*height;	// scanlines must be aligned on 4-byte boundaries
	BYTE* pDest; 

	if(m_bData) {
		delete [] m_bData;
	}

	m_bData = new BYTE[size];
	pDest = m_bData;

	if(WS_IMAGE_COLOR) {
		if (raw) {
			BYTE *pTemp = (BYTE*)src+1;
			pSrc = (short*)pTemp;					
		}
		else { 
			pSrc = src;
		}

		
		for(int i=0;i<height;i++) {
			for(int j=0;j<width;j++) {

				// Display single component of YUV if set to Y U or V
				if (m_displayYUV > 0 && m_displayYUV <= 3) {

					// Set offset to select desired component
					if (i==0 && j==0) {
						pSrc+=m_displayYUV-1;
					}

					// Display only single component of YUV image
					if(*pSrc >= 255) {
						*pDest++ = 0xFF;
						*pDest++ = 0xFF;
						*pDest++ = 0xFF;
					}
					else if(*pSrc <= 0) {
						*pDest++ = 0x00;
						*pDest++ = 0x00;
						*pDest++ = 0x00;
					}
					else {
						*pDest++ = (BYTE)*pSrc;
						*pDest++ = (BYTE)*pSrc;
						*pDest++ = (BYTE)*pSrc;
					}
					pSrc+=3;
					continue;
				}

				Y = *pSrc++;
				U = *pSrc++;
				V = *pSrc++;

				if (Y < 1)	Y=1;
				else if (Y >254) Y=254;
			
				if (U < 16)	U=16;
				else if (U > 254) U=254;
			
				if (V < 16)	V=16;
				else if (V > 254) V=254;     
			
				B = (int)(1.164*(Y - 16) + 2.017*(U - 128));
				G = (int)(1.164*(Y - 16) - 0.813*(V - 128) - 0.392*(U - 128));
				R = (int)(1.164*(Y - 16) + 1.596*(V - 128));

				// Minor change
				B = (int)(1.164*(Y - 16) + 2.018*(U - 128));
				G = (int)(1.164*(Y - 16) - 0.813*(V - 128) - 0.392*(U - 128));
				R = (int)(1.164*(Y - 16) + 1.596*(V - 128));

				if(R>255) R = 255; 
				else if(R<0) R = 0;
			
				if(G>255) G = 255; 
				else if(G<0) G = 0;
			
				if(B>255) B = 255;
				else if(B<0) B = 0;
			
				*pDest++ = (BYTE)B;
				*pDest++ = (BYTE)G;
				*pDest++ = (BYTE)R;
			}
		}
	}
	else {
		short* line = src;
		for(int i=0;i<height;i++) {
			short* s = line;
			for(int j=0;j<width;j++) {
				if(*s >= 255) {
					*pDest++ = 0xFF;
					*pDest++ = 0xFF;
					*pDest++ = 0xFF;
				}
				else if(*s <= 0) {
					*pDest++ = 0x00;
					*pDest++ = 0x00;
					*pDest++ = 0x00;
				}
				else {
					*pDest++ = (BYTE)*s;
					*pDest++ = (BYTE)*s;
					*pDest++ = (BYTE)*s;
				}
				s++;
			}

			// pad scanline if necessary
			pDest += (width%4);
			line += m_width;
		}
	}
}

void CWaveletStatic::Compress(bool newRLE)
{
	// only B&W is implemented
	Transform();
	if(m_quantLevel >= 0 && m_quantTable != NULL) {
		Quantize();
	}
	Encode(newRLE);
}

bool CWaveletStatic::Decompress(int YUV, bool newRLE)
{
	if(Decode(YUV, newRLE)) {
		if(m_quantLevel >= 0 && m_quantTable != NULL) {
			DeQuantize(YUV);
		}
		if (!m_bDisplayTransform) {
			InvTransform(YUV);
		}
		return true;
	}
	else {
		if(YUV==0) {
			memset(m_uData, 0, m_width*m_height*sizeof(short));
		}
		else {
			memset(m_uData, 0, (m_width/2)*m_height*sizeof(short));
		}
		return false;
	}
}

void CWaveletStatic::Transform()
{
	// only B&W is implemented

	unsigned int i, level;
	// 1st horizontal pass to take advantage of 2:1 aspect ratio
	for(i=0;i<m_height;i++)	{
		ForwardDWT(&m_uData[i*m_width], m_width, 1);
	}

	// 3-level DWT transform on left low-pass image
	for(level=2;level<=8;level<<=1) {
		// rows first
		for (i=0;i<((2*m_height/level)*m_width);i+=m_width) {
			ForwardDWT(&m_uData[i], (m_width/level), 1);
		}

		// columns next
		for (i=0; i<(m_width/level); i++) {
			ForwardDWT(&m_uData[i], (2*m_height/level), m_width);
		}
	}
}

void CWaveletStatic::ForwardDWT(short* x, int n, int step)
{
	short* s = new short[n/2];
	short* d = new short[n/2];
 
	int mid=(n/2)-1;					// Middle sample of vector (n must be even)
 
	int i;
	for (i=0;i<=mid;i++)
	{
		s[i]=x[2*i*step];			// s[i] contains even samples of x[i]
		d[i]=x[2*i*step+step];		// d[i] contains odd samples of x[i]
	}
									// Special modified case for left boundary
									// filters
	short t;
	t = s[0]+s[1];
	if(t < 0) {
		t = -((-t)>>1);
	}
	else {
		t >>= 1;
	}
	d[0]=d[0]-t;

	t = (d[0]+d[0]+2);
	if(t < 0) {
		t = -((-t)>>2);
	}
	else {
		t >>= 2;
	}
	s[0]=s[0]+t;

	for (i=1;i<mid;i++)
	{								// Generic filters
		t = s[i]+s[i+1];
		if(t < 0) {
			t = -((-t)>>1);
		}
		else {
			t >>= 1;
		}
		d[i]=d[i]-t;

		t = (d[i-1]+d[i]+2);
		if(t < 0) {
			t = -((-t)>>2);
		}
		else {
			t >>= 2;
		}
		s[i]=s[i]+t;
	}
									// Special modified case for right boundary
									// filters
	t = s[mid]+s[mid];
	if(t < 0) {
		t = -((-t)>>1);
	}
	else {
		t >>= 1;
	}
	d[mid]=d[mid]-t;

	t = (d[mid-1]+d[mid]+2);
	if(t < 0) {
		t = -((-t)>>2);
	}
	else {
		t >>= 2;
	}
	s[mid]=s[mid]+t;
 
	for(i=0;i<=mid;i++)
	{
		x[i*step]=s[i];				// Recombine detail d[i] and smooth s[i]
		x[(i+mid+1)*step]=d[i];		// into Mallat diagram in x[i] vector
	}

	delete [] s;
	delete [] d;
}

void CWaveletStatic::Quantize()
{
	// only B&W is implemented
	int block = 0;

	// N-W block (only first block)
	QuantizeBlock(0, 0, (m_width/16), (m_height/8), m_quantTable[block++], m_quantLevel);

	// Scan through each block and quantize
	for(int level=16;level>=4;level>>=1)
	{
		// N-E block
		QuantizeBlock((m_width/level), 0, (m_width/level), (2*m_height/level),
			m_quantTable[block++], m_quantLevel);
		// S-E block
		QuantizeBlock((m_width/level), (2*m_height/level), (m_width/level), (2*m_height/level),
			m_quantTable[block++], m_quantLevel);
		// S-W block
		QuantizeBlock(0, (2*m_height/level), (m_width/level), (2*m_height/level),
			m_quantTable[block++], m_quantLevel);
	}

	// Quantize right half image
	QuantizeBlock((m_width/2), 0, (m_width/2), m_height,
		m_quantTable[block++], m_quantLevel);
}

void CWaveletStatic::QuantizeBlock(int x, int y, int width, int height, int q, int c)
{
	if(q < 0) {
		return;
	}

	for(int i=x;i<(x+width);i++) {
		for(int j=y;j<(y+height);j++) {
			short t = m_uData[i+j*m_width];
			if(t < 0) {
				t = -((-t)>>(q+c));
			}
			else {
				t >>= (q+c);
			}
			m_uData[i+j*m_width] = t;
		}
	}
}

void CWaveletStatic::Encode(bool newRLE)
{
	// only B&W is implemented
	m_cBitLength = 0;

	// N-W block (only first block)
	EncodeBlock(0, 0, (m_width/16), (m_height/8), newRLE);

	// Scan through each block and encode
	for(int level=16;level>=4;level>>=1)
	{
		// N-E block
		EncodeBlock((m_width/level), 0, (m_width/level), (2*m_height/level), newRLE);
		// S-E block
		EncodeBlock((m_width/level), (2*m_height/level), (m_width/level), (2*m_height/level), newRLE);
		// S-W block
		EncodeBlock(0, (2*m_height/level), (m_width/level), (2*m_height/level), newRLE);
	}

	// Encode right half image
	EncodeBlock((m_width/2), 0, (m_width/2), m_height, newRLE);
}

void CWaveletStatic::EncodeBlock(int x, int y, int width, int height, bool newRLE)
{
	int runLength;
	WORD codeWord;
	int codeLength;

	runLength = 0;
	for(int i=y;i<(y+height);i++) {
		for(int j=x;j<(x+width);j++) {
			short val = m_uData[i*m_width+j];
			if(val == 0) {
				if(newRLE) {
					if(++runLength == 260) {
						OutputCodeWord(0x0100, 12);
						runLength = 0;
					}
				}
				else {
					if(++runLength == 8191) {
						OutputCodeWord((WORD)runLength, 16);
						runLength = 0;
					}
				}
			}
			else {
				if(runLength > 0) {
					if(newRLE) {
						if(runLength <= 4) {
							OutputCodeWord((WORD)(runLength&0x00000003), 6);
						}
						else {
							OutputCodeWord(0x0100 | (WORD)(runLength-4), 12);
						}
						runLength = 0;
					}
					else {
						OutputCodeWord((WORD)runLength, 16);
						runLength = 0;
					}
				}

				EncodeValue(val, codeWord, codeLength);
				OutputCodeWord(codeWord, codeLength);
			}
		}
	}

	// Check if we have an outstanding run-length before exiting
	if(runLength > 0) {
		if(newRLE) {
			if(runLength <= 4) {
				OutputCodeWord((WORD)(runLength&0x00000003), 6);
			}
			else {
				OutputCodeWord(0x0100 | (WORD)(runLength-4), 12);
			}
			runLength = 0;
		}
		else {
			OutputCodeWord((WORD)runLength, 16);
			runLength = 0;
		}
	}

	// Pad to 16-bit boundary
	OutputCodeWord(0, 16-(m_cBitLength%16));
}

void CWaveletStatic::EncodeValue(short val, WORD& codeWord, int& codeLength)
{
	// clamp input
	if(val > 2047) {
		val = 2047;
	}
	else if(val < -2047) {
		val = -2047;
	}

	if(val==0) {
		// This should never happen! Special case, zero RLE
		// should be handled outside this function
		codeLength = 0;
		return;
	}

	int aval = abs(val);
	if(aval == 1) {
		// 001x x=sign
		if(val > 0) {
			codeWord = 0x0002;
		}
		else {
			codeWord = 0x0003;
		}
		codeLength = 4;
	}
	else if(aval <= 3) {
		// 010x x=sign
		if(val > 0) {
			codeWord = (0x0004 << 1) | (aval & 0x0001);
		}
		else {
			codeWord = (0x0005 << 1) | (aval & 0x0001);
		}
		codeLength = 5;
	}
	else if(aval <= 7) {
		// 011x x=sign
		if(val > 0) {
			codeWord = (0x0006 << 2) | (aval & 0x0003);
		}
		else {
			codeWord = (0x0007 << 2) | (aval & 0x0003);
		}
		codeLength = 6;
	}
	else if(aval <= 15) {
		// 1000x x=sign
		if(val > 0) {
			codeWord = (0x0010 << 3) | (aval & 0x0007);
		}
		else {
			codeWord = (0x0011 << 3) | (aval & 0x0007);
		}
		codeLength = 8;
	}
	else if(aval <= 31) {
		// 1001x x=sign
		if(val > 0) {
			codeWord = (0x0012 << 4) | (aval & 0x000F);
		}
		else {
			codeWord = (0x0013 << 4) | (aval & 0x000F);
		}
		codeLength = 9;
	}
	else if(aval <= 63) {
		// 1010x x=sign
		if(val > 0) {
			codeWord = (0x0014 << 5) | (aval & 0x001F);
		}
		else {
			codeWord = (0x0015 << 5) | (aval & 0x001F);
		}
		codeLength = 10;
	}
	else if(aval <= 127) {
		// 1011x x=sign
		if(val > 0) {
			codeWord = (0x0016 << 6) | (aval & 0x003F);
		}
		else {
			codeWord = (0x0017 << 6) | (aval & 0x003F);
		}
		codeLength = 11;
	}
	else if(aval <= 255) {
		// 1100x x=sign
		if(val > 0) {
			codeWord = (0x0018 << 7) | (aval & 0x007F);
		}
		else {
			codeWord = (0x0019 << 7) | (aval & 0x007F);
		}
		codeLength = 12;
	}
	else if(aval <= 511) {
		// 1101x x=sign
		if(val > 0) {
			codeWord = (0x001A << 8) | (aval & 0x00FF);
		}
		else {
			codeWord = (0x001B << 8) | (aval & 0x00FF);
		}
		codeLength = 13;
	}
	else if(aval <= 1023) {
		// 1110x x=sign
		if(val > 0) {
			codeWord = (0x001C << 9) | (aval & 0x01FF);
		}
		else {
			codeWord = (0x001D << 9) | (aval & 0x01FF);
		}
		codeLength = 14;
	}
	else {
		// 1111x x=sign
		if(val > 0) {
			codeWord = (0x001E << 10) | (aval & 0x03FF);
		}
		else {
			codeWord = (0x001F << 10) | (aval & 0x03FF);
		}
		codeLength = 15;
	}
}

void CWaveletStatic::OutputCodeWord(WORD codeWord, int codeLength)
{
	BYTE* p = m_cData;
	p += m_cBitLength/8;
	int left = 8-(m_cBitLength%8);
	m_cBitLength += codeLength;
	while(codeLength > 0) {
		if(left >= codeLength) {
			*p |= (BYTE)(codeWord << (left-codeLength));
			codeLength = 0;
		}
		else {
			*p |= (BYTE)(codeWord >> (codeLength-left));
			codeWord &= (0xFFFF >> (16-(codeLength-left)));
			codeLength -= left;
			p++;
			left = 8;
		}
	}
}

bool CWaveletStatic::Decode(int YUV, bool newRLE)
{
	int width = (YUV==0)? m_width:m_width/2;
	int level;
	m_cBitLength = 0;

	if(WS_IMAGE_HALFRES) {
		level = 8;
		
		// N-W block (only first block)
		if(!DecodeBlock(0, 0, (width/8), (m_height/8), YUV, newRLE)) {
			return false;
		}

		// Scan through each block and decode
		for(unsigned int i=1;i<=3;i++) {
			if(m_displayMode <= i) {
				break;
			}

			// N-E block
			if(!DecodeBlock((width/level), 0, (width/level), (m_height/level), YUV, newRLE)) {
				return false;
			}	
			// S-E block
			if(!DecodeBlock((width/level), (m_height/level), (width/level), (m_height/level), YUV, newRLE)) {
				return false;
			}	
			// S-W block
			if(!DecodeBlock(0, (m_height/level), (width/level), (m_height/level), YUV, newRLE)) {
				return false;
			}

			level >>= 1;
		}
	}
	else {
		level = 16;

		// N-W block (only first block)
		if(!DecodeBlock(0, 0, (width/16), (m_height/8), YUV, newRLE)) {
			return false;
		}
	
		// Scan through each block and decode
		for(unsigned int i=1;i<=3;i++) {
			if(m_displayMode <= i) {
				break;
			}

			// N-E block
			if(!DecodeBlock((width/level), 0, (width/level), (2*m_height/level), YUV, newRLE)) {
				return false;
			}
			// S-E block
			if(!DecodeBlock((width/level), (2*m_height/level), (width/level), (2*m_height/level), YUV, newRLE)) {
				return false;
			}
			// S-W block
			if(!DecodeBlock(0, (2*m_height/level), (width/level), (2*m_height/level), YUV, newRLE)) {
				return false;
			}

			level >>= 1;
		}

		// Decode right half image
		if(m_displayMode >= 5) {
			if(!DecodeBlock((width/2), 0, (width/2), m_height, YUV, newRLE)) {
				return false;
			}
		}
	}

	return true;
}

bool CWaveletStatic::DecodeBlock(int x, int y, int width, int height, int YUV, bool newRLE)
{
	int runLength;
	WORD codeWord;
	int codeLength;
	unsigned int DataLength;
	int widthFull = (YUV==0)? m_width:m_width/2;

	if(YUV==0) {
		DataLength = m_oDataLength;
	}
	else if(YUV==1) {
		DataLength = m_oDataLengthU;
	}
	else {
		DataLength = m_oDataLengthV;
	}

	runLength = 0;
	for(int i=y;i<(y+height);i++) {
		for(int j=x;j<(x+width);j++) {
			if(runLength > 0) {
				m_uData[i*widthFull+j] = 0;
				runLength--;
			}
			else {
				if(!InputCodeWord(codeWord, codeLength, runLength, newRLE, YUV)) {
					return false;
				}
				if(runLength > 0) {
					m_uData[i*widthFull+j] = 0;
					runLength--;
				}
				else {
					short val;
					if(!DecodeValue(val, codeWord, codeLength)) {
						return false;
					}
					m_uData[i*widthFull+j] = val;
				}
			}
		}
	}

	// Pad to 16-bit boundary
	m_cBitLength += 16-(m_cBitLength%16);
	if((m_oDataType == WS_TYPE_WAVELET_Y 
	|| m_oDataType == WS_TYPE_WAVELET2_Y 
	|| m_oDataType == WS_TYPE_WAVELET2_YUV
	|| m_oDataType == WS_TYPE_WAVELET2_YUV_2 ) 
	&& m_cBitLength / 8 > DataLength) {
#if _DEBUG
		CString sMsg;
		sMsg.Format("[Plane %s] Calculated Length %d > %d Original Length. Decoding stopped for this image.\n", YUV, m_cBitLength / 8, DataLength);
		TRACE(sMsg);
#endif
		return false;
	}
	else {
		return true;
	}
}

bool CWaveletStatic::DecodeValue(short& val, WORD codeWord, int codeLength)
{
	switch(codeLength) {
	case 4:
		if(codeWord & 0x0001) {
			val = -1;
		}
		else {
			val = 1;
		}
		break;
	case 5:
		val = 0x0002 | (codeWord & 0x0001);
		if(codeWord & 0x0002) {
			val = -val;
		}
		break;
	case 6:
		val = 0x0004 | (codeWord & 0x0003);
		if(codeWord & 0x0004) {
			val = -val;
		}
		break;
	case 8:
		val = 0x0008 | (codeWord & 0x0007);
		if(codeWord & 0x0008) {
			val = -val;
		}
		break;
	case 9:
		val = 0x0010 | (codeWord & 0x000F);
		if(codeWord & 0x0010) {
			val = -val;
		}
		break;
	case 10:
		val = 0x0020 | (codeWord & 0x001F);
		if(codeWord & 0x0020) {
			val = -val;
		}
		break;
	case 11:
		val = 0x0040 | (codeWord & 0x003F);
		if(codeWord & 0x0040) {
			val = -val;
		}
		break;
	case 12:
		val = 0x0080 | (codeWord & 0x007F);
		if(codeWord & 0x0080) {
			val = -val;
		}
		break;
	case 13:
		val = 0x0100 | (codeWord & 0x00FF);
		if(codeWord & 0x0100) {
			val = -val;
		}
		break;
	case 14:
		val = 0x0200 | (codeWord & 0x01FF);
		if(codeWord & 0x0200) {
			val = -val;
		}
		break;
	case 15:
		val = 0x0400 | (codeWord & 0x03FF);
		if(codeWord & 0x0400) {
			val = -val;
		}
		break;
	default:
		return false;
	}
	return true;
}

bool CWaveletStatic::InputCodeWord(WORD& codeWord, int& codeLength, int& runLength, bool newRLE, int YUV)
{
	BYTE* p = m_cData;
	p += m_cBitLength/8;
	int left = 8-(m_cBitLength%8);

	int length;
	if(*p & (0x01 << (left-1))) {
		length = 4;
	}
	else {
		length = 3;
	}
	codeLength = length;
	codeWord = 0;

	while(length > 0) {
		if(left >= length) {
			codeWord <<= length;
			codeWord |= (*p & (0xFF >> (8-left))) >> (left-length);
			left -= length;
			length = 0;
		}
		else {
			codeWord <<= left;
			codeWord |= *p & (0xFF >> (8-left));
			length -= left;
			p++;
			left = 8;
		}
	}

	int RLECode;
	if(codeWord == 0) {
		if(newRLE) {
			if(left == 0) {
				p++;
				left = 8;
			}
			if(*p & (0x01 << (left-1))) {
				// long run length
				RLECode = 3;
				codeWord = 0x0001;
				length = 8;
			}
			else {
				// short run length
				RLECode = 2;
				length = 2;
			}
			left--;
			codeLength += length + 1;
		}
		else {
			RLECode = 1;
			length = 13;
			codeLength += length;
		}
	}
	else {
		RLECode = 0;
		switch(codeWord) {
		case 1:
			length = 1;
			break;
		case 2:
			length = 2;
			break;
		case 3:
			length = 3;
			break;
		case 8:
			length = 4;
			break;
		case 9:
			length = 5;
			break;
		case 10:
			length = 6;
			break;
		case 11:
			length = 7;
			break;
		case 12:
			length = 8;
			break;
		case 13:
			length = 9;
			break;
		case 14:
			length = 10;
			break;
		case 15:
			length = 11;
			break;
		default:
			return false;
		}
		codeLength += length;
	}

	while(length > 0) {
		if(left >= length) {
			codeWord <<= length;
			codeWord |= (*p & (0xFF >> (8-left))) >> (left-length);
			left -= length;
			length = 0;
		}
		else {
			codeWord <<= left;
			codeWord |= *p & (0xFF >> (8-left));
			length -= left;
			p++;
			left = 8;
		}
	}

	switch(RLECode) {
	case 1:
		runLength = codeWord;
		break;
	case 2:
		runLength = (codeWord & 0x0003);
		if(runLength == 0) {
			runLength = 4;
		}
		break;
	case 3:
		runLength = (codeWord & 0x00FF);
		if(runLength == 0) {
			runLength = 260;
		}
		else {
			runLength += 4;
		}
		break;
	default:
		runLength = 0;
		break;
	}

	m_cBitLength += codeLength;

	// if((m_oDataType == WS_TYPE_WAVELET_Y || m_oDataType == WS_TYPE_WAVELET2_Y) && m_cBitLength / 8 > m_oDataLength) {	// First version checks Y only.
	if (((YUV == 0) && m_cBitLength / 8 > m_oDataLength ) ||
		((YUV == 1) && m_cBitLength / 8 > m_oDataLengthU) ||
		((YUV == 2) && m_cBitLength / 8 > m_oDataLengthV) )
	{

#if _DEBUG
		CString sMsg;
		switch (YUV) {
		case 0:
			sMsg.Format("[Y] Calculated Length %d > %d Original Length. Decoding stopped for this image.\n", m_cBitLength / 8, m_oDataLength);
			break;
		case 1:
			sMsg.Format("[U] Calculated Length %d > %d Original Length. Decoding stopped for this image.\n", m_cBitLength / 8, m_oDataLengthU);
			break;
		case 2:
			sMsg.Format("[V] Calculated Length %d > %d Original Length. Decoding stopped for this image.\n", m_cBitLength / 8, m_oDataLengthV);
			break;
		default:
			break;
		}
		TRACE(sMsg);
#endif
		return false;
	}
	else {
		return true;
	}
}

void CWaveletStatic::DeQuantize(int YUV)
{
	int width = (YUV==0)? m_width:m_width/2;
	int block = 0;
	int level;

	if(WS_IMAGE_HALFRES) {
		// N-W block (only first block)
		DeQuantizeBlock(0, 0, (width/8), (m_height/8), YUV, m_quantTable[YUV*11+block], m_quantLevel);
		block++;

		// Scan through each block and quantize
		for(level=8;level>=2;level>>=1)
		{
			// N-E block
			DeQuantizeBlock((width/level), 0, (width/level), (m_height/level), YUV,
				m_quantTable[YUV*11+block], m_quantLevel);
			block++;

			// S-E block
			DeQuantizeBlock((width/level), (m_height/level), (width/level), (m_height/level), YUV,
				m_quantTable[YUV*11+block], m_quantLevel);
			block++;

			// S-W block
			DeQuantizeBlock(0, (m_height/level), (width/level), (m_height/level), YUV,
				m_quantTable[YUV*11+block], m_quantLevel);
			block++;
		}
	}
	else {
		int offset = YUV*11;

		// N-W block (only first block)
		DeQuantizeBlock(0, 0, (width/16), (m_height/8), YUV, m_quantTable[offset+block], m_quantLevel);
		block++;

		// Scan through each block and quantize
		level = 16;
		for(unsigned int i=1;i<=3;i++) {
			if(m_displayMode <= i) {
				break;
			}

			// N-E block
			DeQuantizeBlock((width/level), 0, (width/level), (2*m_height/level), YUV, 
				m_quantTable[offset+block], m_quantLevel);
			block++;

			// S-E block
			DeQuantizeBlock((width/level), (2*m_height/level), (width/level), (2*m_height/level), YUV, 
				m_quantTable[offset+block], m_quantLevel);
			block++;

			// S-W block
			DeQuantizeBlock(0, (2*m_height/level), (width/level), (2*m_height/level), YUV, 
				m_quantTable[offset+block], m_quantLevel);
			block++;

			level >>= 1;
		}
	
		// Quantize right half image
		DeQuantizeBlock((width/2), 0, (width/2), m_height, YUV,
			m_quantTable[offset+block], m_quantLevel);
	}
}

void CWaveletStatic::DeQuantizeBlock(int x, int y, int width, int height, int YUV, int q, int c)
{
	int widthFull = (YUV==0)? m_width:m_width/2;
	if(q < 0) {
		return;
	}

	for(int i=x;i<(x+width);i++) {
		for(int j=y;j<(y+height);j++) {
			m_uData[i+j*widthFull] <<= (q+c);
		}
	}
}

void CWaveletStatic::InvTransform(int YUV)
{
	int width = (YUV==0)? m_width:m_width/2;
	unsigned int i, j, level, height;

	if(WS_IMAGE_HALFRES) {
		level = 4;
		height = m_height;
	}
	else {
		level = 8;
		height = 2*m_height;
	}

	// IDWT transform image
	for(i=1;i<=3;i++) {
		if(m_displayMode <= i) {
			break;
		}

		// columns first
		for (j=0; j<(width/level); j++) {
			ReverseDWT(&m_uData[j], (height/level), width);
		}

		// rows next
		for (j=0;j<((height/level)*width);j+=width) {
			ReverseDWT(&m_uData[j], (width/level), 1);
		}

		level >>= 1;
	}

	// Undo 1st horizontal pass
	if(m_displayMode >= 5) {
		for(i=0;i<m_height;i++)	{
			ReverseDWT(&m_uData[i*width], width, 1);
		}
	}
}

void CWaveletStatic::ReverseDWT(short* x, int n, int step)
{
	short* s = new short[n/2];
	short* d = new short[n/2];
 
	int mid=(n/2)-1;					// Middle sample of vector (n must be even)

	int i;
	for (i=0;i<=mid;i++)
	{
		s[i]=x[i*step];				// s[i] contains even samples of x[i]
		d[i]=x[(i+mid+1)*step];		// d[i] contains odd samples of x[i]
	}
									// Special modified case for left boundary
									// filters
	short t = (d[mid-1]+d[mid]+2);
	if(t < 0) {
		t = -((-t)>>2);
	}
	else {
		t >>= 2;
	}
	s[mid]=s[mid]-t;

	t = s[mid]+s[mid];
	if(t < 0) {
		t = -((-t)>>1);
	}
	else {
		t >>= 1;
	}
	d[mid]=d[mid]+t;

	for (i=mid-1; i>0; i--)
	{								// Generic filters
		t = (d[i-1]+d[i]+2);
		if(t < 0) {
			t = -((-t)>>2);
		}
		else {
			t >>= 2;
		}
		s[i]=s[i]-t;

		t = s[i]+s[i+1];
		if(t < 0) {
			t = -((-t)>>1);
		}
		else {
			t >>= 1;
		}
		d[i]=d[i]+t;
	}
									// Special modified case for right boundary
									// filters
	t = (d[0]+d[0]+2);
	if(t < 0) {
		t = -((-t)>>2);
	}
	else {
		t >>= 2;
	}
	s[0]=s[0]-t;

	t = s[0]+s[1];
	if(t < 0) {
		t = -((-t)>>1);
	}
	else {
		t >>= 1;
	}
	d[0]=d[0]+t;
 
	for(i=0;i<=mid;i++)
	{
	    x[2*step*i]=s[i];			// Recombine detail d[i] and smooth s[i]
	    x[2*step*i+step]=d[i];		// into x[i] vector
	}

	delete [] s;
	delete [] d;
}

void CWaveletStatic::Interpolate(bool raw)
{
    int size = m_width*m_height*3;

	if(m_DataYUV != NULL) {
		delete [] m_DataYUV;
		m_DataYUV = NULL;
	}
	
	m_DataYUV = new short[size];
	memset(m_DataYUV, 0, sizeof(short)*size);
	short *pYUV = m_DataYUV;
	short *pY = m_DataY;
	short *pU = m_DataU;
	short *pV = m_DataV;
	
	if(raw) {
		BYTE *pByte = (BYTE*)m_DataY;
		while (*pByte==0xFF) {
			pByte++;
		}
		pY=(short*)pByte;

		pByte = (BYTE*)m_DataU;
		while (*pByte==0xFF) {
			pByte++;
		}
		pU=(short*)pByte;
	}

    for(unsigned int i=0;i<m_height; i++)
    for(unsigned int j=0;j<m_width/2;j++)
    {
        *pYUV++ = *pY++;
        *pYUV++ = *pU;
        *pYUV++ = *pV;

        if(j==m_width/2-1) {
            *pYUV++ = *pY++;
            *pYUV++ = *pU++;
            *pYUV++ = *pV++;
        }
        else {
			// Y component
            *pYUV++ = *pY++;

			// U component
            if((*pU & 0x8000)==(*(pU+1) & 0x8000)) {// if same sign
                unsigned long a = *pU;
                a += *(pU+1);
                a /= 2;
                if(raw) {
                    *pYUV++ = (short)(a & 0x0000FF00);
                }
                else {
                    *pYUV++ = (short)a;
                }
            }
            else {
                *pYUV++ = *pU;
			}
            pU++;

			// V component
            if((*pV & 0x8000)==(*(pV+1) & 0x8000)) {   //if same sign
                unsigned long a = *pV;
                a += *(pV+1);
                a /= 2;
                if(raw) {
                    *pYUV++ = (short)(a & 0x0000FF00);
                }
                else {
                    *pYUV++ = (short)a;
                }

            }
            else {
                *pYUV++ = *pV;
			}
            pV++;
        }
    }
}


/*TEMP
more complicated filter, not necessary based on many articles
void CWaveletStatic::Interpolate(bool raw)
{
	int size = m_width*m_height*3;
	
	if(m_DataYUV != NULL) {
//TEMP memory leak, temporily avoid shared access violation		delete [] m_DataYUV;
	}
	
	m_DataYUV = new short[size];
	memset(m_DataYUV, 0, sizeof(short)*size);
	short *pYUV = m_DataYUV;
	short *pY = m_DataY;
	short *pU = m_DataU;
	short *pV = m_DataV;
	int a;
	short temp;

	if(raw) {
		BYTE *pByte = (BYTE*)m_DataY;
		while (*pByte==0xFF) {
			pByte++;
		}
		pY=(short*)pByte;

		pByte = (BYTE*)m_DataU;
		while (*pByte==0xFF) {
			pByte++;
		}
		pU=(short*)pByte;
	}

	for(unsigned int i=0;i<m_height; i++)
    for(unsigned int j=0;j<m_width/2;j++)
    {
		*pYUV++ = *pY++;
		*pYUV++ = *pU;
		*pYUV++ = *pV;

		if(j == m_width/2-1) {
			*pYUV++ = *pY++;
			*pYUV++ = *pU++;
			*pYUV++ = *pV++;
		}
		else {
			// Y component
			*pYUV++ = *pY++;

			// U component
			if((*pU & 0x8000)==(*(pU+1) & 0x8000)) {// if same sign
				
				a = (160*(*(pU) + *(pU+1)) - 48*(*(pU-1) + *(pU+2)) + 24*(*(pU-2) + *(pU+3))
					- 12*(*(pU-3) + *(pU+4)) + 6*(*(pU-4) + *(pU+5)) - 42*(*(pU-5) + *(pU+6)))/176.0f;

				if(raw) {
					//TEMP *pYUV++ = (short)(a & 0x0000FF00);
				}
				else {
					temp = (short)a;
					*pYUV++ = temp;
				}
			}
			else {
				*pYUV++ = *pU;
			}
			pU++;

			// V component
			if((*pV & 0x8000)==(*(pV+1) & 0x8000)) {	//if same sign

				a = (160*(*(pV) + *(pV+1)) - 48*(*(pV-1) + *(pV+2)) + 24*(*(pV-2) + *(pV+3))
					- 12*(*(pV-3) + *(pV+4)) + 6*(*(pV-4) + *(pV+5)) - 42*(*(pV-5) + *(pV+6)) )/176.0f;

				if(raw) {
					//TEMP *pYUV++ = (short)(a & 0x0000FF00);
				}
				else {
					temp = (short)a;
					*pYUV++ = temp;
				}

			}
			else {
				*pYUV++ = *pV;
			}
			pV++;
		}
	}
}*/