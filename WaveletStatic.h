#if !defined(AFX_WAVELETSTATIC_H__0A8C9417_24AC_4C5C_8F89_10443568D0E6__INCLUDED_)
#define AFX_WAVELETSTATIC_H__0A8C9417_24AC_4C5C_8F89_10443568D0E6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WaveletStatic.h : header file
//
/*
#ifdef WAVELET_EXPORTS
#define WAVELET_API __declspec(dllexport)
#else
#define WAVELET_API __declspec(dllimport)
#endif
*/

#define WS_TYPE_RAW_Y				0
#define WS_TYPE_WAVELET_Y			1
#define WS_TYPE_RAW_YUV				2
#define WS_TYPE_WAVELET_YUV			3
#define WS_TYPE_WAVELET2_Y			4
#define WS_TYPE_WAVELET2_YUV		5
#define WS_TYPE_WAVELET2_YUV_2		6

/////////////////////////////////////////////////////////////////////////////
// CWaveletStatic window

//class WAVELET_API CWaveletStatic : public CStatic
class CWaveletStatic : public CStatic

{
// Construction
public:
	CWaveletStatic();

// Attributes
public:

// Operations
public:
	inline BYTE * GetData() { return m_bData; }
	void SetWidth(int width) { m_width = width; }
	void SetHeight(int height) { m_height = height; }
	void SetData(BYTE* data, int type, int length, int lengthU=0, int lengthV=0);
	void SetQuantTable(int* table) { m_quantTable = table; }
	void SetQuantLevel(int level) { m_quantLevel = level; }
	void SetContrast(SHORT contrast);
	void SetBrightness(SHORT brightness);
	int GetModeWidth();
	int GetModeHeight();
	int GetCompressedLength() { return m_cBitLength; }

	void SetColorMode(bool value=true) { m_bColorMode = value; }
	void SetDisplayMode(unsigned int mode) { m_displayMode = mode; }
	void SetDisplayYUV(unsigned int yuv);
	void SetDisplayTransform(bool transform) { m_bDisplayTransform = transform; }
	void SetShowErrors(bool value=true) { m_bShowErrors = value; }

	bool GetColorMode() { return m_bColorMode; }
	unsigned int GetDisplayMode() { return m_displayMode; }
	unsigned int GetDisplayYUV() { return m_displayYUV; }
	bool GetDisplayTransform() { return m_bDisplayTransform; }
	bool GetShowErrors() { return m_bShowErrors; }

	void GetUncompressedData(short * &data);
	void CreateImage(int& width, int& height);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWaveletStatic)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CWaveletStatic();

	// Generated message map functions
protected:
	unsigned int m_width;
	unsigned int m_height;
	BITMAPINFO m_bmi;

	BYTE* m_oData;		// original (raw or compressed) data
	unsigned int m_oDataType;
	DWORD m_oDataLength;
	DWORD m_oDataLengthU;
	DWORD m_oDataLengthV;

	BYTE* m_cData;		// compressed data
	short* m_uData;		// uncompressed (reconstructed) data
	short* m_DataY;		// uncompressed (reconstructed) Y data
	short* m_DataU;		// uncompressed (reconstructed) U data
	short* m_DataV;		// uncompressed (reconstructed) V data
	short* m_DataYUV;	// Interpolated data
	BYTE* m_bData;		// bitmap (RGB) data
	int* m_quantTable;
	unsigned int m_quantLevel;
	unsigned int m_displayMode;	// Wavelet level to display
	unsigned int m_displayYUV;	// YUV component to display. Y=1, U=2, V=3. Default is 0 ==> all avail YUV.
	bool m_bDisplayTransform;	// Display the encoded transform when true
	bool m_bColorMode;			// Color components present when true
	bool m_bShowErrors;			// Avoid showing errored data when set to true

	unsigned int m_cBitLength;

	COLORADJUSTMENT colorAdjustment;
	COLORADJUSTMENT m_colorAdj;

	void Compress(bool newRLE);
	bool Decompress(int YUV, bool newRLE);

	void Transform();
	void ForwardDWT(short* x, int n, int step);

	void Quantize();
	void QuantizeBlock(int x, int y, int width, int height, int q, int c);

	void Encode(bool newRLE);
	void EncodeBlock(int x, int y, int width, int height, bool newRLE);
	void EncodeValue(short val, WORD& codeWord, int& codeLength);
	void OutputCodeWord(WORD codeWord, int codeLength);

	bool Decode(int YUV, bool newRLE);
	bool DecodeBlock(int x, int y, int width, int height, int YUV, bool newRLE);
	bool InputCodeWord(WORD& codeWord, int& codeLength, int& runLength, bool newRLE, int YUV);
	bool DecodeValue(short& val, WORD codeWord, int codeLength);

	void DeQuantize(int YUV);
	void DeQuantizeBlock(int x, int y, int width, int height, int YUV, int q, int c);
	
	void InvTransform(int YUV);
	void ReverseDWT(short* x, int n, int step);

	void YUVToRGB(short* src, int width, int height, bool raw=false);

	void Interpolate(bool raw);
	
	//{{AFX_MSG(CWaveletStatic)
	afx_msg void OnPaint();
	afx_msg void OnWindowPosChanging(WINDOWPOS FAR* lpwndpos);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WAVELETSTATIC_H__0A8C9417_24AC_4C5C_8F89_10443568D0E6__INCLUDED_)
