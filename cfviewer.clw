; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CExtractDialog
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "cfviewer.h"
LastPage=0

ClassCount=8
Class1=CCFViewerApp
Class2=CCFViewerDlg
Class3=CWaveletStatic

ResourceCount=12
Resource1=IDD_SEARCH_DIALOG
Resource2=IDD_EXTRACT_DIALOG (English (U.S.))
Resource3=IDD_CFVIEWER_DIALOG
Class4=CReaderDlg
Class5=CJumpDlg
Class6=CSectorDlg
Resource4=IDD_SECTOR_DIALOG
Class7=CSearchDlg
Resource5=IDD_CFVIEWER_DIALOG (English (U.S.))
Class8=CExtractDialog
Resource6=IDD_JUMP_DIALOG
Resource7=IDD_JUMP_DIALOG (English (U.S.))
Resource8=IDD_READER_DIALOG
Resource9=IDD_READER_DIALOG (English (U.S.))
Resource10=IDD_SECTOR_DIALOG (English (U.S.))
Resource11=IDD_SEARCH_DIALOG (English (U.S.))
Resource12=IDD_EXTRACT_DIALOG

[CLS:CCFViewerApp]
Type=0
BaseClass=CWinApp
HeaderFile=cfviewer.h
ImplementationFile=cfviewer.cpp
LastObject=CCFViewerApp

[CLS:CCFViewerDlg]
Type=0
BaseClass=CDialog
HeaderFile=cfviewerDlg.h
ImplementationFile=cfviewerDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=IDC_COMBO_TRANSFORM

[CLS:CWaveletStatic]
Type=0
BaseClass=CStatic
HeaderFile=WaveletStatic.h
ImplementationFile=WaveletStatic.cpp

[DLG:IDD_CFVIEWER_DIALOG]
Type=1
Class=CCFViewerDlg
ControlCount=36
Control1=IDC_SOURCE1,button,1342308361
Control2=IDC_SOURCE2,button,1342177289
Control3=IDC_EVENTS,SysListView32,1350762505
Control4=IDC_PLAY_MODE1,button,1342308361
Control5=IDC_PLAY_MODE2,button,1342177289
Control6=IDC_PREV,button,1342374016
Control7=IDC_REWIND,button,1342242944
Control8=IDC_PAUSE,button,1342242944
Control9=IDC_PLAY,button,1342242944
Control10=IDC_NEXT,button,1342242944
Control11=IDC_COMBO_YUV,combobox,1344339971
Control12=IDC_COMBO_DISPLAY_OPTIONS,combobox,1344339971
Control13=IDC_INFO0,static,1342177280
Control14=IDC_FILENAME,edit,1350631552
Control15=IDC_BROWSE,button,1342242816
Control16=IDC_OPEN,button,1342242816
Control17=IDC_STATIC,button,1342177287
Control18=IDC_JUMP,button,1342242816
Control19=IDC_IMAGE0,static,1342177287
Control20=IDC_IMAGE1,static,1342177287
Control21=IDC_INFO1,static,1342177280
Control22=IDC_IMAGE2,static,1073741831
Control23=IDC_INFO2,static,1073741824
Control24=IDC_IMAGE3,static,1073741831
Control25=IDC_INFO3,static,1073741824
Control26=IDC_STATIC,button,1342177287
Control27=IDC_SEARCH_EVENTS,button,1342242816
Control28=IDC_EXTRACT_EVENT,button,1476460544
Control29=IDC_STATIC,button,1342177287
Control30=IDC_VIEW_SECTOR,button,1342242816
Control31=IDC_SECTOR,static,1342308353
Control32=IDC_STATUS,static,1342177280
Control33=IDC_STATUS2,static,1342308352
Control34=IDC_GRAPH,button,1476460544
Control35=IDC_SLIDER,msctls_trackbar32,1342242840
Control36=IDC_STATIC,static,1342308352

[DLG:IDD_SECTOR_DIALOG]
Type=1
Class=CSectorDlg
ControlCount=7
Control1=IDC_PREV,button,1342242944
Control2=IDC_NEXT,button,1342242944
Control3=IDC_GOTO,button,1342242817
Control4=IDC_STATIC,static,1342308864
Control5=IDC_LBA,edit,1350631552
Control6=IDC_TEXT,RICHEDIT,1342244868
Control7=IDC_LAST,button,1476460544

[DLG:IDD_READER_DIALOG]
Type=1
Class=CReaderDlg
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_DEVICE,listbox,1353777409

[DLG:IDD_JUMP_DIALOG]
Type=1
Class=CJumpDlg
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_ADDRESS,edit,1350631552

[CLS:CReaderDlg]
Type=0
HeaderFile=ReaderDlg.h
ImplementationFile=ReaderDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CReaderDlg

[CLS:CJumpDlg]
Type=0
HeaderFile=JumpDlg.h
ImplementationFile=JumpDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CJumpDlg

[CLS:CSectorDlg]
Type=0
HeaderFile=SectorDlg.h
ImplementationFile=SectorDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CSectorDlg

[DLG:IDD_SEARCH_DIALOG]
Type=1
Class=CSearchDlg
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_DIRECTION1,button,1342308361
Control4=IDC_DIRECTION2,button,1342177289
Control5=IDC_STATIC,button,1342177287

[CLS:CSearchDlg]
Type=0
HeaderFile=SearchDlg.h
ImplementationFile=SearchDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CSearchDlg
VirtualFilter=dWC

[DLG:IDD_EXTRACT_DIALOG]
Type=1
Class=CExtractDialog
ControlCount=10
Control1=IDC_TYPE1,button,1342308361
Control2=IDC_FOLDERS,button,1342251011
Control3=IDC_TYPE2,button,1342177289
Control4=IDC_TYPE3,button,1342177289
Control5=IDC_FILTER,button,1342242819
Control6=IDC_TYPE4,button,1342177289
Control7=IDC_TYPE5,button,1342177289
Control8=IDOK,button,1342373889
Control9=IDCANCEL,button,1342242816
Control10=IDC_STATIC,button,1342177287

[CLS:CExtractDialog]
Type=0
HeaderFile=ExtractDialog.h
ImplementationFile=ExtractDialog.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_TYPE4

[DLG:IDD_CFVIEWER_DIALOG (English (U.S.))]
Type=1
Class=CCFViewerDlg
ControlCount=33
Control1=IDC_IMAGE0,static,1342177287
Control2=IDC_INFO0,static,1342177280
Control3=IDC_IMAGE1,static,1342177287
Control4=IDC_INFO1,static,1342177280
Control5=IDC_IMAGE2,static,1073741831
Control6=IDC_INFO2,static,1073741824
Control7=IDC_IMAGE3,static,1073741831
Control8=IDC_INFO3,static,1073741824
Control9=IDC_STATIC,button,1342177287
Control10=IDC_SOURCE1,button,1342308361
Control11=IDC_SOURCE2,button,1342177289
Control12=IDC_FILENAME,edit,1350631552
Control13=IDC_BROWSE,button,1342242816
Control14=IDC_OPEN,button,1342242816
Control15=IDC_STATIC,button,1342177287
Control16=IDC_EVENTS,SysListView32,1350631433
Control17=IDC_SEARCH_EVENTS,button,1342242816
Control18=IDC_EXTRACT_EVENT,button,1476460544
Control19=IDC_STATIC,button,1342177287
Control20=IDC_PLAY_MODE1,button,1342308361
Control21=IDC_PLAY_MODE2,button,1342177289
Control22=IDC_PREV,button,1342242944
Control23=IDC_REWIND,button,1342242944
Control24=IDC_PAUSE,button,1342242944
Control25=IDC_PLAY,button,1342242944
Control26=IDC_NEXT,button,1342242944
Control27=IDC_JUMP,button,1342242816
Control28=IDC_VIEW_SECTOR,button,1342242816
Control29=IDC_SECTOR,static,1342308353
Control30=IDC_STATUS,static,1342177280
Control31=IDC_STATUS2,static,1342308352
Control32=IDC_GRAPH,button,1476460544
Control33=IDC_SLIDER,msctls_trackbar32,1342242840

[DLG:IDD_READER_DIALOG (English (U.S.))]
Type=1
Class=CReaderDlg
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_DEVICE,listbox,1353777409

[DLG:IDD_JUMP_DIALOG (English (U.S.))]
Type=1
Class=CJumpDlg
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_ADDRESS,edit,1350631552

[DLG:IDD_SECTOR_DIALOG (English (U.S.))]
Type=1
Class=CSectorDlg
ControlCount=7
Control1=IDC_PREV,button,1342242944
Control2=IDC_NEXT,button,1342242944
Control3=IDC_GOTO,button,1342242817
Control4=IDC_STATIC,static,1342308864
Control5=IDC_LBA,edit,1350631552
Control6=IDC_TEXT,RICHEDIT,1342244868
Control7=IDC_LAST,button,1476460544

[DLG:IDD_SEARCH_DIALOG (English (U.S.))]
Type=1
Class=CSearchDlg
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_DIRECTION1,button,1342308361
Control4=IDC_DIRECTION2,button,1342177289
Control5=IDC_STATIC,button,1342177287

[DLG:IDD_EXTRACT_DIALOG (English (U.S.))]
Type=1
Class=CExtractDialog
ControlCount=9
Control1=IDC_TYPE1,button,1342308361
Control2=IDC_FOLDERS,button,1342251011
Control3=IDC_TYPE2,button,1342177289
Control4=IDC_TYPE3,button,1342177289
Control5=IDC_FILTER,button,1342242819
Control6=IDC_TYPE4,button,1342177289
Control7=IDOK,button,1342373889
Control8=IDCANCEL,button,1342242816
Control9=IDC_STATIC,button,1342177287

