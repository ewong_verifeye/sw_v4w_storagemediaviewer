// cfviewer.h : main header file for the CFVIEWER application
//

#if !defined(AFX_CFVIEWER_H__D07BFBD2_DF89_41BD_A435_35E64B63348D__INCLUDED_)
#define AFX_CFVIEWER_H__D07BFBD2_DF89_41BD_A435_35E64B63348D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCFViewerApp:
// See cfviewer.cpp for the implementation of this class
//

class CCFViewerApp : public CWinApp
{
public:
	CCFViewerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCFViewerApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCFViewerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CFVIEWER_H__D07BFBD2_DF89_41BD_A435_35E64B63348D__INCLUDED_)
