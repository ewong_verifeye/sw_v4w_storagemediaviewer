// cfviewerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "cfviewer.h"
#include "cfviewerDlg.h"
#include "v4w.h"
#include "ExtractDialog.h"
#include "WaveFile.h"
#include "VersionInfo.h"

#ifdef _DEBUG
#define DEBUG_CHECKSUM_HEARTBEAT		1
#define DEBUG_CHECKSUM_ERRORS			1
#define DEBUG_MEM_LEAKS					1	// Value greater than 0 to output memory leak info
#endif

#if DEBUG_MEM_LEAKS > 0
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

// Conditions that distinguishes between image types to process correctly (ie ten wavelet blocks vs eleven)
#define CFV_IMAGE_HALFRES		(entry->_data.image.type == WS_TYPE_WAVELET2_YUV || (entry->_data.image.type == WS_TYPE_WAVELET2_YUV_2 && entry->_data.image.width <= 360))
#define CFV_IMAGE_COLOR			(entry->_data.image.type == WS_TYPE_WAVELET2_YUV || (entry->_data.image.type == WS_TYPE_WAVELET2_YUV_2 && !entry->_data.image._data.wavelet2_yuv_2.blackwhite))
#define CFV_IMAGE_NTSC			(entry->_data.image.type == WS_TYPE_WAVELET2_YUV || (entry->_data.image.type == WS_TYPE_WAVELET2_YUV_2 && !entry->_data.image._data.wavelet2_yuv_2.pal_standard))

// Wavelet level (1-5) to display
#define DISPLAY_LEVEL_Y					5
#define DISPLAY_LEVEL_YUV				4

#define DISPLAY_OPTION_NORMAL			0
#define DISPLAY_OPTION_SHOW_ERRORS		1
#define DISPLAY_OPTION_SHOW_TRANSFORM	2

#define SWAP_WORD(x)				((((x) & 0xFF) << 8) | (((x) >> 8) & 0xFF))
#define SWAP_DWORD(x)				((SWAP_WORD((x)) << 16) | SWAP_WORD((x) >> 16))

#define CF_MAX_READ_SECTORS			128
#define CF_MAX_READ_LENGTH			CF_MAX_READ_SECTORS*IDE_SECTOR_SIZE

#define RAW_IMAGE_GRAB_FIX_BYTES	8

int BLOCK_QUANT_TABLE[3][11] = {{ 0, 0, 0, 0, 2, 2, 2, 3, 4, 3, 8 },
								{ 0, 0, 0, 0, 2, 2, 2, 3, 4, 3, 8 },
								{ 0, 0, 0, 0, 2, 2, 2, 3, 4, 3, 8 }};

const LPCTSTR EVENTS[V4W_EVENT_NUM_CYCLES+3] = {
	_T("Trig 1"),
	_T("Trig 2"),
	_T("Trig 3"),
	_T("Trig 4"),
	_T("Trig 5"),
	_T("Trig 6"),
	_T("G-Force Right Strong"),
	_T("G-Force Right Weak"),
	_T("G-Force Left Strong"),
	_T("G-Force Left Weak"),
	_T("G-Force Accel Strong"),
	_T("G-Force Accel Weak"),
	_T("G-Force Brake Strong"),
	_T("G-Force Brake Weak"),
	_T("Power"),
	_T("Reserved"),
	_T("Speed"),
};

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCFViewerDlg dialog

CCFViewerDlg::CCFViewerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCFViewerDlg::IDD, pParent),
	m_dlgFileOpen(TRUE, "v4w", NULL, OFN_HIDEREADONLY,
	"V4W video file (*.v4w)|*.v4w|CF Card Image (*.vcfi)|*.vcfi|All Files (*.*)|*.*|||"),
	m_dlgFileSave(FALSE, "v4w", NULL, OFN_OVERWRITEPROMPT,
	"V4W video file (*.v4w)|*.v4w|CF Card Image (*.vcfi)|*.vcfi|All Files (*.*)|*.*|||")
{
	//{{AFX_DATA_INIT(CCFViewerDlg)
	m_source = 0;
	m_filename = _T("");
	m_playmode = 0;
	m_status = _T("");
	m_statusData = _T("");
	m_displayYUV = 0;
	m_displayOptions = DISPLAY_OPTION_SHOW_ERRORS;
	//}}AFX_DATA_INIT
	m_info[0] = _T("");
	m_info[1] = _T("");
	m_info[2] = _T("");
	m_info[3] = _T("");
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_hFile = NULL;
	m_pImage[0] = NULL;
	m_pImage[1] = NULL;
	m_pImage[2] = NULL;
	m_pImage[3] = NULL;
	m_pHeader = (V4W_FILESYS_HEADER*)new BYTE[IDE_SECTOR_SIZE*2];	// Header is 128B, but we are reserving 1024B to take audio which makes it size of V4W_FILESYS_ENTRY which is 1024
	m_pEntry = (V4W_FILESYS_ENTRY*)m_pHeader;
	m_sector = 0;
	m_offset = 0;
	m_font.CreateFont(14, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, "Arial");
//	m_out = NULL;
	m_sortColumn = -1;
	m_sortAscending = false;
	m_pushWindow = 0;
	ResetStats();
	m_sFilePath.Empty();
	m_maxLBA = 0;
}

void CCFViewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCFViewerDlg)
	DDX_Control(pDX, IDC_SLIDER, m_sliderSector);
//	DDX_Control(pDX, IDC_GRAPH, m_buttonGraph);
	DDX_Control(pDX, IDC_EVENTS, m_events);
	DDX_Control(pDX, IDC_SECTOR, m_sectorStatic);
	DDX_Radio(pDX, IDC_SOURCE1, m_source);
	DDX_Text(pDX, IDC_FILENAME, m_filename);
	DDX_Radio(pDX, IDC_PLAY_MODE1, m_playmode);
	DDX_Text(pDX, IDC_STATUS, m_status);
	DDX_Text(pDX, IDC_STATUS2, m_statusData);
	DDX_CBIndex(pDX, IDC_COMBO_YUV, m_displayYUV);
	DDX_CBIndex(pDX, IDC_COMBO_DISPLAY_OPTIONS, m_displayOptions);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_IMAGE0, m_image[0]);
	DDX_Control(pDX, IDC_IMAGE1, m_image[1]);
	DDX_Control(pDX, IDC_IMAGE2, m_image[2]);
	DDX_Control(pDX, IDC_IMAGE3, m_image[3]);
	DDX_Text(pDX, IDC_INFO0, m_info[0]);
	DDX_Text(pDX, IDC_INFO1, m_info[1]);
	DDX_Text(pDX, IDC_INFO2, m_info[2]);
	DDX_Text(pDX, IDC_INFO3, m_info[3]);
}

BEGIN_MESSAGE_MAP(CCFViewerDlg, CDialog)
	//{{AFX_MSG_MAP(CCFViewerDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_PREV, OnPrev)
	ON_BN_CLICKED(IDC_REWIND, OnRewind)
	ON_BN_CLICKED(IDC_PAUSE, OnPause)
	ON_BN_CLICKED(IDC_PLAY, OnPlay)
	ON_BN_CLICKED(IDC_NEXT, OnNext)
	ON_BN_CLICKED(IDC_BROWSE, OnBrowse)
	ON_BN_CLICKED(IDC_OPEN, OnOpen)
	ON_BN_CLICKED(IDC_JUMP, OnJump)
	ON_BN_CLICKED(IDC_SEARCH_EVENTS, OnSearchEvents)
	ON_BN_CLICKED(IDC_VIEW_SECTOR, OnViewSector)
	ON_BN_CLICKED(IDC_EXTRACT_EVENT, OnExtractEvent)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_EVENTS, OnColumnclickEvents)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_EVENTS, OnItemchangedEvents)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER, OnCustomdrawSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER, OnReleasedcaptureSlider)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCFViewerDlg message handlers

BOOL CCFViewerDlg::OnInitDialog()
{
#if DEBUG_MEM_LEAKS > 0
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	SetWindowText(WINDOW_TITLE);

	AfxInitRichEdit();

	CRect r;
	m_image[0].GetWindowRect(&r);
	ScreenToClient(&r);
	r.right = r.left + 360;
	r.bottom = r.top + 288;
	m_image[0].MoveWindow(r);
	m_image[1].GetWindowRect(&r);
	ScreenToClient(&r);
	r.right = r.left + 360;
	r.bottom = r.top + 288;
	m_image[1].MoveWindow(r);

	((CStatic*)GetDlgItem(IDC_INFO0))->SetFont(&m_font);
	((CStatic*)GetDlgItem(IDC_INFO1))->SetFont(&m_font);
	((CStatic*)GetDlgItem(IDC_INFO2))->SetFont(&m_font);
	((CStatic*)GetDlgItem(IDC_INFO3))->SetFont(&m_font);

	m_events.InsertColumn(0, "Partial", LVCFMT_LEFT, 41, 0);
	m_events.InsertColumn(1, "Area", LVCFMT_LEFT, 35, 1);
	m_events.InsertColumn(2, "Address", LVCFMT_LEFT, 77, 2);
	m_events.InsertColumn(3, "Time", LVCFMT_LEFT, 127, 3);
	m_events.InsertColumn(4, "Type", LVCFMT_LEFT, 115, 4);
	m_events.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	m_bmPrev = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_PREV));
	((CWnd*)GetDlgItem(IDC_PREV))->SendMessage(BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)m_bmPrev);
	m_bmRewind = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_REWIND));
	((CWnd*)GetDlgItem(IDC_REWIND))->SendMessage(BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)m_bmRewind);
	m_bmPause = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_PAUSE));
	((CWnd*)GetDlgItem(IDC_PAUSE))->SendMessage(BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)m_bmPause);
	m_bmPlay = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_PLAY));
	((CWnd*)GetDlgItem(IDC_PLAY))->SendMessage(BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)m_bmPlay);
	m_bmNext = LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_NEXT));
	((CWnd*)GetDlgItem(IDC_NEXT))->SendMessage(BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)m_bmNext);

	m_buttonGraph.SetType(PLOT_TYPE_GFORCE_CIRCLE);
	m_sliderSector.SetRange(0x00, m_maxLBA);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCFViewerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCFViewerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

BYTE CCFViewerDlg::CalcChecksum(BYTE* buffer, int length)
{
	BYTE checksum = 0;
	while(length-- > 0) {
		checksum += *buffer++;
	}

	return checksum;
}

void CCFViewerDlg::SetStatus(LPCTSTR text)
{
	m_status.Format("Status: %s", text);
	UpdateData(FALSE);
}

void CCFViewerDlg::OnBrowse() 
{
//	In Vista & later cannot make more than one call to SetFileTypes, otherwise will crash.
//	Fix: create new instance of file dialog box
//	if(m_dlgFileOpen.DoModal() == IDOK) {

	CFileDialog fileDialog(
		TRUE, 
		"v4w", 
		NULL, 
		OFN_HIDEREADONLY,
		"V4W video file (*.v4w)|*.v4w|CF Card Image (*.vcfi)|*.vcfi|All Files (*.*)|*.*|||" );

	if(fileDialog.DoModal() == IDOK) {
		UpdateData();
		m_filename = fileDialog.m_ofn.lpstrFile;
		m_source = 1;
		UpdateData(FALSE);
	}
}

void CCFViewerDlg::OnOpen() 
{
	UpdateData();

	m_events.DeleteAllItems();
	GetDlgItem(IDC_EXTRACT_EVENT)->EnableWindow(FALSE);

	Open();
}

bool CCFViewerDlg::Open()
{
	bool bIsVCFI = FALSE;

	if(m_hFile != NULL && m_hFile != INVALID_HANDLE_VALUE) {
		CloseHandle(m_hFile);
	}
	m_hFile = NULL;
	m_file = (m_source == 1);
	m_maxLBA = 0;

	CString path;
	if(m_file) {
		path = m_filename;
		path.TrimLeft();
		path.TrimRight();

		if( m_filename.GetLength() > 5 ) {
			if ( m_filename.Right(4) == "vcfi" ) {
				bIsVCFI = TRUE;
			}
			else {
				bIsVCFI = FALSE;
			}
		}
	}
	else {
		FindFlashReader(path);
	}
	if(path.IsEmpty()) {
		return false;
	}

	m_hFile = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if(m_hFile == INVALID_HANDLE_VALUE) {
		CString msg;
		msg.Format("Unable to open %s", path);
		SetStatus(msg);
		return false;
	}

	if(m_file && !bIsVCFI ) {
		DWORD size = GetFileSize(m_hFile, NULL);
		size >>= 9;

		m_offset = 0;
		m_end = size;
		m_sector = 0;
		for(unsigned int i=0;i<size;i++) {
			if(ReadHeader()) {
				switch(m_pHeader->type) {
				case V4W_FILESYS_ENTRY_TYPE_EVENT:
					m_offset = m_pEntry->_data.event.next_data-1-m_sector;
					break;
				case V4W_FILESYS_ENTRY_TYPE_IMAGE:
					size = m_pEntry->header.size + sizeof(V4W_FILESYS_HEADER);
					size = ((size-1) >> 9) + 1;
					m_offset = m_pEntry->header.next-size-m_sector;
					break;
				case V4W_FILESYS_ENTRY_TYPE_AUDIO:
					m_offset = m_pEntry->header.next-2-m_sector;
					break;
				case V4W_FILESYS_ENTRY_TYPE_GFORCE:
					m_offset = m_pEntry->header.next-1-m_sector;
					break;
				}

				m_end += m_offset;
				m_sector += m_offset;
				if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_EVENT) {
					SearchEvents(true);
				}
				else {
					LVITEM item;
					item.mask = LVIF_PARAM;
					item.iItem = 0;
					item.iSubItem = 0;
					item.lParam = m_sector;
					int index = m_events.InsertItem(&item);
					item.mask = LVIF_TEXT;
					item.iItem = index;
					item.iSubItem = 2;
					CString str;
					str.Format("0x%.08X", m_sector);
					item.pszText = (LPTSTR)(LPCTSTR)str;
					m_events.SetItem(&item);
					item.iSubItem = 4;
					item.pszText = "<data>";
					m_events.SetItem(&item);
				}

				break;
			}
			m_sector++;
		}
	}
	else
	{
		m_pushWindow = 0;

		if(Read(V4W_CARD_CONFIG_LBA, sizeof(V4W_CARD_CONFIG), (BYTE*)&m_cardConfig) &&
			m_cardConfig.init == V4W_CARD_CONFIG_INIT_CHAR) {

			LVITEM item;
			item.mask = LVIF_PARAM;
			item.iItem = 0;
			item.iSubItem = 0;
			item.lParam = m_cardConfig.filesys.buffer.start;
			int index = m_events.InsertItem(&item);
			item.mask = LVIF_TEXT;
			item.iItem = index;
			item.iSubItem = 2;
			CString str;
			str.Format("0x%.08X", m_cardConfig.filesys.buffer.start);
			item.pszText = (LPTSTR)(LPCTSTR)str;
			m_events.SetItem(&item);
			item.iSubItem = 4;
			item.pszText = "<real time buffer>";
			m_events.SetItem(&item);
			if(Read(V4W_FRAM_MAP_LBA, sizeof(V4W_FRAM_MAP), (BYTE*)&m_framMap) &&
				m_framMap.init == V4W_FRAM_MAP_INIT_CHAR) {
				for(int i=0;i<V4W_FILESYS_NUM_STORAGE_AREAS;i++) {
					m_sector = (m_framMap.card.filesys.storage[i].status & V4W_FILESYS_STATUS_USE_BACKUP) ?
						m_framMap.card.filesys.storage[i].backup.ptr.prev_event :
						m_framMap.card.filesys.storage[i].orig.ptr.prev_event;
					if(ReadHeader()) {
						SearchEvents(false);
					}
				}
				if(m_framMap.card.ide.max_lba == 0xFFFFFFFF) {
					m_maxLBA = 0;
				}
				else {
					m_maxLBA = m_framMap.card.ide.max_lba;
				}
			}
			else {
				for(int i=0;i<V4W_FILESYS_NUM_STORAGE_AREAS;i++) {
					m_sector = m_cardConfig.filesys.storage[i].start;
					if(ReadHeader()) {
						SearchEvents(true);
					}
				}
			}
		}
		if(m_maxLBA == 0) {
			AfxMessageBox("Bad card size read.");
		}
	}
	m_sliderSector.SetRange(0x00, m_maxLBA);
	return true;
}

void CCFViewerDlg::FindFlashReader(CString& path)
{
	HDEVINFO hDevInfo = SetupDiGetClassDevs((LPGUID)&DiskClassGuid, NULL, NULL,
		(DIGCF_INTERFACEDEVICE | DIGCF_PRESENT));
	if(hDevInfo == INVALID_HANDLE_VALUE) {
		path.Empty();
		return;
	}

	SP_DEVICE_INTERFACE_DATA spdid;
    spdid.cbSize = sizeof(SP_INTERFACE_DEVICE_DATA);
	DWORD index = 0;
	m_dlgReader.ResetContent();
	while(1) {
		if(!SetupDiEnumDeviceInterfaces(hDevInfo, NULL, (LPGUID)&DiskClassGuid,
			index, &spdid) && GetLastError() == ERROR_NO_MORE_ITEMS) {
			break;
		}

		DWORD size;
		SetupDiGetDeviceInterfaceDetail(hDevInfo, &spdid, NULL, 0, &size, NULL);
		PSP_DEVICE_INTERFACE_DETAIL_DATA pspdidd;
		pspdidd = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(size);
		pspdidd->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);
		SetupDiGetDeviceInterfaceDetail(hDevInfo, &spdid, pspdidd, size, NULL, NULL);

		m_dlgReader.AddString(pspdidd->DevicePath);
		free(pspdidd);
		index++;
	}

    SetupDiDestroyDeviceInfoList(hDevInfo);

	if(m_dlgReader.DoModal() == IDOK) {
		path = m_dlgReader.m_sel;
	}
	else {
		path.Empty();
	}
}

bool CCFViewerDlg::Read(DWORD lba, DWORD size, BYTE* buffer)
{
	if(m_hFile == NULL || m_hFile == INVALID_HANDLE_VALUE) {
		return false;
	}

	if(m_file) {
		DWORD sectors = ((size-1) >> 9) + 1;
		if(lba < m_offset || lba + sectors > m_end) {
			return false;
		}
		lba -= m_offset;
	}

	while(size > 0) {
		LARGE_INTEGER i;
		i.QuadPart = (__int64)lba*IDE_SECTOR_SIZE;
		DWORD result = SetFilePointer(m_hFile, i.LowPart, &i.HighPart, FILE_BEGIN);
		if(result == 0xFFFFFFFF && GetLastError() != NO_ERROR) {
			return false;
		}

		DWORD bytesToRead;
		if(m_file) {
			bytesToRead = size;
		}
		else {
			bytesToRead = size >= CF_MAX_READ_LENGTH ? CF_MAX_READ_LENGTH : size;
		}
		DWORD bytesRead;
		if(bytesToRead % IDE_SECTOR_SIZE != 0) {
			DWORD n = (((bytesToRead-1)/IDE_SECTOR_SIZE)+1)*IDE_SECTOR_SIZE;
			BYTE* b = new BYTE[n];
			if(ReadFile(m_hFile, b, n, &bytesRead, NULL) && bytesRead == n) {
				memcpy(buffer, b, bytesToRead);
				result = 1;
			}
			else {
				result = 0;
			}
			delete b;
		}
		else {
			if(ReadFile(m_hFile, buffer, bytesToRead, &bytesRead, NULL) &&
				bytesRead == bytesToRead) {
				result = 1;
			}
			else {
				result = 0;
			}
		}

		if(!result) {
			LPVOID msg;
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
				FORMAT_MESSAGE_IGNORE_INSERTS |
				FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), 0, (LPTSTR)&msg, 0, NULL);
			SetStatus((LPCTSTR)msg);
			LocalFree(msg);
			return false;
		}

		size -= bytesToRead;
		lba += bytesToRead/IDE_SECTOR_SIZE;
		buffer += bytesToRead;
	}

	return true;
}

bool CCFViewerDlg::ReadHeader()
{
	if(m_sector != m_maxLBA) {
		// Read 1024 bytes so that we also get Audio
		if(!Read(m_sector, IDE_SECTOR_SIZE*2, (BYTE*)m_pHeader)) {
			return false;
		}
	}
	else {
		if(!Read(m_sector, IDE_SECTOR_SIZE, (BYTE*)m_pHeader)) {
			return false;
		}
	}

	if(m_pHeader->magic != V4W_FILESYS_HEADER_MAGIC) {
		return false;
	}

	if(m_pHeader->checksum != CalcChecksum((BYTE*)m_pHeader, sizeof(V4W_FILESYS_HEADER)-1)) {
		return false;
	}

	return true;
}

bool CCFViewerDlg::SearchHeader(bool forward, bool event)
{
	int tries = 0;
	// try searching in 0.5MB of data
	while(tries < 1024) {
		if(ReadHeader()) {
			if(event && m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_EVENT) {
				return true;
			}
			else if(!event && m_pHeader->type != V4W_FILESYS_ENTRY_TYPE_EVENT) {
				return true;
			}
		}

		if(tries%50 == 0) {
			// reduce updating to avoid GUI freezing
			UpdateSlider();
		}

		if(forward) {
/*			if(m_sector >= m_maxLBA && m_maxLBA != 0 && m_maxLBA != 0xFFFFFFFF) {
				// AfxMessageBox("End of card reached.");
				return false;
			}
*/
			IncrementSector();
		}
		else {
/*			if(m_sector == 0x00) {
				// AfxMessageBox("Beginning of card reached.");
				return false;
			}
*/
			DecrementSector();
		}
		tries++;
	}

	return false;
}

bool CCFViewerDlg::ReadImage()
{
	int camera = m_pHeader->source;
	if(camera == 0x10) {
		camera = 1;
		if(m_pushWindow < 1) {
			m_pushWindow = 1;
		}
	}
	else if(camera == 0x20) {
		camera = 2;
		if(m_pushWindow < 2) {
			m_pushWindow = 2;
		}
	}
	else if(camera == 0x30) {
		camera = 3;
		if(m_pushWindow < 3) {
			m_pushWindow = 3;
		}
	}
	else if(m_pushWindow && (camera==1 || camera==2 || camera==3)) {
		camera = camera + m_pushWindow;
	}
	
	// if out of bounds, display in the highest camera view
	if(camera < 0 || camera >= 4) {
		camera = 3;
	}
	CWaveletStatic* pImage = &m_image[camera];
	pImage->SetWidth(0);
	pImage->SetHeight(0);

	CTime t = GetEntryTime();
	CString g = GetEntryGPS();
	CString str;
	CString h;
	CString sType = GetImageType(m_pEntry->_data.image.type);

	CString sYUV2 = "";
	if (m_pEntry->_data.image.type == WS_TYPE_WAVELET2_YUV_2) {
		sYUV2.Format("\n%s %s %s, %s", 
			m_pEntry->_data.image._data.wavelet2_yuv_2.pal_standard ? "PAL":"NTSC",
			m_pEntry->_data.image._data.wavelet2_yuv_2.blackwhite ? "BW":"Color",
			m_pEntry->_data.image._data.wavelet2_yuv_2.image_full_resolution ? "Full Res":"Half Res",
			m_pEntry->_data.image._data.wavelet2_yuv_2.oddeven ? "Field 2":"Field 1");
	}

	// Display size of compressed data for Y U & V
	CString sYUVsizes;
	int nSizeY = 0, nSizeU = 0, nSizeV = 0;
	GetImageSize(m_pEntry, nSizeY, nSizeU, nSizeV);
	sYUVsizes.Format("Y[%d] U[%d] V[%d]", nSizeY, nSizeU, nSizeV);

	h.Format("%dx%d %s%s\n%s", m_pEntry->_data.image.width, m_pEntry->_data.image.height, sType, sYUV2, sYUVsizes);

	if(camera >= 2 || m_image[2].IsWindowVisible()) {
		int i = g.FindOneOf("WE");
		g.SetAt(i+1, _T('\n'));
		g.Delete(i+2);
		str.Format("%s.%02d %s\n0x%.08X %d (%X)\n%s\n\n%s", t.Format(_T("%b %d, %Y %I:%M:%S")), m_pHeader->time.hundredths,
			t.Format(_T("%p")), m_sector, m_pHeader->size, ((m_pHeader->size+sizeof(V4W_FILESYS_HEADER)-1)/512)+1, g, h);
	}
	else {
		str.Format("%s.%02d %s 0x%.08X %d (%X)\n%s\n\n%s", t.Format(_T("%b %d, %Y %I:%M:%S")), m_pHeader->time.hundredths,
			t.Format(_T("%p")), m_sector, m_pHeader->size, ((m_pHeader->size+sizeof(V4W_FILESYS_HEADER)-1)/512)+1, g, h);
	}
	m_info[camera] = str;
//	if(m_out) {
//		fprintf(m_out, "Cam %d: %s\n", camera, str);
//	}
	UpdateData(FALSE);

	DWORD end = m_pHeader->next;
	if(end < m_sector) {
		if(m_file) {
			SetStatus("Cannot handle wraparound.");
			return false;
		}
	}

	DWORD size = sizeof(V4W_FILESYS_HEADER) + m_pEntry->header.size;
	if(m_pImage[camera] != NULL) {
		delete m_pImage[camera];
	}

	m_pImage[camera] = (V4W_FILESYS_ENTRY*)new BYTE[size];
	if(end < m_sector) {
		int area = GetArea(m_sector);
		DWORD size1;
		if(area == -1) {
			size1 = (m_cardConfig.filesys.buffer.end-m_sector+1)*IDE_SECTOR_SIZE;
		}
		else {
			size1 = (m_cardConfig.filesys.storage[area].end-m_sector+1)*IDE_SECTOR_SIZE;
		}
		if(!Read(m_sector, size1, (BYTE*)m_pImage[camera])) {
			return false;
		}
		DWORD start;
		if(area == -1) {
			start = m_cardConfig.filesys.buffer.start;
		}
		else {
			start = m_cardConfig.filesys.storage[area].start;
		}
		if(!Read(start, size-size1, ((BYTE*)m_pImage[camera])+size1)) {
			return false;
		}
	}
	else {
		if(!Read(m_sector, size, (BYTE*)m_pImage[camera])) {
			return false;
		}
	}

#ifdef _DEBUG
//	HANDLE hFile;
//	CString filename;
//	filename.Format("img%.04X.bin", m_sector);
//	hFile = CreateFile(filename, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
//	if(hFile != INVALID_HANDLE_VALUE) {
//		DWORD d;
//		WriteFile(hFile, m_pImage[camera], size, &d, NULL);
//		CloseHandle(hFile);
//	}
#endif

	CString sError = "";
	if(!ShowImage(m_pImage[camera], sError)) {
		return false;
	}
	m_info[camera] += "\n";
	m_info[camera] += sError;
	UpdateData(FALSE);

//	CRect r;
//	pImage->GetWindowRect(&r);
//	ScreenToClient(&r);
//	r.SetRect(r.left, r.top, r.left+m_pImage[m_pHeader->source]->_data.image.width,
//		r.top+m_pImage[m_pHeader->source]->_data.image.height);
//	pImage->MoveWindow(&r, FALSE);

	UpdateSlider();
		
	return true;
}

CTime CCFViewerDlg::GetEntryTime()
{
	V4W_TIME* t = &m_pHeader->time;
	SYSTEMTIME st;
	st.wYear = 2000+t->year;
	st.wMonth = t->month;
	st.wDay = t->date;
	st.wHour = t->hours;
	st.wMinute = t->minutes;
	st.wSecond = t->seconds;
	st.wMilliseconds = 0;

	// 17 Feb 2007: EDW
	// Fix for GPS Issue: Old firmware in the Nemerix GPS from Taiwan
	// Examine the UTC Timestamp and apply the correction if the Day field is zero.

	if ( st.wDay == 0  &&  st.wYear >= 2000 && st.wMonth > 0 && st.wMonth <= 12 )
	{
		// Set Day to 1 using the current month
		CTime timeStamp = CTime( st.wYear, st.wMonth, 1, st.wHour,0,0);

		// Subtract 1 day: 24 hours
		CTimeSpan ts = CTimeSpan(1,0,0,0); 

		timeStamp -= ts;
		st.wYear = timeStamp.GetYear();
		st.wMonth = timeStamp.GetMonth();
		st.wDay = timeStamp.GetDay();
		st.wHour = timeStamp.GetHour();
	}

	FILETIME ft1;
	SystemTimeToFileTime(&st, &ft1);
	FILETIME ft2;
	FileTimeToLocalFileTime(&ft1, &ft2);
	FileTimeToSystemTime(&ft2, &st);
	return CTime(st);
}

int CCFViewerDlg::GetEntryMilliseconds()
{
	V4W_TIME* t = &m_pHeader->time;
	return 10*t->hundredths;
}

CString CCFViewerDlg::GetImageType(BYTE type)
{
	switch(type) {
		case V4W_IMAGE_TYPE_RAW_Y:
			return "B&W Raw v1";
		case V4W_IMAGE_TYPE_WAVELET_Y:
			return "B&W v1";
		case V4W_IMAGE_TYPE_RAW_YUV:
			return "Color Raw v1";
		case V4W_IMAGE_TYPE_WAVELET_YUV:
			return "Color v1";
		case V4W_IMAGE_TYPE_WAVELET2_Y:
			return "B&W";
		case V4W_IMAGE_TYPE_WAVELET2_YUV:
			return "Color v2";
		case V4W_IMAGE_TYPE_WAVELET2_YUV_2:
			return "V4600v2+";
		default:
			return "Unknown Type";
	}
}

CString CCFViewerDlg::GetEntryHeaderStr(V4W_FILESYS_ENTRY* entry)
{
	V4W_TIME* t = &entry->header.time;
	CString st, ret;
	st.Format("%d-%02d-%02d %02d:%02d:%02d.%03d", 2000+t->year, t->month, t->date, t->hours, t->minutes, t->seconds, t->hundredths*10);
	return st;

//	Return time with sector
//	ret.Format("[0x%08X %s]", m_sector, st);
//	return ret;
}

void CCFViewerDlg::GetImageSize(V4W_FILESYS_ENTRY* entry, int &Y, int &U, int &V)
{
	int wavelet_levels = V4W_IMAGE_WAVELET_LEVELS_YUV;

	switch(entry->_data.image.type) {
		case V4W_IMAGE_TYPE_RAW_Y:
			Y = 0;
			U = -1;
			V = -1;
			return;
		case V4W_IMAGE_TYPE_RAW_YUV:
			Y = 0;
			U = 0;
			V = 0;
			return;
		case V4W_IMAGE_TYPE_WAVELET_Y:
		case V4W_IMAGE_TYPE_WAVELET2_Y:
			Y = entry->_data.image._data.wavelet_y.level[V4W_IMAGE_WAVELET_LEVELS-1].size;
			U = -1;
			V = -1;
			return;
		case V4W_IMAGE_TYPE_WAVELET_YUV:
		case V4W_IMAGE_TYPE_WAVELET2_YUV:
			Y = entry->_data.image._data.wavelet_yuv.level[0][V4W_IMAGE_WAVELET_LEVELS_YUV-1].size;
			U = entry->_data.image._data.wavelet_yuv.level[1][V4W_IMAGE_WAVELET_LEVELS_YUV-1].size;
			V = entry->_data.image._data.wavelet_yuv.level[2][V4W_IMAGE_WAVELET_LEVELS_YUV-1].size;
			return;
		case V4W_IMAGE_TYPE_WAVELET2_YUV_2:
			if (entry->_data.image._data.wavelet2_yuv_2.image_full_resolution) {
				wavelet_levels = V4W_IMAGE_WAVELET_LEVELS;
			}

			if (entry->_data.image._data.wavelet2_yuv_2.blackwhite) {
				Y = entry->_data.image._data.wavelet2_yuv_2.level[0][wavelet_levels-1].size;
				U = -1;
				V = -1;
			}
			else {
				Y = entry->_data.image._data.wavelet2_yuv_2.level[0][wavelet_levels-1].size;
				U = entry->_data.image._data.wavelet2_yuv_2.level[1][wavelet_levels-1].size;
				V = entry->_data.image._data.wavelet2_yuv_2.level[2][wavelet_levels-1].size;
			}
			return;
		default:
			Y = -1;
			U = -1;
			V = -1;
			return;
	}
}


CString CCFViewerDlg::GetEntryGPS()
{
	struct {
		int deg;
		int min;
		float sec;
		char hem;
	} lat, lon;

	lat.hem = (m_pHeader->gps.latitude >= 0) ? 'N' : 'S';
	m_pHeader->gps.latitude = (float)fabs(m_pHeader->gps.latitude);
	m_pHeader->gps.latitude /= 100.0;
	lat.deg = (int)m_pHeader->gps.latitude;
	m_pHeader->gps.latitude -= lat.deg;
	m_pHeader->gps.latitude *= 60;
	lat.min = (int)m_pHeader->gps.latitude;
	m_pHeader->gps.latitude -= lat.min;
	m_pHeader->gps.latitude *= 60;
	lat.sec = m_pHeader->gps.latitude;

	lon.hem = (m_pHeader->gps.longitude >= 0) ? 'E' : 'W';
	m_pHeader->gps.longitude = (float)fabs(m_pHeader->gps.longitude);
	m_pHeader->gps.longitude /= 100.0;
	lon.deg = (int)m_pHeader->gps.longitude;
	m_pHeader->gps.longitude -= lon.deg;
	m_pHeader->gps.longitude *= 60;
	lon.min = (int)m_pHeader->gps.longitude;
	m_pHeader->gps.longitude -= lon.min;
	m_pHeader->gps.longitude *= 60;
	lon.sec = m_pHeader->gps.longitude;

	float kph = (float)(m_pHeader->gps.speed*1.852);

	CString str;
	str.Format("%d\xB0%.02d'%.2f\" %c, %d\xB0%.02d'%.2f\" %c, %.2f kph, %.2f\xB0",
		lat.deg, lat.min, lat.sec, lat.hem, lon.deg, lon.min, lon.sec, lon.hem,
		kph, m_pHeader->gps.heading);

	return str;
}

bool CCFViewerDlg::ShowImage(V4W_FILESYS_ENTRY* entry, CString &sError)
{
	CString sTmp="";
	bool error = false;
	int camera = m_pHeader->source;
	if(camera == 0x10) {
		camera = 1;
	}
	else if(camera == 0x20) {
		camera = 2;
	}
	else if(camera == 0x30) {
		camera = 3;
	}
	else if(m_pushWindow && (camera==1 || camera==2 || camera==3)) {
		camera = camera + m_pushWindow;
	}

	if(!m_image[2].IsWindowVisible()) {
		if (CFV_IMAGE_COLOR) // 352 width
		{
			if(CFV_IMAGE_NTSC) {
				// set to 352 :240 ratio for NTSC Color
				m_image[0].SetWindowPos(NULL, 0, 0, 360, 245, SWP_NOMOVE);
				m_image[1].SetWindowPos(NULL, 0, 0, 360, 245, SWP_NOMOVE);
			}
			else {
				// set to 352 :288 ratio for PAL Color
				m_image[0].SetWindowPos(NULL, 0, 0, 360, 295, SWP_NOMOVE);
				m_image[1].SetWindowPos(NULL, 0, 0, 360, 295, SWP_NOMOVE);
			}
		}
		else	// 360 width
		{
			if(CFV_IMAGE_NTSC) {
				// set to 360 :240 ratio for NTSC B&W
				m_image[0].SetWindowPos(NULL, 0, 0, 360, 240, SWP_NOMOVE);
				m_image[1].SetWindowPos(NULL, 0, 0, 360, 240, SWP_NOMOVE);
			}
			else {
				// set to 360 :288 ratio for PAL B&W
				m_image[0].SetWindowPos(NULL, 0, 0, 360, 288, SWP_NOMOVE);
				m_image[1].SetWindowPos(NULL, 0, 0, 360, 288, SWP_NOMOVE);
			}
		}
	}

	// if out of bounds, display in the highest camera view
	if(camera < 0 || camera >= 4) {
		camera = 3;
	}
	CWaveletStatic* pImage = &m_image[camera];
	pImage->SetWidth(entry->_data.image.width);
	pImage->SetHeight(entry->_data.image.height);
	pImage->SetDisplayYUV(m_displayYUV);
	pImage->SetDisplayTransform( m_displayOptions == DISPLAY_OPTION_SHOW_TRANSFORM ? true:false );
	pImage->SetShowErrors( m_displayOptions == DISPLAY_OPTION_SHOW_ERRORS ? true:false );

	if(entry->_data.image.type == V4W_IMAGE_TYPE_RAW_Y) {
		pImage->SetData(entry->_data.image._data.raw_y._data, WS_TYPE_RAW_Y,
			entry->_data.image.width*entry->_data.image.height);
		pImage->SetDisplayMode(0);
	}
	else if(entry->_data.image.type == V4W_IMAGE_TYPE_WAVELET_Y ||
		entry->_data.image.type == V4W_IMAGE_TYPE_WAVELET2_Y) {
		for(int i=0;i<DISPLAY_LEVEL_Y;i++) {

			int nSize = entry->_data.image._data.wavelet_y.level[i].size;
			if ( nSize > 0x100000) nSize -= 0x100000;

			if(entry->_data.image._data.wavelet_y.level[i].checksum !=
				CalcChecksum( entry->_data.image._data.wavelet_y._data, nSize ) ) {
				sError = "BAD WAVELET CHECKSUM.";
				SetStatus("BAD WAVELET CHECKSUM.");
				error = true;
				// do not return, the image should be displayed for troubleshooting
				sTmp.Format(
					"0x%X BAD WAVELET CHECKSUM in Y%d 0x%02X vs 0x%02X (calculated)\n",
					m_sector,
					i, 
					entry->_data.image._data.wavelet_y.level[i].checksum,
					CalcChecksum( entry->_data.image._data.wavelet_y._data, nSize )
					);
#if DEBUG_CHECKSUM_ERRORS
				TRACE(sTmp);
#endif
			}
		}

		if(error) {
			m_stats.image.bad++;
		}

		if(entry->_data.image.type == V4W_IMAGE_TYPE_WAVELET_Y) {
			pImage->SetData(entry->_data.image._data.wavelet_y._data,
				WS_TYPE_WAVELET_Y, entry->_data.image._data.wavelet_y.level[DISPLAY_LEVEL_Y-1].size);
		}
		else {
			pImage->SetData(entry->_data.image._data.wavelet_y._data,
				WS_TYPE_WAVELET2_Y, entry->_data.image._data.wavelet_y.level[DISPLAY_LEVEL_Y-1].size);
		}
		for(int i=0;i<V4W_IMAGE_WAVELET_BLOCKS;i++) {
			BLOCK_QUANT_TABLE[0][i] = entry->_data.image._data.wavelet_y.quant[i];
		}
		pImage->SetQuantTable(BLOCK_QUANT_TABLE[0]);
		pImage->SetQuantLevel(0);
		pImage->SetDisplayMode(DISPLAY_LEVEL_Y);
	}
	else if(entry->_data.image.type == V4W_IMAGE_TYPE_RAW_YUV) {
		pImage->SetData(entry->_data.image._data.raw_yuv._data, 
			WS_TYPE_RAW_YUV,
			entry->_data.image.width*entry->_data.image.height*2 + RAW_IMAGE_GRAB_FIX_BYTES*2,
			entry->header.size - (sizeof(entry->_data.image) - sizeof(entry->_data.image._data))
			- entry->_data.image.width*entry->_data.image.height*3 - RAW_IMAGE_GRAB_FIX_BYTES*2,
			entry->_data.image.width*entry->_data.image.height );
		pImage->SetDisplayMode(0);
	}
	else if(entry->_data.image.type == V4W_IMAGE_TYPE_WAVELET2_YUV) {
		for(int j=0;j<DISPLAY_LEVEL_YUV;j++) {
			if(entry->_data.image._data.wavelet_yuv.level[0][j].checksum !=
				CalcChecksum(entry->_data.image._data.wavelet_yuv._data,
				entry->_data.image._data.wavelet_yuv.level[0][j].size)) {
				sTmp.Format("Y%d", j);
				sError += sTmp;
				error = true;
				// do not return, the image should be displayed for troubleshooting
				sTmp.Format(
					"%s BAD WAVELET CHECKSUM in Y%d 0x%02X vs 0x%02X (calculated)\n",
					GetEntryHeaderStr(entry),
					j, 
					entry->_data.image._data.wavelet_yuv.level[0][j].checksum,
					CalcChecksum(entry->_data.image._data.wavelet_yuv._data, 
						entry->_data.image._data.wavelet_yuv.level[0][j].size)
					);
#if DEBUG_CHECKSUM_ERRORS
				TRACE(sTmp);
#endif
			}
			if(entry->_data.image._data.wavelet_yuv.level[1][j].checksum !=
				CalcChecksum(entry->_data.image._data.wavelet_yuv._data + 
				entry->_data.image._data.wavelet_yuv.level[0][V4W_IMAGE_WAVELET_LEVELS_YUV-1].size,
				entry->_data.image._data.wavelet_yuv.level[1][j].size)) {
				sTmp.Format("U%d", j);
				sError += sTmp;
				error = true;
				// do not return, the image should be displayed for troubleshooting
				sTmp.Format(
					"%s BAD WAVELET CHECKSUM in U%d 0x%02X vs 0x%02X (calculated)\n", 
					GetEntryHeaderStr(entry),
					j, 
					entry->_data.image._data.wavelet_yuv.level[1][j].checksum,
					CalcChecksum(entry->_data.image._data.wavelet_yuv._data + 
						entry->_data.image._data.wavelet_yuv.level[0][V4W_IMAGE_WAVELET_LEVELS_YUV-1].size,
						entry->_data.image._data.wavelet_yuv.level[1][j].size)
					);
#if DEBUG_CHECKSUM_ERRORS
				TRACE(sTmp);
#endif
			}
			if(entry->_data.image._data.wavelet_yuv.level[2][j].checksum !=
				CalcChecksum(entry->_data.image._data.wavelet_yuv._data +
				entry->_data.image._data.wavelet_yuv.level[0][V4W_IMAGE_WAVELET_LEVELS_YUV-1].size +
				entry->_data.image._data.wavelet_yuv.level[1][V4W_IMAGE_WAVELET_LEVELS_YUV-1].size,
				entry->_data.image._data.wavelet_yuv.level[2][j].size)) {
				sTmp.Format("V%d", j);
				sError += sTmp;
				error = true;
				// do not return, the image should be displayed for troubleshooting
				sTmp.Format(
					"%s BAD WAVELET CHECKSUM in V%d 0x%02X vs 0x%02X (calculated)\n", 
					GetEntryHeaderStr(entry),
					j, 
					entry->_data.image._data.wavelet_yuv.level[2][j].checksum,
					CalcChecksum(entry->_data.image._data.wavelet_yuv._data +
						entry->_data.image._data.wavelet_yuv.level[0][V4W_IMAGE_WAVELET_LEVELS_YUV-1].size +
						entry->_data.image._data.wavelet_yuv.level[1][V4W_IMAGE_WAVELET_LEVELS_YUV-1].size,
						entry->_data.image._data.wavelet_yuv.level[2][j].size)
					);
#if DEBUG_CHECKSUM_ERRORS
				TRACE(sTmp);
#endif
			}

		}

		if(error) {
			sError = "BAD WAVELET:" + sError;
			SetStatus(sError);
			m_stats.image.bad++;

			// Avoid seeing green images when U or V have checksum errors by displaying Y only, unless specifying single component
			if (m_displayOptions != 1 && m_displayYUV == 0) { pImage->SetDisplayYUV(1); }	
		}
	
		pImage->SetData(entry->_data.image._data.wavelet_yuv._data,
			WS_TYPE_WAVELET2_YUV, 
			entry->_data.image._data.wavelet_yuv.level[0][DISPLAY_LEVEL_YUV-1].size,
			entry->_data.image._data.wavelet_yuv.level[1][DISPLAY_LEVEL_YUV-1].size,
			entry->_data.image._data.wavelet_yuv.level[2][DISPLAY_LEVEL_YUV-1].size);
		
		for(int i=0;i<3;i++) {
			for(int j=0;j<V4W_IMAGE_WAVELET_BLOCKS_YUV;j++) {
				BLOCK_QUANT_TABLE[i][j] = entry->_data.image._data.wavelet_yuv.quant[i][j];
			}
		}
		pImage->SetQuantTable(BLOCK_QUANT_TABLE[0]);
		pImage->SetQuantLevel(0);
		pImage->SetDisplayMode(DISPLAY_LEVEL_YUV);
	}
	else if(entry->_data.image.type == V4W_IMAGE_TYPE_WAVELET2_YUV_2) {

		bool bColor;		// True if image is color
		int nYUV;			// Number of YUV components used
		int nLevels;		// Number of wavelet levels
		int nBlocks;		// Number of wavelet blocks
		int nDisplayLevel;	// Display level to show. Same as nLevels when displaying maximum quality 

		// Differentiate Color from Black & White images.
		if(entry->_data.image._data.wavelet2_yuv_2.blackwhite) {
			bColor	= false;
			nYUV	= 1;
		}
		else {
			bColor	= true;
			nYUV	= 3;
		}

		// Differentiate Wavelet Types. Full resolution uses 5 levels, 11 blocks, Half resolution uses 4 levels, 10 blocks.
		if(entry->_data.image._data.wavelet2_yuv_2.image_full_resolution) {
			nLevels			= V4W_IMAGE_WAVELET_LEVELS;
			nBlocks			= V4W_IMAGE_WAVELET_BLOCKS;
			nDisplayLevel	= DISPLAY_LEVEL_Y;
		}
		else {	// half width images lack final horizontal wavelet pass
			nLevels			= V4W_IMAGE_WAVELET_LEVELS_YUV;
			nBlocks			= V4W_IMAGE_WAVELET_BLOCKS_YUV;
			nDisplayLevel	= DISPLAY_LEVEL_YUV;
		}

		for(int j=0;j<nDisplayLevel;j++) {

			if (bColor) {
				if(entry->_data.image._data.wavelet2_yuv_2.level[0][j].checksum !=
					CalcChecksum(entry->_data.image._data.wavelet2_yuv_2._data,
					entry->_data.image._data.wavelet2_yuv_2.level[0][j].size)) {
					sTmp.Format("Y%d", j);
					sError += sTmp;
					error = true;
					// do not return, the image should be displayed for troubleshooting
					sTmp.Format(
						"%s BAD WAVELET CHECKSUM in Y%d 0x%02X vs 0x%02X (calculated)\n", 
						GetEntryHeaderStr(entry),
						j, 
						entry->_data.image._data.wavelet2_yuv_2.level[0][j].checksum,
						CalcChecksum(entry->_data.image._data.wavelet2_yuv_2._data,
							entry->_data.image._data.wavelet2_yuv_2.level[0][j].size)
						);
#if DEBUG_CHECKSUM_ERRORS
					TRACE(sTmp);
#endif
				}
				if(entry->_data.image._data.wavelet2_yuv_2.level[1][j].checksum !=
					CalcChecksum(entry->_data.image._data.wavelet2_yuv_2._data + 
					entry->_data.image._data.wavelet2_yuv_2.level[0][nLevels-1].size,
					entry->_data.image._data.wavelet2_yuv_2.level[1][j].size)) {
					sTmp.Format("U%d", j);
					sError += sTmp;
					error = true;
					// do not return, the image should be displayed for troubleshooting
					sTmp.Format(
						"%s BAD WAVELET CHECKSUM in U%d 0x%02X vs 0x%02X (calculated)\n", 
						GetEntryHeaderStr(entry),
						j, 
						entry->_data.image._data.wavelet2_yuv_2.level[1][j].checksum,
						CalcChecksum(entry->_data.image._data.wavelet2_yuv_2._data + 
							entry->_data.image._data.wavelet2_yuv_2.level[0][nLevels-1].size,
							entry->_data.image._data.wavelet2_yuv_2.level[1][j].size)
						);
#if DEBUG_CHECKSUM_ERRORS
					TRACE(sTmp);
#endif
				}
				if(entry->_data.image._data.wavelet2_yuv_2.level[2][j].checksum !=
					CalcChecksum(entry->_data.image._data.wavelet2_yuv_2._data +
					entry->_data.image._data.wavelet2_yuv_2.level[0][nLevels-1].size +
					entry->_data.image._data.wavelet2_yuv_2.level[1][nLevels-1].size,
					entry->_data.image._data.wavelet2_yuv_2.level[2][j].size)) {
					sTmp.Format("V%d", j);
					sError += sTmp;
					error = true;
					// do not return, the image should be displayed for troubleshooting
					sTmp.Format(
						"%s BAD WAVELET CHECKSUM in V%d 0x%02X vs 0x%02X (calculated)\n", 
						GetEntryHeaderStr(entry),
						j, 
						entry->_data.image._data.wavelet2_yuv_2.level[2][j].checksum,
						CalcChecksum(entry->_data.image._data.wavelet2_yuv_2._data +
							entry->_data.image._data.wavelet2_yuv_2.level[0][nLevels-1].size +
							entry->_data.image._data.wavelet2_yuv_2.level[1][nLevels-1].size,
							entry->_data.image._data.wavelet2_yuv_2.level[2][j].size)
						);
#if DEBUG_CHECKSUM_ERRORS
					TRACE(sTmp);
#endif
				}
			}
			else {
				int nSize = entry->_data.image._data.wavelet2_yuv_2.level[0][j].size;
				if ( nSize > 0x100000) nSize -= 0x100000;
			
				if(entry->_data.image._data.wavelet2_yuv_2.level[0][j].checksum !=
					CalcChecksum( entry->_data.image._data.wavelet2_yuv_2._data, nSize ) ) {
					sError = "BAD WAVELET CHECKSUM.";
					SetStatus("BAD WAVELET CHECKSUM.");
					error = true;
					// do not return, the image should be displayed for troubleshooting
					sTmp.Format(
						"%s BAD WAVELET CHECKSUM in Y%d 0x%02X vs 0x%02X (calculated)\n", 
						GetEntryHeaderStr(entry),
						j, 
						entry->_data.image._data.wavelet2_yuv_2.level[0][j].checksum,
						CalcChecksum( entry->_data.image._data.wavelet2_yuv_2._data, nSize )
						);
#if DEBUG_CHECKSUM_ERRORS
					TRACE(sTmp);
#endif
				}
			}
		}

		if(error) {
			sError = "BAD WAVELET:" + sError;
			SetStatus(sError);
			m_stats.image.bad++;

			// Avoid seeing green images when U or V have checksum errors by displaying Y only, unless specifying single component
			if (m_displayOptions != 1 && m_displayYUV == 0) { pImage->SetDisplayYUV(1); }	

		}
	
		if(bColor) {
			pImage->SetData(entry->_data.image._data.wavelet2_yuv_2._data,
				WS_TYPE_WAVELET2_YUV_2, 
				entry->_data.image._data.wavelet2_yuv_2.level[0][nDisplayLevel-1].size,
				entry->_data.image._data.wavelet2_yuv_2.level[1][nDisplayLevel-1].size,
				entry->_data.image._data.wavelet2_yuv_2.level[2][nDisplayLevel-1].size);
		}
		else {
			pImage->SetData(entry->_data.image._data.wavelet2_yuv_2._data,
				WS_TYPE_WAVELET2_YUV_2,
				entry->_data.image._data.wavelet2_yuv_2.level[0][nDisplayLevel-1].size);
		}

		for(int i=0;i<nYUV;i++) {
			for(int j=0;j<nBlocks;j++) {
				BLOCK_QUANT_TABLE[i][j] = entry->_data.image._data.wavelet2_yuv_2.quant[i][j];
			}
		}
		pImage->SetQuantTable(BLOCK_QUANT_TABLE[0]);
		pImage->SetQuantLevel(0);
		pImage->SetDisplayMode(nDisplayLevel);
		
	}
	else {
		return false;
	}

	if(camera >= 2 && !m_image[2].IsWindowVisible()) {
		// resize/reposition images
		CRect ri, rs;
		m_image[0].GetWindowRect(&ri);
		ScreenToClient(&ri);

		if (CFV_IMAGE_COLOR) // 352 width
		{
			if(CFV_IMAGE_NTSC) {
				// set to 352 :240 ratio if color. scaled to 360x245. (180x123)
				ri.right = ri.left + 270;
				ri.bottom = ri.top + 184;

				rs.left = ri.right + 8;
				rs.right = rs.left + 270;
				rs.top = ri.top;
				rs.bottom = rs.top + 184;

			}
			else {
				// set to 352 :288 ratio by default. scaled to 360x295 (originally 180x147. 270x221 too tall)
				ri.right = ri.left + 225;
				ri.bottom = ri.top + 184;
					
				rs.left = ri.right + 8;
				rs.right = rs.left + 225;
				rs.top = ri.top;
				rs.bottom = rs.top + 184;
			}
		}
		else	// 360 width
		{
			if(CFV_IMAGE_NTSC) {
				// set to 360 :240 ratio if color (180x120)
				ri.right = ri.left + 270;
				ri.bottom = ri.top + 180;
					
				rs.left = ri.right + 8;
				rs.right = rs.left + 270;
				rs.top = ri.top;
				rs.bottom = rs.top + 180;
			}
			else {
				// set to 360 :288 ratio by default (originally 180x144. 270x216 too tall)
				ri.right = ri.left + 230;
				ri.bottom = ri.top + 184;
					
				rs.left = ri.right + 8;
				rs.right = rs.left + 230;
				rs.top = ri.top;
				rs.bottom = rs.top + 184;
			}
		}

		m_image[0].MoveWindow(ri);
		GetDlgItem(IDC_INFO0)->MoveWindow(rs);

		int dx = rs.right-ri.left+4;
		ri.OffsetRect(dx, 0);
		rs.OffsetRect(dx, 0);
		m_image[1].MoveWindow(ri);
		GetDlgItem(IDC_INFO1)->MoveWindow(rs);

		int dy = ri.Height()+8;
		ri.OffsetRect(0, dy);
		rs.OffsetRect(0, dy);
		m_image[3].MoveWindow(ri);
		GetDlgItem(IDC_INFO3)->MoveWindow(rs);
		
		ri.OffsetRect(-dx, 0);
		rs.OffsetRect(-dx, 0);
		m_image[2].MoveWindow(ri);
		GetDlgItem(IDC_INFO2)->MoveWindow(rs);

		m_image[2].ShowWindow(SW_SHOW);
		m_image[3].ShowWindow(SW_SHOW);
		GetDlgItem(IDC_INFO2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_INFO3)->ShowWindow(SW_SHOW);
	}
	else {
		pImage->Invalidate();
	}
#if DEBUG_CHECKSUM_HEARTBEAT
	int nSizeY = 0, nSizeU = 0, nSizeV = 0;
	GetImageSize(m_pEntry, nSizeY, nSizeU, nSizeV);
	//sTmp.Format("%s Camera 0x%02X Y[%5d] U[%5d] V[%5d]\ttotal:%6d\n", GetEntryHeaderStr(entry), m_pHeader->source, nSizeY, nSizeU, nSizeV, nSizeY+nSizeU+nSizeV);
	SYSTEMTIME st;
    GetSystemTime(&st);
	sTmp.Format("%d-%02d-%02d %2d:%02d:%02d.%03d, ",st.wYear,st.wMonth,st.wDay,st.wHour,st.wMinute,st.wSecond, st.wMilliseconds);
	TRACE(sTmp);
	sTmp.Format("%s\n", GetEntryHeaderStr(entry));
//	sTmp.Format("%s, %d, %02X, %5d, %5d, %5d, %6d\n", GetEntryHeaderStr(entry), error? 1:2, m_pHeader->source, nSizeY, nSizeU, nSizeV, nSizeY+nSizeU+nSizeV);
	TRACE(sTmp);
#endif

	return true;
}

void CCFViewerDlg::OnSearchEvents() 
{
	UpdateData();
	m_dlgSearch.m_pCFViewerDlg = (UINT_PTR)this;
	int i = m_dlgSearch.DoModal();
	m_dlgSearch.Reset();
	return;
}

bool CCFViewerDlg::SearchEvents(bool forward)
{
	if(SearchHeader(forward, true)) {
		while(1) {
			CTime t = GetEntryTime();
			LVFINDINFO fi;
			fi.flags = LVFI_PARAM;
			fi.lParam = m_sector;
			if(m_events.FindItem(&fi, -1) == -1) {
				LVITEM item;
				item.mask = LVIF_PARAM;
				item.iItem = m_events.GetItemCount();
				item.iSubItem = 0;
				item.lParam = m_sector;
				int index = m_events.InsertItem(&item);

				item.mask = LVIF_TEXT;
				item.iItem = index;
				item.iSubItem = 0;
				item.pszText = m_pEntry->_data.event.complete ? "" : "*";
				m_events.SetItem(&item);

				CString str;
				item.mask = LVIF_TEXT;
				item.iSubItem = 1;
				str.Format("%d", GetArea(m_sector));
				item.pszText = (LPTSTR)(LPCTSTR)str;
				m_events.SetItem(&item);

				item.mask = LVIF_TEXT;
				item.iSubItem = 2;
				str.Format("0x%.08X", m_sector);
				item.pszText = (LPTSTR)(LPCTSTR)str;
				m_events.SetItem(&item);

				item.mask = LVIF_TEXT;
				item.iSubItem = 3;
				str.Format("%s.%.02d", t.Format(_T("%Y-%m-%d %H:%M:%S")), m_pEntry->header.time.hundredths);
				item.pszText = (LPTSTR)(LPCTSTR)str;
				m_events.SetItem(&item);

				item.mask = LVIF_TEXT;
				item.iSubItem = 4;
				str = m_pHeader->source == V4W_EVENT_BACKGROUND ? "Background" : EVENTS[m_pHeader->source];
				item.pszText = (LPTSTR)(LPCTSTR)str;
				m_events.SetItem(&item);
			}
			else {
				// we have looped around
				return true;
			}

			if(forward) {
				if(m_pHeader->next == m_sector) {
					AfxMessageBox("Error:  Next points to self.");
					return true;
				}
				m_sector = m_pHeader->next;
			}
			else {
				if(m_pHeader->prev == 0x00000000) {
					return true;
				}
				else {
					if(m_pHeader->prev == m_sector) {
						AfxMessageBox("Error:  Prev points to self.");
						return true;
					}
					m_sector = m_pHeader->prev;
				}
			}

			if(!ReadHeader()) {
				return true;
			}

			if(m_pHeader->type != V4W_FILESYS_ENTRY_TYPE_EVENT) {
				SetStatus("Error: event linked to non-event");
				return true;
			}
		}
	}
	else {
		return false;
	}
}

int CCFViewerDlg::GetArea(DWORD sector)
{
	for(int i=0;i<V4W_FILESYS_NUM_STORAGE_AREAS;i++) {
		if(sector >= m_cardConfig.filesys.storage[i].start && sector <= m_cardConfig.filesys.storage[i].end) {
			return i;
		}
	}

	return -1;
}

void CCFViewerDlg::OnItemchangedEvents(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	if(!(pNMListView->uOldState & LVIS_SELECTED) && (pNMListView->uNewState & LVIS_SELECTED)) {
		UpdateData();

		m_sector = m_events.GetItemData(pNMListView->iItem);
		UpdateSlider();
		GetDlgItem(IDC_EXTRACT_EVENT)->EnableWindow(TRUE);

		m_buttonGraph.SetX(0);
		m_buttonGraph.SetY(0);
		m_buttonGraph.Repaint();
		ClearDisplay();
		ResetStats();
		m_statusData = "";
		UpdateData(FALSE);

//		m_out = fopen("c:\\temp\\images.txt", "wt");

		bool first = true;
		bool found0 = false;
		bool found1 = false;
		while(1) {
			if(!ReadHeader()) {
				return;
			}
			UpdateStats();

			if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_IMAGE) {
				if(m_pHeader->source == 0) {
					if(found0) {
						break;
					}
					else {
						found0 = true;
					}
				}
				else {
					if(found1) {
						break;
					}
					else {
						found1 = true;
					}

				}

				ReadImage();
				if(found0 && found1) {
					break;
				}
				else {
					if(m_pHeader->next == m_sector) {
						AfxMessageBox("Error:  Next points to self.");
						return;
					}
					m_sector = m_pHeader->next;
				}
			}
			else if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_EVENT) {
				if(first) {
					if(m_pEntry->_data.event.next_data == m_sector) {
						AfxMessageBox("Error:  Next points to self.");
						return;
					}
					m_sector = m_pEntry->_data.event.next_data;
					first = false;
				}
				else {
					break;
				}
			}
			else {
				if(m_pHeader->next == m_sector) {
					AfxMessageBox("Error:  Next points to self.");
					return;
				}
				m_sector = m_pHeader->next;
			}
		}
	}

	*pResult = 0;
}

void CCFViewerDlg::OnExtractEvent() 
{
	UpdateData();

	CExtractDialog dlg;
	if(dlg.DoModal() != IDOK) {
		return;
	}

	if(dlg.m_type == 0) {
		ExtractImages(dlg.m_folders);
	}
	else if(dlg.m_type == 1) {
		ExtractVideo();
	}
	else if(dlg.m_type == 2) {
		ExtractAudio(dlg.m_filter);
	}
	else if(dlg.m_type == 3) {
		ExtractGForce();
	}
	else {
		ExtractGPS();
	}
}

void CCFViewerDlg::ExtractImages(BOOL folders)
{
	BROWSEINFO bi;
	TCHAR path[MAX_PATH];
	bi.hwndOwner = GetSafeHwnd();
	bi.pidlRoot = NULL;
	bi.pszDisplayName = path;
	bi.lpszTitle = _T("Choose the directory where the extracted images will be placed.");
	bi.ulFlags = BIF_EDITBOX | 0x0040; // BIF_NEWDIALOGSTYLE
	bi.lpfn = NULL;
	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
	if(pidl == NULL) {
		return;
	}
	if(!SHGetPathFromIDList(pidl, path)) {
		AfxMessageBox("Invalid path.");
		return;
	}

	POSITION pos = m_events.GetFirstSelectedItemPosition();
	if(pos == NULL) {
		return;
	}

	m_sector = m_events.GetItemData(m_events.GetNextSelectedItem(pos));
	bool first = true;
	int images = 0;
	while(1) {
		if(!ReadHeader()) {
			break;
		}

		if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_EVENT) {
			if(first) {
				if(m_pEntry->_data.event.next_data == m_sector) {
					AfxMessageBox("Error:  Next points to self.");
					return;
				}
				m_sector = m_pEntry->_data.event.next_data;
				first = false;
				continue;
			}
			else {
				break;
			}
		}
		else if(m_pHeader->type != V4W_FILESYS_ENTRY_TYPE_IMAGE) {
			if(m_pHeader->next == m_sector) {
				AfxMessageBox("Error:  Next points to self.");
				return;
			}
			m_sector = m_pHeader->next;
			continue;
		}

		if(!ReadImage()) {
			break;
		}
		int camera = m_pHeader->source;
		if(camera == 0x10) {
			camera = 1;
		}
		else if(camera == 0x20) {
			camera = 2;
		}
		else if(camera == 0x30) {
			camera = 3;
		}
		else if(m_pushWindow && (camera==1 || camera==2 || camera==3)) {
			camera = camera + m_pushWindow;
		}
	
		// if out of bounds, display in the highest camera view
		if(camera < 0 || camera >= 4) {
			camera = 3;
		}
		m_image[camera].UpdateWindow();

		V4W_FILESYS_ENTRY* e = (V4W_FILESYS_ENTRY*)m_pImage[camera];
		int width = e->_data.image.width;
		int height = e->_data.image.height;
		int linesize = (width*3+(width%4));
		DWORD size = linesize*height;

		BITMAPFILEHEADER bmfh;
		BITMAPINFOHEADER bmih;

		bmfh.bfType = ((WORD) ('M' << 8) | 'B');
		bmfh.bfSize = sizeof(bmfh) + sizeof(bmih) + size;
		bmfh.bfReserved1 = 0;
		bmfh.bfReserved2 = 0;
		bmfh.bfOffBits = sizeof(bmfh) + sizeof(bmih);

		memset(&bmih, 0, sizeof(bmih));
		bmih.biSize = sizeof(bmih);
		bmih.biWidth = width;
		bmih.biHeight = -height;
		bmih.biPlanes = 1;
		bmih.biBitCount = 24;
		bmih.biCompression = BI_RGB;
		bmih.biSizeImage = size;

		TCHAR filename[MAX_PATH];

		CString msg;
		if(folders) {
			_stprintf(filename, _T("%s\\camera %d"), path, camera);
			if(!CreateDirectory(filename, NULL) && GetLastError() != ERROR_ALREADY_EXISTS) {
				msg.Format("Error creating directory %s", filename);
				SetStatus(msg);
				return;
			}
		}

		V4W_TIME* t = &e->header.time;
		int i = 0;
		HANDLE hFile;
		while(1) {
			if(i == 0) {
				if(folders) {
					_stprintf(filename, _T("%s\\camera %d\\%.04d_%.02d_%.02d__%.02d_%.02d_%.02d_%.02d.bmp"), path,
						camera, 2000+t->year, t->month, t->date, t->hours, t->minutes, t->seconds, t->hundredths);
				}
				else {
					_stprintf(filename, _T("%s\\%.04d_%.02d_%.02d__%.02d_%.02d_%.02d_%.02d.bmp"), path,
						2000+t->year, t->month, t->date, t->hours, t->minutes, t->seconds, t->hundredths);
				}
			}
			else {
				if(folders) {
					_stprintf(filename, _T("%s\\camera %d\\%.04d_%.02d_%.02d__%.02d_%.02d_%.02d_%.02d_%.02d.bmp"), path,
						camera, 2000+t->year, t->month, t->date, t->hours, t->minutes, t->seconds, t->hundredths, i);
				}
				else {
					_stprintf(filename, _T("%s\\%.04d_%.02d_%.02d__%.02d_%.02d_%.02d_%.02d_%.02d.bmp"), path,
						2000+t->year, t->month, t->date, t->hours, t->minutes, t->seconds, t->hundredths, i);
				}
			}

			hFile = CreateFile(filename, 0, 0, NULL, OPEN_EXISTING, 0, NULL);
			if(hFile == INVALID_HANDLE_VALUE) {
				break;
			}
			CloseHandle(hFile);

			i++;
		}

		hFile = CreateFile(filename, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
		if(hFile == INVALID_HANDLE_VALUE) {
			msg.Format("Error creating file %s", filename);
			SetStatus(msg);
			return;
		}
		DWORD w;
		if(!WriteFile(hFile, &bmfh, sizeof(bmfh), &w, NULL) || w != sizeof(bmfh)) {
			msg.Format("Error writing to file %s", filename);
			SetStatus(msg);
			return;
		}
		if(!WriteFile(hFile, &bmih, sizeof(bmih), &w, NULL) || w != sizeof(bmih)) {
			msg.Format("Error writing to file %s", filename);
			SetStatus(msg);
			return;
		}

		BYTE* bData;
		if(e->_data.image.type == V4W_IMAGE_TYPE_RAW_Y) {
			bData = new BYTE[size];
			BYTE* dst = bData;
			BYTE* line = e->_data.image._data.raw_y._data+width*(height-1); 
			for(int i=0;i<height;i++) {
				BYTE* src = line;
				for(int j=0;j<width;j++) {
					*dst++ = (BYTE)*src;
					*dst++ = (BYTE)*src;
					*dst++ = (BYTE)*src;
					src++;
				}
				// pad scanline if necessary
				dst += (width%4);

				line -= width;
			}
		}
		else {
			// ***** THIS IS A HACK TO GET TO THE m_bData MEMBER *****
			// BYTE** ppData = (BYTE**)(((BYTE*)&m_image[camera])+160);		// offset changed from 136 to 160
			// bData = *ppData;

			// Get the m_bData directly by making it public rather than protected
			// bData = m_image[camera].GetData().m_bData;

			// Get the m_bData via accessor
			bData = m_image[camera].GetData();
			// ********************************************************
		}
		if(!WriteFile(hFile, bData, size, &w, NULL) || w != size) {
			msg.Format("Error writing to file %s", filename);
			SetStatus(msg);
			return;
		}
		CloseHandle(hFile);
		if(e->_data.image.type == V4W_IMAGE_TYPE_RAW_Y) {
			delete [] bData;	// MPA 2011/01/06 changed delete to delete[]
		}

		msg.Format("Extracted %d image(s)", ++images);
		SetStatus(msg);

		if(m_pHeader->next == m_sector) {
			AfxMessageBox("Error:  Next points to self.");
			return;
		}
		m_sector = m_pHeader->next;
	}
}

void CCFViewerDlg::ExtractVideo()
{
	CFileDialog fileDialog(
		FALSE, 
		"v4w", 
		GetDefaultFilename(), 
		OFN_OVERWRITEPROMPT,
		"V4W video file (*.v4w)|*.v4w|CF Card Image (*.vcfi)|*.vcfi|All Files (*.*)|*.*|||");
	
	/* 'm_dlgFileSave' replaced with local declaration (and renamed 'fileDialog') 
	so that we can give default file name */

	if(fileDialog.DoModal() != IDOK) {
		return;
	}

	HANDLE hFile = CreateFile(fileDialog.m_ofn.lpstrFile, GENERIC_WRITE, 0,
		NULL, CREATE_ALWAYS, 0, NULL);
	if(hFile == INVALID_HANDLE_VALUE) {
		CString msg;
		msg.Format("Error opening %s", fileDialog.m_ofn.lpstrFile);
		SetStatus(msg);
		return;
	}

	POSITION pos = m_events.GetFirstSelectedItemPosition();
	if(pos == NULL) {
		return;
	}

	m_sector = m_events.GetItemData(m_events.GetNextSelectedItem(pos));
	if(!ReadHeader()) {
		SetStatus("Error reading event header.");
		CloseHandle(hFile);
		return;
	}

	DWORD start = m_sector;
	DWORD end = m_pHeader->next;

	SetStatus("Saving...");

	DWORD size;
	int area = GetArea(m_sector);
	if(end > start) {
		size = (end-start)*IDE_SECTOR_SIZE;
	}
	else if(end < start) {
//		if(AfxMessageBox("Warning: Event data wraps around.  Continue?", MB_YESNO) != IDYES) {
//			CloseHandle(hFile);
//			return;
//		}
		if(m_file) {
			size = ((0x7a400-start)+(end-0x20004))*
				IDE_SECTOR_SIZE;
		}
		else {
			size = ((m_cardConfig.filesys.storage[area].end-start+1)+
				(end-m_cardConfig.filesys.storage[area].start))*
				IDE_SECTOR_SIZE;
		}
	}
	else {
		SetStatus("Empty event.");
		CloseHandle(hFile);
		return;
	}

	BYTE* buffer = new BYTE[size+2048];		// new is never deleted
	if(!buffer) {
		SetStatus("Error allocating buffer.");
		CloseHandle(hFile);
		return;
	}

	DWORD w;
	memset(buffer, 0x00, V4W_IDE_SECTOR_SIZE);
	if(!WriteFile(hFile, buffer, V4W_IDE_SECTOR_SIZE, &w, NULL) || w != V4W_IDE_SECTOR_SIZE) {
		SetStatus("Error writing file.");
		CloseHandle(hFile);
		return;
	}
	if(!Read(0, 3*V4W_IDE_SECTOR_SIZE, buffer)) {
		SetStatus("Error reading data.");
		CloseHandle(hFile);
		return;
	}
	if(!WriteFile(hFile, buffer, 3*V4W_IDE_SECTOR_SIZE, &w, NULL) || w != 3*V4W_IDE_SECTOR_SIZE) {
		SetStatus("Error writing file.");
		CloseHandle(hFile);
		return;
	}
	if(end > start) {
		if(!Read(start, size, buffer)) {
			SetStatus("Error reading data.");
			CloseHandle(hFile);
			return;
		}
		if(!WriteFile(hFile, buffer, size, &w, NULL) || w != size) {
			SetStatus("Error writing file.");
			CloseHandle(hFile);
			return;
		}
	}
	else {
		if(m_file) {
			size = (0x7a400-start)*IDE_SECTOR_SIZE;
		}
		else {
			size = (m_cardConfig.filesys.storage[area].end-start+1)*IDE_SECTOR_SIZE;
		}
		if(!Read(start, size, buffer)) {
			SetStatus("Error reading data.");
			CloseHandle(hFile);
			return;
		}
		if(!WriteFile(hFile, buffer, size, &w, NULL) || w != size) {
			SetStatus("Error writing file.");
			CloseHandle(hFile);
			return;
		}

		if(m_file) {
			size = (end-0x20004)*IDE_SECTOR_SIZE;
		}
		else {
			size = (end-m_cardConfig.filesys.storage[area].start)*IDE_SECTOR_SIZE;
		}
		if(!Read(m_file ? 0x20004 : m_cardConfig.filesys.storage[area].start, size, buffer)) {
			SetStatus("Error reading data.");
			CloseHandle(hFile);
			return;
		}
		if(!WriteFile(hFile, buffer, size, &w, NULL) || w != size) {
			SetStatus("Error writing file.");
			CloseHandle(hFile);
			return;
		}
	}

	SetStatus("Done");
	CloseHandle(hFile);
}

bool CCFViewerDlg::SeekImage(bool forward, bool stats)
{
	if(!ReadHeader()) {
		return false;
	}

	while(1) {
		if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_EVENT) {
			if(m_playmode == 0) {
				return false;
			}
			if(forward) {
				if(m_pEntry->_data.event.next_data == m_sector) {
					AfxMessageBox("Error:  Next points to self.");
					return false;
				}
				m_sector = m_pEntry->_data.event.next_data;
			}
			else {
				if(m_pEntry->_data.event.prev_data == 0x00000000) {
					return false;
				}
				else {
					if(m_pEntry->_data.event.prev_data == m_sector) {
						AfxMessageBox("Error:  Prev points to self.");
						return false;
					}
					m_sector = m_pEntry->_data.event.prev_data;
				}
			}
		}
		else {
			if(forward) {
				if(m_pHeader->next == m_sector) {
					AfxMessageBox("Error:  Next points to self.");
					return false;
				}
				m_sector = m_pHeader->next;
			}
			else {
				if(m_pHeader->prev == 0x00000000) {
					return false;
				}
				else {
					if(m_pHeader->prev == m_sector) {
						AfxMessageBox("Error:  Prev points to self.");
						return false;
					}
					m_sector = m_pHeader->prev;
				}
			}
		}

		if(!ReadHeader()) {
			return false;
		}

		if(stats) {
			UpdateStats();
		}

		if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_IMAGE) {
			return true;
		}
	}
}

bool CCFViewerDlg::SearchImage(bool forward)
{
	if(SearchHeader(forward, false)) {
		if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_IMAGE) {
			return true;
		}
		else {
			return SeekImage(forward);
		}
	}
	else {
		return false;
	}
}

void CCFViewerDlg::ResetStats()
{
	m_stats.first = true;
	m_stats.image.first = true;
	m_stats.image.n = 0;
	m_stats.image.size = 0;
	m_stats.image.bad = 0;
	m_stats.audio.first = true;
	m_stats.audio.n = 0;
	m_stats.audio.missed = 0;
	m_stats.audio.max = 0;
	m_stats.audio.min = 0;
	m_stats.gforce.first = true;
	m_stats.gforce.n = 0;
	m_stats.gforce.missed = 0;
	m_stats.gforce.x.max = 0;
	m_stats.gforce.x.min = 0;
	m_stats.gforce.y.max = 0;
	m_stats.gforce.y.min = 0;
}

FILE* fp = NULL;

void CCFViewerDlg::UpdateStats()
{
	int i;
	int j;
	int m;
	CString sTemp;
	CTime t;
	float fX;
	float fY;

	switch(m_pHeader->type) {
	case V4W_FILESYS_ENTRY_TYPE_IMAGE:
		if(m_stats.image.first) {
			m_stats.image.first = false;
			m_stats.image.n = 0;
			m_stats.image.size = 0;
			m_stats.image.bad = 0;
		}
		m_stats.image.n++;
		m_stats.image.size += m_pHeader->size;
		break;
	case V4W_FILESYS_ENTRY_TYPE_AUDIO:
#ifdef _DEBUG
//		fp = fopen("C:\\temp\\audio.dat", "ab");
//		for(i=0;i<V4W_AUDIO_ENTRY_BLOCKS;i++) fwrite(&m_pEntry->_data.audio.blocks[i], 1, sizeof(V4W_AUDIO_BLOCK), fp);
//		fclose(fp);
#endif
		if(m_stats.audio.first) {
			m_stats.audio.first = false;
			m_stats.audio.n = 0;
			m_stats.audio.sequence = m_pEntry->_data.audio.blocks[0].sequence;
			m_stats.audio.missed = 0;
		}
		m_stats.audio.n++;
		for(i=0;i<V4W_AUDIO_ENTRY_BLOCKS;i++) {
			if(m_pEntry->_data.audio.blocks[i].sequence != m_stats.audio.sequence) {
				m = (int)m_pEntry->_data.audio.blocks[i].sequence-(int)m_stats.audio.sequence;
				if(m < 0) {
					m += 256;
				}
				m_stats.audio.missed += m;
				m_stats.audio.sequence = m_pEntry->_data.audio.blocks[i].sequence;
			}
			m_stats.audio.sequence++;
		}
		break;
	case V4W_FILESYS_ENTRY_TYPE_GFORCE:
#ifdef _DEBUG
//		fp = fopen("C:\\temp\\gforce.dat", "ab");
//		for(i=0;i<V4W_GFORCE_ENTRY_BLOCKS;i++) fwrite(&m_pEntry->_data.gforce.blocks[i], 1, sizeof(V4W_GFORCE_BLOCK), fp);
//		fclose(fp);
#endif
		if(m_stats.gforce.first) {
			m_stats.gforce.first = false;
			m_stats.gforce.n = 0;
			m_stats.gforce.sequence = m_pEntry->_data.gforce.blocks[0].sequence;
			m_stats.gforce.missed = 0;
		}
		m_stats.gforce.n++;

		fp = NULL;
		if(!m_sFilePath.IsEmpty()) {
			fp = fopen(m_sFilePath, "ab");
		}

		if(fp) {
			t = GetEntryTime();
			sTemp.Format("%s.%.02d", t.Format(_T("%Y-%m-%d__%H:%M:%S")), m_pEntry->header.time.hundredths);
			fwrite(sTemp, 1, sTemp.GetLength(), fp);
		}

		for(i=0;i<V4W_GFORCE_ENTRY_BLOCKS;i++) {
			if(m_pEntry->_data.gforce.blocks[i].sequence != m_stats.gforce.sequence) {
				m = (int)m_pEntry->_data.gforce.blocks[i].sequence-(int)m_stats.gforce.sequence;
				if(m < 0) {
					m += 256;
				}
				m_stats.gforce.missed += m;
				m_stats.gforce.sequence = m_pEntry->_data.gforce.blocks[i].sequence;
			}
			m_stats.gforce.sequence++;

			for(j=0; j<V4W_GFORCE_BLOCK_SAMPLES_PER_AXIS; j++) {
				if(m_pEntry->_data.gforce.blocks[i].x[j] > m_stats.gforce.x.max) {
					m_stats.gforce.x.max = m_pEntry->_data.gforce.blocks[i].x[j];
				}
				else if(m_pEntry->_data.gforce.blocks[i].x[j] < m_stats.gforce.x.min) {
					m_stats.gforce.x.min = m_pEntry->_data.gforce.blocks[i].x[j];
				}

				if(m_pEntry->_data.gforce.blocks[i].y[j] > m_stats.gforce.y.max) {
					m_stats.gforce.y.max = m_pEntry->_data.gforce.blocks[i].y[j];
				}
				else if(m_pEntry->_data.gforce.blocks[i].y[j] < m_stats.gforce.y.min) {
					m_stats.gforce.y.min = m_pEntry->_data.gforce.blocks[i].y[j];
				}
				
				fX = SampleValuetoG((int)m_pEntry->_data.gforce.blocks[i].x[j]);
				fY = SampleValuetoG((int)m_pEntry->_data.gforce.blocks[i].y[j]);
				m_buttonGraph.SetX(fX);
				m_buttonGraph.SetY(fY);
				m_buttonGraph.Repaint();
				
				sTemp.Format(", %i, %.2f, %i, %.2f\r\n", 
					m_pEntry->_data.gforce.blocks[i].x[j], fX,
					m_pEntry->_data.gforce.blocks[i].y[j], fY);
				if(fp) {
					fwrite(sTemp, 1, sTemp.GetLength(), fp);
				}
			}			
		}
		if(fp) {
			fclose(fp);
		}

		break;
	default:
		return;
	}

	if(m_stats.first) {
		m_stats.first = false;
		m_stats.start = GetEntryTime();
		m_stats.startMilliseconds = GetEntryMilliseconds();
	}
	else {
		CTime now = GetEntryTime();
		int diffMilliseconds = GetEntryMilliseconds() - m_stats.startMilliseconds;
		CTimeSpan diff = now - m_stats.start;
		LONG s = diff.GetTotalSeconds();
		LONG ms = diff.GetTotalSeconds()*1000 + diffMilliseconds;
		if(s > 0) {
			CString str;
			str.Format("%d images/%.2f s, %.2f FPS (avg = %d), %d audio (%d Hz), %d missed, %d G-force (%d Hz), %d missed \n            %i bad images",
				m_stats.image.n, (float)ms/1000, (1000*(float)m_stats.image.n/ms), m_stats.image.n == 0 ? 0 : m_stats.image.size/m_stats.image.n,
				m_stats.audio.n, m_stats.audio.n*V4W_AUDIO_BLOCK_SAMPLES*V4W_AUDIO_ENTRY_BLOCKS/s, m_stats.audio.missed,
				m_stats.gforce.n, m_stats.gforce.n*V4W_GFORCE_BLOCK_SAMPLES_PER_AXIS*V4W_GFORCE_ENTRY_BLOCKS/s, m_stats.gforce.missed,
				m_stats.image.bad);
			SetStatus(str);

			m_statusData = "";

			if(!m_stats.gforce.first) {
				str.Format( "Maximum: Right: %i (%.2fG), Left: %i (%.2fG), Accel: %i (%.2fG), Brake: %i (%.2fG)          ", 
					m_stats.gforce.x.max, SampleValuetoG(m_stats.gforce.x.max), m_stats.gforce.x.min, SampleValuetoG(m_stats.gforce.x.min),
					m_stats.gforce.y.max, SampleValuetoG(m_stats.gforce.y.max), m_stats.gforce.y.min, SampleValuetoG(m_stats.gforce.y.min));
				m_statusData += str;
			}

			if(m_stats.audio.max || m_stats.audio.min) {
				str.Format("Max Audio: %i, Min Audio: %i", m_stats.audio.max, m_stats.audio.min);
				m_statusData += str;
			}

			UpdateData(FALSE);
		}
	}
}

void CCFViewerDlg::OnPrev() 
{
	UpdateData();
	if(SeekImage(false)) {
		ReadImage();
	}
	else {
		if(AfxMessageBox("Invalid image header.  Perform sector-by-sector search?",
			MB_YESNO) == IDYES) {
			if(SearchImage(false)) {
				ReadImage();
				return;
			}

			while(m_sector != 0x00 && 
				AfxMessageBox("No images found.\n\nSearch for another 16MB?\n(Warning: This will take a moment.)", 
				MB_YESNO) == IDYES){
				for(int nTries=0; nTries<32 && m_sector != 0x00; nTries++) {
					if(SearchImage(false)) {
						ReadImage();
						return;
					}
				}
			}
			SetStatus("");
			Sleep(200);
			SetStatus("No images found.");
		}
	}
}

void CCFViewerDlg::OnNext() 
{
	UpdateData();
	if(SeekImage(true)) {
		ReadImage();
	}
	else {
		if(AfxMessageBox("Invalid image header.  Perform sector-by-sector search?",
			MB_YESNO) == IDYES) {
			if(SearchImage(true)) {
				ReadImage();
				return;
			}

			while(m_sector < m_maxLBA && 
				AfxMessageBox("No images found.\n\nSearch for another 16MB?\n(Warning: This will take a moment.)", 
				MB_YESNO) == IDYES){
				for(int nTries=0; nTries<32 && m_sector < m_maxLBA; nTries++) {
					if(SearchImage(true)) {
						ReadImage();
						return;
					}
				}
			}
			SetStatus("");
			Sleep(200);
			SetStatus("No images found.");
		}
	}
}

void CCFViewerDlg::OnPlay() 
{
	UpdateData();
	m_forward = true;
	SetTimer(1, 100, NULL);
}

void CCFViewerDlg::OnRewind() 
{
	UpdateData();
	m_forward = false;
	SetTimer(1, 100, NULL);
}

void CCFViewerDlg::OnPause() 
{
	KillTimer(1);
	ResetStats();
//	if(m_out) {
//		fclose(m_out);
//		m_out = NULL;
//	}
}

void CCFViewerDlg::OnTimer(UINT_PTR nIDEvent) 
{
	UpdateData();

	if(nIDEvent == 1) {
		if(SeekImage(m_forward, true)) {
			ReadImage();
		}
		else {
			OnPause();
		}
	}

	CDialog::OnTimer(nIDEvent);
}

void CCFViewerDlg::OnJump() 
{
	DWORD dwTemp;

	if(m_dlgJump.DoModal() == IDOK) {
		dwTemp = strtoul(m_dlgJump.m_address, NULL, 0);
		if(dwTemp > m_maxLBA && m_maxLBA!=0 && m_maxLBA!=0xFFFFFFFF &&
			AfxMessageBox("Warning: Address is out of range.  Continue?", MB_YESNO) != IDYES) {
			return;
		}	

		m_sector = dwTemp;
		UpdateSlider();
		if(ReadHeader()) {
			if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_IMAGE) {
				ReadImage();
			}
		}
	}

}

void CCFViewerDlg::OnViewSector() 
{
	m_dlgSector.m_hFile = m_hFile;
	m_dlgSector.m_sector = m_sector;
	m_dlgSector.m_maxLBA = m_maxLBA;
	m_dlgSector.m_offset = m_file ? m_offset : 0;
	m_dlgSector.DoModal();
}

void CCFViewerDlg::ClearDisplay()
{
	for(int i=0;i<4;i++) {
		m_image[i].SetWidth(0);
		m_image[i].SetHeight(0);
		m_image[i].SetData(NULL, 0, 0);
		if(m_pImage[i] != NULL) {
			delete m_pImage[i];
			m_pImage[i] = NULL;
		}
		m_info[i].Empty();
		m_image[i].Invalidate();
	}
	UpdateData(FALSE);
}

void CCFViewerDlg::OnColumnclickEvents(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	SortEvents(pNMListView->iSubItem);
	
	*pResult = 0;
}

void CCFViewerDlg::SortEvents(int column)
{
	SORT s;

	if(column == m_sortColumn) {
		m_sortAscending = !m_sortAscending;
	}
	else {
		m_sortColumn = column;
		m_sortAscending = true;
	}

	s.pList = &m_events;
	s.column = m_sortColumn;
	s.ascending = m_sortAscending;
	m_events.SendMessage(LVM_FIRST + 81 /*LVM_SORTITEMSEX*/, (WPARAM)&s, (LPARAM)CompareFunc);
}

int CCFViewerDlg::CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	SORT* s = (SORT*)lParamSort;

	CString str1 = s->pList->GetItemText(lParam1, 4);
	CString str2 = s->pList->GetItemText(lParam2, 4);
	if(str1.Compare("<real time buffer>") == 0) {
		return -1;
	}
	if(str2.Compare("<real time buffer>") == 0) {
		return 1;
	}

	str1 = s->pList->GetItemText(lParam1, s->column);
	str2 = s->pList->GetItemText(lParam2, s->column);
	if(s->ascending) {
		return str1.Compare(str2);
	}
	else {
		return -str1.Compare(str2);
	}
}

void CCFViewerDlg::ExtractAudio(BOOL bFilter) 
{
	CFileDialog fileDialog ( 
		FALSE,					// Save As dialog
		"wav",					// default extension to be added if not entered by user
		GetDefaultFilename(),	// current file name
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_LONGNAMES | OFN_EXTENSIONDIFFERENT,
		"Wav file (*.wav)|*.wav||",
		this );

	fileDialog.m_ofn.lpstrTitle = _T("Save Audio");	

	if ( fileDialog.DoModal() == IDOK ) {
		CString sFile = fileDialog.GetPathName();
		CWaveFile waveFile;
		if(waveFile.Open(sFile)) {
			m_firstAudio = true;
			ResetStats();
			while(SeekAudio(true)) {
				ReadAudio();
				if(bFilter) {
					FilterAudio(m_decodedBuffer, m_filteredBuffer, sizeof(m_decodedBuffer)/2);
					waveFile.AddSamples((BYTE*)m_filteredBuffer, sizeof(m_filteredBuffer));
				}
				else {
					waveFile.AddSamples((BYTE*)m_decodedBuffer, sizeof(m_decodedBuffer));
				}
			}
			waveFile.Close();

			HINSTANCE exec_result = ShellExecute( AfxGetMainWnd()->m_hWnd,"open",
											  (LPCTSTR)sFile.GetBuffer(0),
											  "","", SW_SHOWNORMAL);
			if(DWORD(exec_result)<=32) {
				AfxMessageBox("Error occured while opening .wav file.");
			}
		}
		else {
			CString sMsg;
			sMsg.Format("Error writing to\n%s\n please make sure the file is closed.", sFile);
			AfxMessageBox(sMsg);
		}
	}
}

bool CCFViewerDlg::SeekAudio(bool stats)
{
	if(!ReadHeader()) {
		return false;
	}

	while(1) {
		if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_EVENT) {
			// reached next event
			return false;
		}
		else {
			if(m_pHeader->next == m_sector) {
				AfxMessageBox("Error:  Next points to self.");
				return false;
			}
			m_sector = m_pHeader->next;
		}

		if(!ReadHeader()) {
			return false;
		}

		if(stats) {
			UpdateStats();
		}

		if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_AUDIO) {
			return true;
		}
	}
}

bool CCFViewerDlg::ReadAudio()
{
	DWORD end = m_pHeader->next;
	if(end < m_sector) {
		if(m_file) {
			SetStatus("Cannot handle wraparound.");
			return false;
		}
	}

	DWORD size = sizeof(V4W_FILESYS_HEADER) + m_pEntry->header.size;

	if(end < m_sector) {
		int area = GetArea(m_sector);
		DWORD size1;
		if(area == -1) {
			size1 = (m_cardConfig.filesys.buffer.end-m_sector+1)*IDE_SECTOR_SIZE;
		}
		else {
			size1 = (m_cardConfig.filesys.storage[area].end-m_sector+1)*IDE_SECTOR_SIZE;
		}
		if(!Read(m_sector, size1, m_audioBuffer)) {
			return false;
		}
		DWORD start;
		if(area == -1) {
			start = m_cardConfig.filesys.buffer.start;
		}
		else {
			start = m_cardConfig.filesys.storage[area].start;
		}
		if(!Read(start, size-size1, m_audioBuffer+size1)) {
			return false;
		}
	}
	else {
		if(!Read(m_sector, size, m_audioBuffer)) {
			return false;
		}
	}

	for(int i=0; i<V4W_AUDIO_ENTRY_BLOCKS; i++) {
		AdpcmDecodePacket(&m_decodedBuffer[V4W_AUDIO_BLOCK_SAMPLES*i], 
			(V4W_AUDIO_BLOCK*)&m_audioBuffer[sizeof(V4W_FILESYS_HEADER) + i*sizeof(V4W_AUDIO_BLOCK)]);
	}

	return true;
}

void CCFViewerDlg::AdpcmDecodePacket(short *pwAdpcmBuffer, V4W_AUDIO_BLOCK *pAudioBlock)
{
	// ADPCM constants
	const char ADPCM_INDEX_TABLE3[8] =
	{
		-1, -1, 1, 2,
		-1, -1, 1, 2
	};

	const unsigned short ADPCM_STEPSIZE_TABLE[89]  =
	{
		7, 8, 9, 10, 11, 12, 13, 14, 16, 17,
		19, 21, 23, 25, 28, 31, 34, 37, 41, 45,
		50, 55, 60, 66, 73, 80, 88, 97, 107, 118,
		130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
		337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
		876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066,
		2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
		5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
		15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767
	};
	
	// ADPCM variables
	union
	{
		DWORD d;
		BYTE b[4];
	} adpcm_holding_reg;

	short adpcm_prev_value;
	char adpcm_index;
	BYTE adpcm_holding_reg_samples;
	BYTE *adpcm_ptr;
	adpcm_ptr = pAudioBlock->_data;

	BYTE sign;
	BYTE delta;
	unsigned short step;
	long pred_value;
	unsigned short pred_diff;
	short* sample_ptr = pwAdpcmBuffer;
	int n = V4W_AUDIO_BLOCK_SAMPLES;

	*sample_ptr++ = pAudioBlock->value;
	adpcm_prev_value = pAudioBlock->value;
	adpcm_index = pAudioBlock->index;
	n--;

	adpcm_holding_reg_samples = 0;

	while(n > 0) {
		pred_value = adpcm_prev_value;
		step = ADPCM_STEPSIZE_TABLE[adpcm_index];

		// Step 1 - get the delta value 
		if(adpcm_holding_reg_samples == 0) {
			adpcm_holding_reg.b[3] = *adpcm_ptr++;
			adpcm_holding_reg.b[2] = *adpcm_ptr++;
			adpcm_holding_reg.b[1] = *adpcm_ptr++;
			adpcm_holding_reg.b[0] = 0;
			adpcm_holding_reg_samples = 8;
		}
		delta = (adpcm_holding_reg.b[3] >> 5);
		adpcm_holding_reg.d <<= 3;
		adpcm_holding_reg_samples--;

		// Step 2 - Find new index value (for later) 
		adpcm_index += ADPCM_INDEX_TABLE3[delta];
		if(adpcm_index < 0) {
			adpcm_index = 0;
		}
		else if(adpcm_index > 88) {
			adpcm_index = 88;
		}

		// Step 3 - Separate sign and magnitude 
		sign = delta & 4;
		delta = delta & 3;

		// Step 4 - Compute difference and new predicted value 
		//
		// Computes 'pred_diff = (delta+0.5)*step/4', but see comment
		// in adpcm_encode4.
		//
		pred_diff = step>>2;
		if(delta & 2) pred_diff += step;
		if(delta & 1) pred_diff += step>>1;

		if(sign) {
			pred_value -= pred_diff;
		}
		else {
			pred_value += pred_diff;
		}

		// Step 5 - clamp output value 
		if(pred_value > 32767) {
			pred_value = 32767;
		}
		else if(pred_value < -32768) {
			pred_value = -32768;
		}

		// Step 6 - Output value 
		*sample_ptr++ = (short)pred_value;
		adpcm_prev_value = (short)pred_value;
		n--;

		// update max and min
		if((short)pred_value > m_stats.audio.max) {
			m_stats.audio.max = (short)pred_value;
		}
		else if((short)pred_value < m_stats.audio.min) {
			m_stats.audio.min = (short)pred_value;
		}
	}
}

void CCFViewerDlg::FilterAudio(short *pIn, short *pOut, int nSamples)
{
	/* note: the last nOrder samples are cutoff in this implementation
	filtertype  =  IIR Butterworth  
	passtype  =  Lowpass  
	order  =  10  
	samplerate  =  11037  
	corner1  =  3800  */

	const double GAIN = 2.717939360e+01;
	const int nOrder = 10;
	static double in[nOrder], out[nOrder];
	double dTemp;

	for(int j=nSamples; j>0; j--) {
		if(m_firstAudio) {
			memset(in, 0, sizeof(in));
			memset(out, 0, sizeof(out));
			m_firstAudio = false;
		}

		dTemp = (1 * in[0]) + (10 * in[1]) + (45 * in[2]) + (120 * in[3]) + (210 * in[4])
			+ (252 * in[5]) + (210 * in[6]) + (120 * in[7]) + (45 * in[8])
			+ (10 * in[9]) + (1 * (*pIn/GAIN))
		    + (-0.0013536984 * out[0]) + (-0.0219841005 * out[1]) + (-0.1646084503 * out[2])
			+ (-0.7504687855 * out[3]) + (-2.3155529730 * out[4]) + (-5.0746017756 * out[5])
			+ (-8.0496294746 * out[6]) + (-9.1965009545 * out[7]) + (-7.3410628281 * out[8])
			+ (-3.7598383699 * out[9]);

		if(dTemp > 32767) {
			*pOut = 32767;
		}
		else if(dTemp < -32768) {
			*pOut = -32768;
		}
		else {
			*pOut = (short)dTemp;
		}
		
		for(int i=0; i<nOrder-1; i++) {
			in[i] = in[i+1]; 
			out[i] = out[i+1];
		}
		in[nOrder-1] = *pIn/GAIN;
		out[nOrder-1] = dTemp;
		pOut++;
		pIn++;
	}
}

float CCFViewerDlg::SampleValuetoG(int SampleValue)
{
	const float GFORCE_VS =	3.3f;		// supply voltage for the accelerometer
	const float VOLTS_PER_G	= ((GFORCE_VS/3.0f)*0.3f);		// as per spec sheet

	return (SampleValue/255.0f*GFORCE_VS)/VOLTS_PER_G;
}

void CCFViewerDlg::ExtractGForce()
{
	CString sTemp;

	CFileDialog fileDialog ( 
		FALSE,					// Save As dialog
		"csv",					// default extension to be added if not entered by user
		GetDefaultFilename(""," G-Force"),	// current file name
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_LONGNAMES | OFN_EXTENSIONDIFFERENT,
		"csv file (*.csv)|*.csv||",
		this );

	fileDialog.m_ofn.lpstrTitle = _T("Save G-Force");	

	if ( fileDialog.DoModal() == IDOK ) {
		m_sFilePath = fileDialog.GetPathName();
		fp = fopen(m_sFilePath, "w");
		if(fp) {
			sTemp.Format("Local Time, X(sample), X(G), Y(sample), Y(G)\n");
			fwrite(sTemp, 1, sTemp.GetLength(), fp);
			fclose(fp);
			ResetStats();
			// while seeking audio throughout the event, g-forces are extracted
			while(SeekAudio(true));

			HINSTANCE exec_result = ShellExecute( AfxGetMainWnd()->m_hWnd,"open",
											  (LPCTSTR)m_sFilePath.GetBuffer(0),
											  "","", SW_SHOWNORMAL);
			if(DWORD(exec_result)<=32) {
				AfxMessageBox("Error occured while opening .csv file.");
			}
		}
		else {
			CString sMsg;
			sMsg.Format("Error writing to\n%s\n please make sure the file is closed.", m_sFilePath);
			AfxMessageBox(sMsg);
		}
	}
	m_sFilePath.Empty();
}

void CCFViewerDlg::ExtractGPS()
{
	CString sTemp;
	float nMaxSpeed = 0;

//	sTemp.Format("GPS %s_%02d.csv", CSectorDlg::ConvertTime(m_pEntry->header.time).Format("%Y-%m-%d %H_%M_%S"), m_pEntry->header.time.hundredths);

	CFileDialog fileDialog ( 
		FALSE,					// Save As dialog
		"csv",					// default extension to be added if not entered by user
		GetDefaultFilename("", " GPS"),		// current file name
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_LONGNAMES | OFN_EXTENSIONDIFFERENT,
		"csv file (*.csv)|*.csv||",
		this );

	fileDialog.m_ofn.lpstrTitle = _T("Save G-Force");	

	if ( fileDialog.DoModal() == IDOK ) {
		m_sFilePath = fileDialog.GetPathName();
		fp = fopen(m_sFilePath, "w");
		if(fp) {
			sTemp.Format("Local Time, Speed (km/h), Latitude, Longitude, Heading\n");
			fwrite(sTemp, 1, sTemp.GetLength(), fp);

			// Do not save stats, otherwise it will write gforce to the csv file
			// display degrees for long & lat in decimal fraction
			while(SeekImage(true, false)) {
				sTemp.Format("%s.%02d, %6.2f, %8.6f, %8.6f, %8.4f\n", 
					CSectorDlg::ConvertTime(m_pHeader->time).Format("%Y-%m-%d %H:%M:%S"), 
					m_pHeader->time.hundredths, 
					m_pHeader->gps.speed*1.852,
					m_pHeader->gps.latitude/100,
					m_pHeader->gps.longitude/100,
					m_pHeader->gps.heading );
				if(fp) {
					fwrite(sTemp, 1, sTemp.GetLength(), fp);
				}
				if ( m_pHeader->gps.speed > nMaxSpeed) { nMaxSpeed = m_pHeader->gps.speed; }
			}
			fclose(fp);

			sTemp.Format("Maximum speed in event is %6.2f km/h", nMaxSpeed*1.852);
			SetStatus(sTemp);


			HINSTANCE exec_result = ShellExecute( AfxGetMainWnd()->m_hWnd,"open",
											  (LPCTSTR)m_sFilePath.GetBuffer(0),
											  "","", SW_SHOWNORMAL); 
			if(DWORD(exec_result)<=32) {
				AfxMessageBox("Error occured while opening .csv file.");
			}
		}
		else {
			CString sMsg;
			sMsg.Format("Error writing to\n%s\n please make sure the file is closed.", m_sFilePath);
			AfxMessageBox(sMsg);
		}
	}
	m_sFilePath.Empty();

}

CString CCFViewerDlg::GetDefaultFilename(CString prefix, CString postfix) {
	// Default filename should include esn or unit name + entry time
	CString s;
	s.Format("%s%s (%.02d.%.02d.%.02d.%.06d) (%s_%02d)%s", 
		prefix, 
		m_pEntry->header.unit_id, 
		m_pEntry->header.esn[0], m_pEntry->header.esn[1], m_pEntry->header.esn[2], m_pEntry->header.esn[3]*10000+m_pEntry->header.esn[4]*100+m_pEntry->header.esn[5],
		CSectorDlg::ConvertTime(m_pEntry->header.time).Format("%Y-%m-%d %H_%M_%S"), 
		m_pEntry->header.time.hundredths, 
		postfix );
	CleanString(s);
	return s;

}

// TAKEN FROM CSingleton IN INSTALLER/VIEWER
bool CCFViewerDlg::CleanString(CString &s)
{
	int i,n;
	
	if ( s.IsEmpty() ) return FALSE;

	// Remove White space at the end of the string
	n = s.GetLength();

	if ( n <= 0 ) return FALSE;

	while ( s[n-1] == ' ' && n > 1 )
	{
		s.SetAt(n-1,'\0');
		--n;
	}

	for (i = 0; i < n; i++)
	{
	if (s[i] < 32    ||
		s[i] > 122   ||
//		s[i] == ' '  ||
		s[i] == '\"' ||
		s[i] == '.'  ||
		s[i] == '/'  ||
		s[i] == ':'  ||
		s[i] == '<'  ||
		s[i] == '>'  ||
		s[i] == '?'  ||
		s[i] == '\\' ||
		s[i] == '|'  ||
		s[i] == '*'  ||
		s[i] == '%'  ||
		s[i] == '$')
	  s.SetAt(i, '-');
	}
	return TRUE;
}

void CCFViewerDlg::OnCustomdrawSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CString str;

	str.Format("Address:     0x%.08X", m_sliderSector.GetPos());
	m_sectorStatic.SetWindowText(str);
	*pResult = 0;
}

void CCFViewerDlg::OnReleasedcaptureSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	m_sector = m_sliderSector.GetPos();
	UpdateSlider();

	if(ReadHeader()) {
		if(m_pHeader->type == V4W_FILESYS_ENTRY_TYPE_IMAGE) {
			ReadImage();
		}
	}
	*pResult = 0;
}

void CCFViewerDlg::UpdateSlider()
{
	CString str;

	m_sliderSector.SetPos(m_sector);
	str.Format("Address:     0x%.08X", m_sliderSector.GetPos());
	m_sectorStatic.SetWindowText(str);
}

void CCFViewerDlg::HandleWrap()
{
	if (m_sector == 0) {
		m_sector = m_maxLBA;
	}
	else if (m_sector == m_maxLBA) {
		m_sector = 0;
	}
}

void CCFViewerDlg::IncrementSector()
{
	if (m_sector >= m_maxLBA) {
		m_sector = 0;
	}
	else {
		m_sector++;
	}

	// Not using this method since it may give undesired behaviour for values greater than max
/*	// Max LBA represents the highest valid LBA, not the number of sectors
	m_sector = (m_sector + 1) % (m_maxLBA + 1);		*/
}

void CCFViewerDlg::DecrementSector()
{
	if (m_sector <= 0) {
		m_sector = m_maxLBA;
	}
	else {
		m_sector--;
	}
}