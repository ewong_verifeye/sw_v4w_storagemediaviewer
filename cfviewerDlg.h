// cfviewerDlg.h : header file
//

#if !defined(AFX_CFVIEWERDLG_H__BFC971BD_63EA_4995_AB9D_5C7B1C7F1A8D__INCLUDED_)
#define AFX_CFVIEWERDLG_H__BFC971BD_63EA_4995_AB9D_5C7B1C7F1A8D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdio.h>
#include "v4w.h"
#include "WaveletStatic.h"
#include "ReaderDlg.h"
#include "SearchDlg.h"
#include "JumpDlg.h"
#include "SectorDlg.h"
#include "ButtonGraph.h"


#define MAX_AUDIO_BLOCKS 1000*V4W_AUDIO_ENTRY_BLOCKS

typedef struct {
	bool first;
	CTime start;
	int startMilliseconds;
	struct {
		bool first;
		DWORD n;
		DWORD size;
		DWORD bad;
	} image;
	struct {
		bool first;
		DWORD n;
		BYTE sequence;
		DWORD missed;
		struct {
			char max;
			char min;
		} x, y;
	} gforce;
	struct {
		bool first;
		DWORD n;
		BYTE sequence;
		DWORD missed;
		short max;
		short min;
	} audio;
} STATS;

typedef struct {
	CListCtrl* pList;
	int column;
	bool ascending;
} SORT;


/////////////////////////////////////////////////////////////////////////////
// CCFViewerDlg dialog

class CCFViewerDlg : public CDialog
{
// Construction
public:
	CCFViewerDlg(CWnd* pParent = NULL);	// standard constructor
	~CCFViewerDlg() {
		if (m_pImage[0]) { delete [] m_pImage[0]; m_pImage[0] = 0; }
		if (m_pImage[1]) { delete [] m_pImage[1]; m_pImage[1] = 0; }
		if (m_pImage[2]) { delete [] m_pImage[2]; m_pImage[2] = 0; }
		if (m_pImage[3]) { delete [] m_pImage[3]; m_pImage[3] = 0; }
		if (m_pHeader) { delete [] m_pHeader; m_pHeader = 0; }
	}
// Dialog Data
	//{{AFX_DATA(CCFViewerDlg)
	enum { IDD = IDD_CFVIEWER_DIALOG };
	CSliderCtrl	m_sliderSector;
	CButtonGraph	m_buttonGraph;
	CListCtrl	m_events;
	CStatic	m_sectorStatic;
	int		m_source;
	CString	m_filename;
	int		m_playmode;
	CString	m_status;
	CString	m_statusData;
	int		m_displayYUV;
	int		m_displayOptions;
	//}}AFX_DATA
	CWaveletStatic m_image[4];
	CString m_info[4];

	// Helper Functions
	bool SearchEvents(bool forward);
	void HandleWrap();
	void IncrementSector();
	void DecrementSector();

	DWORD GetMaxLBA() { return m_maxLBA; }

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCFViewerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	HBITMAP m_bmPlay;
	HBITMAP m_bmRewind;
	HBITMAP m_bmPause;
	HBITMAP m_bmNext;
	HBITMAP m_bmPrev;
	CFont m_font;

	CFileDialog m_dlgFileOpen;
	CFileDialog m_dlgFileSave;
	CReaderDlg m_dlgReader;
	CSearchDlg m_dlgSearch;
	CJumpDlg m_dlgJump;
	CSectorDlg m_dlgSector;

	HANDLE m_hFile;
	bool m_file;

//	V4W_SYS_CONFIG m_sysConfig;
	V4W_CARD_CONFIG m_cardConfig;
	V4W_FRAM_MAP m_framMap;

	V4W_FILESYS_HEADER* m_pHeader;
	V4W_FILESYS_ENTRY* m_pEntry;
	V4W_FILESYS_ENTRY* m_pImage[4];

	DWORD m_sector;
	DWORD m_offset;
	DWORD m_end;
	DWORD m_maxLBA;

	bool m_forward;

	int m_pushWindow;

	STATS m_stats;

	FILE* m_out;

	int m_sortColumn;
	bool m_sortAscending;

	BYTE m_audioBuffer[sizeof(V4W_AUDIO) + sizeof(V4W_FILESYS_HEADER)];
	SHORT m_decodedBuffer[V4W_AUDIO_BLOCK_SAMPLES*V4W_AUDIO_ENTRY_BLOCKS];
	SHORT m_filteredBuffer[V4W_AUDIO_BLOCK_SAMPLES*V4W_AUDIO_ENTRY_BLOCKS];
	bool m_firstAudio;

	CString m_sFilePath;
			
	BYTE CalcChecksum(BYTE* buffer, int length);
	void SetStatus(LPCTSTR text);
	bool Open();
	void FindFlashReader(CString& path);
	bool Read(DWORD lba, DWORD size, BYTE* buffer);
	bool ReadHeader();
	bool SearchHeader(bool forward, bool event);
	bool ReadImage();
	CTime GetEntryTime();
	int GetEntryMilliseconds();
	CString GetEntryHeaderStr(V4W_FILESYS_ENTRY* entry);
	CString GetImageType(BYTE type);
	bool GetChecksumResults(V4W_FILESYS_ENTRY* entry, bool &Y, bool &U, bool &V);
	void GetImageSize(V4W_FILESYS_ENTRY* entry, int &Y, int &U, int &V);
	CString GetEntryGPS();
	bool ShowImage(V4W_FILESYS_ENTRY* image, CString &sError);
	int GetArea(DWORD sector);
	void ExtractImages(BOOL folders);
	void ExtractVideo();
	bool SeekImage(bool forward, bool stats = false);
	bool SearchImage(bool forward);
	void ResetStats();
	void UpdateStats();
	void ClearDisplay();
	void SortEvents(int column);
	static int CALLBACK CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	float SampleValuetoG(int);

	bool SeekAudio(bool);
	bool ReadAudio();
	void ExtractAudio(BOOL);
	void AdpcmDecodePacket(short *, V4W_AUDIO_BLOCK *);
	void FilterAudio(short *, short *, int);
	void ExtractGForce();
	void ExtractGPS();
	void UpdateSlider();
	CString GetDefaultFilename(CString prefix="", CString postfix="");
	bool CleanString(CString &s);


	// Generated message map functions
	//{{AFX_MSG(CCFViewerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT_PTR nIDEvent);	// to work in x64 UINT must be UINT_PTR
	afx_msg void OnPrev();
	afx_msg void OnRewind();
	afx_msg void OnPause();
	afx_msg void OnPlay();
	afx_msg void OnNext();
	afx_msg void OnBrowse();
	afx_msg void OnOpen();
	afx_msg void OnJump();
	afx_msg void OnSearchEvents();
	afx_msg void OnViewSector();
	afx_msg void OnExtractEvent();
	afx_msg void OnColumnclickEvents(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedEvents(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCustomdrawSlider(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureSlider(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CFVIEWERDLG_H__BFC971BD_63EA_4995_AB9D_5C7B1C7F1A8D__INCLUDED_)
