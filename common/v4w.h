//********************************************************************
//
//		VerifEye Technologies
//		7100 Warden Ave, Unit #3
//		Markham, Ontario, Canada, L3R 8B5
//
//		Project:		V4W Common Files
//		Filename:		v4w.h
//		Author:			Jason Thomas
//						edited by Guy Gagnon
//						and Edmond Chan
//		Description:	Definition of common structures, constants,
//						etc.
//
//********************************************************************

#ifndef V4W_H
#define V4W_H

#if ((defined (WIN32) || defined (linux)) && !defined (_LIB)) || defined (__ICCARM__)
#pragma pack(push, 1)
#endif

#define V4W_IDE_SECTOR_SIZE			512
#define V4W_IDE_ID_LENGTH			20
#define V4W_STRING_LENGTH			25
#define V4W_ESN_LENGTH				6
#define V4W_AUTH_CODE_LENGTH		3

//****************************
//     Version structures
//****************************

typedef struct {
	BYTE fw[V4W_STRING_LENGTH];
	BYTE config;
} V4W_VERSION;

//*****************************
//     Baud rate constants 
//*****************************

#define V4W_BAUDRATE_DEFAULT		0
#define V4W_BAUDRATE_2400			1
#define V4W_BAUDRATE_4800			2
#define V4W_BAUDRATE_9600			3
#define V4W_BAUDRATE_14400			4
#define V4W_BAUDRATE_19200			5
#define V4W_BAUDRATE_28800			6
#define V4W_BAUDRATE_38400			7
#define V4W_BAUDRATE_57600			8
#define V4W_BAUDRATE_115200			9
// Baud rates below are not supported by dual-UART
#define V4W_BAUDRATE_230400			10
#define V4W_BAUDRATE_460800			11
#define V4W_BAUDRATE_921600			12

//***************************
//     Command constants
//***************************

#define V4W_CMD_MAGIC_CHAR			0x5A
#define V4W_CMD_MAGIC_CHAR_STM		0xB2

#define V4W_CMD_CHECKSUM_OFFSET		0x00
#define V4W_CMD_CHECKSUM_OFFSET_STM	0xCE

#define V4W_CMD_UNDEFINED			0x00
#define V4W_CMD_READ_MEM			0x01
#define V4W_CMD_WRITE_MEM			0x02
#define V4W_CMD_READ_I2C			0x03
#define V4W_CMD_WRITE_I2C			0x04
#define V4W_CMD_READ_FRAM			0x05
#define V4W_CMD_WRITE_FRAM			0x06
#define V4W_CMD_READ_IDE			0x07
#define V4W_CMD_WRITE_IDE			0x08
#define V4W_CMD_READ_INFO			0x09
#define V4W_CMD_WRITE_INFO			0x0A
#define V4W_CMD_RESET				0x0B
#define V4W_CMD_GET_RTC				0x0C
#define V4W_CMD_SET_RTC				0x0D
#define V4W_CMD_START_CAPTURE		0x0E
#define V4W_CMD_STOP_CAPTURE		0x0F
//#define V4W_CMD_SYNC				0x10
#define V4W_CMD_GRAB_FRAME			0x11
#define V4W_CMD_GET_CAM_STATUS		0x12
//#define V4W_CMD_READ_HEAD			0x13	// removed in main fw v3.0.0 and later
//#define V4W_CMD_WRITE_HEAD		0x14	// removed in main fw v3.0.0 and later
#define V4W_CMD_UNLOCK				0x15
#define V4W_CMD_WRITE_IDE_ERROR_LOG 0x16
#define V4W_CMD_HEAD_TRANSFER		0x17
#define V4W_CMD_DETECT_CAMERAS      0x18
#define V4W_CMD_GET_GPS_STATUS      0x19
#define V4W_CMD_DETECT_DUART    	0x1A
#define V4W_CMD_CLR_PWR_FREQUENT   	0x1B
#define V4W_CMD_SET_WIFI_WAKEUP		0x1C
#define V4W_CMD_GET_RTC_MODEL		0x1D
#define V4W_CMD_GET_DRIVE_TEMP		0x1E
#define V4W_CMD_GET_DRIVE_STATE		0x1F
#define V4W_CMD_GET_CAM_CURRENT		0x20
#define V4W_CMD_STOP_CAPTURE_SHORT	0x21
#define V4W_CMD_READ_IDE2			0x22
#define V4W_CMD_WRITE_IDE2			0x23
#define V4W_CMD_FORCE_RELAY			0x24
#define V4W_CMD_SET_DOWNLOAD_MODE	0x25
#define V4W_CMD_FORCE_SLEEP			0x26

//************************
//     RTC structures
//************************

typedef struct {
	BYTE hundredths;
	BYTE seconds;
	BYTE minutes;
	BYTE hours;
	BYTE date;
	BYTE month;
	BYTE year;
} V4W_TIME;

//**************************
//     Image structures
//**************************

#define V4W_IMAGE_TYPE_RAW_Y					0
#define V4W_IMAGE_TYPE_WAVELET_Y				1
#define V4W_IMAGE_TYPE_RAW_YUV					2
#define V4W_IMAGE_TYPE_WAVELET_YUV				3
#define V4W_IMAGE_TYPE_WAVELET2_Y				4
#define V4W_IMAGE_TYPE_WAVELET2_YUV				5
#define V4W_IMAGE_TYPE_WAVELET2_YUV_2			6

#define V4W_IMAGE_WAVELET_PASSES				3
#define V4W_IMAGE_WAVELET_LEVELS				(V4W_IMAGE_WAVELET_PASSES+2)
#define V4W_IMAGE_WAVELET_BLOCKS				(V4W_IMAGE_WAVELET_PASSES*3+2)
#define V4W_IMAGE_WAVELET_LEVELS_YUV			(V4W_IMAGE_WAVELET_PASSES+1)
#define V4W_IMAGE_WAVELET_BLOCKS_YUV			(V4W_IMAGE_WAVELET_PASSES*3+1)

typedef struct {
	DWORD size;
	BYTE checksum;
} V4W_WAVELET_LEVEL;

typedef struct {
	BYTE type;
	WORD width;
	WORD height;
	union {
		struct {
			BYTE _data[1];
		} raw_y;
		struct {
			BYTE quant[V4W_IMAGE_WAVELET_BLOCKS];
			V4W_WAVELET_LEVEL level[V4W_IMAGE_WAVELET_LEVELS];
			BYTE _data[1];
		} wavelet_y;
		struct {
			BYTE oddeven;
			BYTE reserved[15];
			BYTE _data[1];
		} raw_yuv;
		struct {
			BYTE oddeven;
			BYTE reserved[15];
			BYTE quant[3][V4W_IMAGE_WAVELET_BLOCKS_YUV];
			V4W_WAVELET_LEVEL level[3][V4W_IMAGE_WAVELET_LEVELS_YUV];
			BYTE _data[1];
		} wavelet_yuv;
		struct {
			BYTE oddeven;
			BYTE pal_standard;
			BYTE blackwhite;
			BYTE image_full_resolution;
			BYTE reserved[12];
			BYTE quant[3][V4W_IMAGE_WAVELET_BLOCKS];
			V4W_WAVELET_LEVEL level[3][V4W_IMAGE_WAVELET_LEVELS];
			BYTE _data[1];
		} wavelet2_yuv_2;
	} _data;
} V4W_IMAGE;

//**************************
//     Audio structures
//**************************

#define V4W_AUDIO_SAMPLE_RATE		11037
#define V4W_AUDIO_BLOCK_SIZE		128
#define V4W_AUDIO_BLOCK_SAMPLES		321
#define V4W_AUDIO_ENTRY_BLOCKS		7

typedef struct {
	BYTE sequence;
	short value;
	BYTE index;
	BYTE _data[120];
    BYTE reserved[2];
	WORD sync;
} V4W_AUDIO_BLOCK;

typedef struct {
	V4W_AUDIO_BLOCK blocks[V4W_AUDIO_ENTRY_BLOCKS];
} V4W_AUDIO;

//****************************
//     G-Force structures
//****************************

#define V4W_GFORCE_SAMPLE_RATE					1003
#define V4W_GFORCE_BLOCK_SIZE					64
#define V4W_GFORCE_BLOCK_SAMPLES_PER_AXIS		30
#define V4W_GFORCE_ENTRY_BLOCKS					6

#define V4W_GFORCE_TRIGGER_Y_NEG_WEAK			0x01
#define V4W_GFORCE_TRIGGER_Y_NEG_STRONG			0x02
#define V4W_GFORCE_TRIGGER_Y_POS_WEAK			0x04
#define V4W_GFORCE_TRIGGER_Y_POS_STRONG			0x08
#define V4W_GFORCE_TRIGGER_X_NEG_WEAK			0x10
#define V4W_GFORCE_TRIGGER_X_NEG_STRONG			0x20
#define V4W_GFORCE_TRIGGER_X_POS_WEAK			0x40
#define V4W_GFORCE_TRIGGER_X_POS_STRONG			0x80

typedef struct {
	BYTE sequence;
	char x[V4W_GFORCE_BLOCK_SAMPLES_PER_AXIS];
	char y[V4W_GFORCE_BLOCK_SAMPLES_PER_AXIS];
	BYTE trigger;
	WORD sync;
} V4W_GFORCE_BLOCK;

typedef struct {
	V4W_GFORCE_BLOCK blocks[V4W_GFORCE_ENTRY_BLOCKS];
} V4W_GFORCE;

//************************
//     GPS structures
//************************

#define V4W_GPS_MAX_STRING_LENGTH				85
#define V4W_GPS_VALID                           0x01
#define V4W_GPS_VALID_LAST_HOUR                 0x02

typedef struct {
	float latitude;
	float longitude;
	float speed;
	float heading;
} V4W_GPS;

typedef struct {
	BYTE num_sat;
	float HDOP;
} V4W_GPS_STATUS;

//**************************
//     Event structures
//**************************

typedef struct {
	BYTE complete;
	DWORD prev_data;
	DWORD next_data;
	V4W_TIME first;
	V4W_TIME last;
	DWORD prev_event_time;
	WORD event_num;
	WORD total_img;
} V4W_EVENT;

typedef struct {
	BYTE complete;
	DWORD prev_marker;
	DWORD next_marker;
	V4W_TIME first;
	V4W_TIME last;
} V4W_MARKER;

//********************************
//     File system structures
//********************************

#define V4W_FILESYS_VERSION					4 

#define V4W_FILESYS_HEADER_MAGIC			0x44

#define V4W_FILESYS_ENTRY_TYPE_IMAGE		0x01
#define V4W_FILESYS_ENTRY_TYPE_AUDIO		0x02
#define V4W_FILESYS_ENTRY_TYPE_GFORCE		0x03
#define V4W_FILESYS_ENTRY_TYPE_EVENT		0x10
#define V4W_FILESYS_ENTRY_TYPE_MARKER		0x20

// constants are bitfield
#define V4W_HEAD_FEATURES_AUDIO		        0x01
#define V4W_HEAD_FEATURES_GFORCE	        0x02
#define V4W_HEAD_FEATURES_GPS     	        0x04

typedef struct {
        BYTE number[4];     // msb at number[0]
        char rev;
        BYTE features;
} V4W_HEAD_ESN;

typedef struct {
        V4W_HEAD_ESN esn;
        BYTE format;
        BYTE color;
        BYTE led_ir;
        BYTE raw_gforce_mode;
        BYTE flipped;
        BYTE reserved[5];
} V4W_HEAD_HEADER;

#define V4W_FILESYS_RTC_STATUS_SYNC_24HRS_BIT		0x01
#define V4W_FILESYS_RTC_STATUS_ERROR_BIT			0x02

typedef struct {
	BYTE magic;
	DWORD prev;	 // lba previous entry of "type"
	DWORD next;	 // lba next entry of "type"
	DWORD size;	 // entry size of "type"
	V4W_TIME time;
	V4W_GPS gps;
	BYTE type;	// type of entry ie: image, event, gforce or audio
	BYTE source;	// source camera of captured image
	BYTE trigger;
	BYTE esn[V4W_ESN_LENGTH];
	BYTE unit_id[V4W_STRING_LENGTH];
    BYTE image_swap;
    BYTE image_rotate[7];
	V4W_HEAD_HEADER head;
	BYTE driver_id[8];
	BYTE rtc_status;
	WORD img_num;	
	BYTE trig_state[6];
	V4W_GPS_STATUS gps_status;	
// N.B.: size of reserved field must be set so that FILESYS_HEADER is
//       128 bytes in size
	BYTE image_skipped;
	BYTE reserved[10];
	BYTE checksum;
} V4W_FILESYS_HEADER;

typedef struct {
	V4W_FILESYS_HEADER header;
	union {
		V4W_IMAGE image;
		V4W_AUDIO audio;
		V4W_GFORCE gforce;
		V4W_EVENT event;
		V4W_MARKER marker;
	} _data;
} V4W_FILESYS_ENTRY;

//********************************
//     Diagnostic error codes
//********************************

#define V4W_DIAG_ERROR_CODE_OFFSET					0x04E100

typedef struct {
	BYTE init;
	DWORD flags;
	// if we add any more elements, 
	// we need to also modify the asm file as well
} V4W_DIAG_ERROR_CODE;

// initialization errors
// there can only be one INIT error at a time
#define V4W_DIAG_ERROR_INIT_NONE					0x00
#define V4W_DIAG_ERROR_INIT_FPGA_NOT_PGMD			0x10
#define V4W_DIAG_ERROR_INIT_IDE_DRIVE_MISSING		0x20
#define V4W_DIAG_ERROR_INIT_IDE_DRIVE_NOT_READY		0x21
#define V4W_DIAG_ERROR_INIT_IDE_NIEN				0x22
#define V4W_DIAG_ERROR_INIT_IDE_IDENTIFY1			0x23
#define V4W_DIAG_ERROR_INIT_IDE_IDENTIFY2			0x24
#define V4W_DIAG_ERROR_INIT_IDE_READBACK			0x25
#define V4W_DIAG_ERROR_INIT_IDE_PWR_ON_SEQ			0x26
#define V4W_DIAG_ERROR_INIT_IDE_SPINUP				0x27
#define V4W_DIAG_ERROR_INIT_IDE_SIGNATURE			0x28
#define V4W_DIAG_ERROR_INIT_FRAM_TEST_FAILED		0x30
#define V4W_DIAG_ERROR_INIT_CONFIG_INVALID			0x40
#define V4W_DIAG_ERROR_INIT_WSM_TEST_FAILED			0x50
#define V4W_DIAG_ERROR_INIT_WSM_CONFIG_INVALID		0x51
#define V4W_DIAG_ERROR_INIT_HEAD_COMM				0x60  // removed in main fw v3.0.0 and later
#define V4W_DIAG_ERROR_INIT_RTC_TIME				0x70
#define V4W_DIAG_ERROR_INIT_RTC_BATTERY				0x71
#define V4W_DIAG_ERROR_INIT_RTC_MODEL    			0x72
#define V4W_DIAG_ERROR_INIT_VIDEO_ADC				0x80
#define V4W_DIAG_ERROR_INIT_FILESYS_CONFIG			0x90
#define V4W_DIAG_ERROR_INIT_THERMISTOR				0xA0
#define V4W_DIAG_ERROR_INIT_HEATER					0xA1

// error flags (runtime)
// there can be multiple ERROR FLAGs 
#define V4W_DIAG_ERROR_FLAG_CAMERA1					0x00000001
#define V4W_DIAG_ERROR_FLAG_CAMERA2					0x00000002
#define V4W_DIAG_ERROR_FLAG_CAMERA3					0x00000004
#define V4W_DIAG_ERROR_FLAG_CAMERA4					0x00000008
#define V4W_DIAG_ERROR_FLAG_CAMERA1_2				0x00000010
#define V4W_DIAG_ERROR_FLAG_CAMERA1_3				0x00000020
#define V4W_DIAG_ERROR_FLAG_CAMERA1_4				0x00000040
#define V4W_DIAG_ERROR_FLAG_LOG_POWER_FREQUENT		0x00000080
#define V4W_DIAG_ERROR_FLAG_RTC_TIME				0x00000100
#define V4W_DIAG_ERROR_FLAG_RTC_BATTERY				0x00000200
#define V4W_DIAG_ERROR_FLAG_IDE_MISSING				0x00000400
#define V4W_DIAG_ERROR_FLAG_IDE_ERROR_THRESHOLD 	0x00000800
#define V4W_DIAG_ERROR_FLAG_HEAD_COMM               0x00001000
#define V4W_DIAG_ERROR_FLAG_HEAD_ERROR              0x00002000
#define V4W_DIAG_ERROR_FLAG_RTC_STOPPED				0x00004000
#define V4W_DIAG_ERROR_FLAG_CAM_OVERCURRENT			0x00008000
#define V4W_DIAG_ERROR_FLAG_OVERHEAT				0x00010000
#define V4W_DIAG_ERROR_FLAG_EVENT_TIMED_OUT			0x00020000
#define V4W_DIAG_ERROR_FLAG_IDE_UNEXPECTED_DRQ		0x00040000
#define V4W_DIAG_ERROR_FLAG_GPS_ANTENNA				0x00080000
#define V4W_DIAG_ERROR_FLAG_TRIGGER_FAILURE			0x00100000
#define V4W_DIAG_ERROR_FLAG_IMAGE_QUALITY			0x00200000

//********************************
//     Diagnostic error codes		
//   (for versions prior to 1.3)
//********************************

#define V4W_DIAG_ERROR_RTC_TIME				0x01
#define V4W_DIAG_ERROR_RTC_BATTERY			0x02
#define V4W_DIAG_ERROR_IDE_TIMEOUT			0x04
#define V4W_DIAG_ERROR_IDE_ERROR			0x08

//*****************************************
//     System configuration structures
//*****************************************

#define V4W_IMAGE_ADC_SETTINGS_LENGTH			0x10

typedef struct {
	BYTE cameras;
	BYTE head_cameras;
	WORD delay;			// this is the delay associated with the frame rate
	BYTE adc_settings[V4W_IMAGE_ADC_SETTINGS_LENGTH];
	BYTE quant_table[2][V4W_IMAGE_WAVELET_BLOCKS];
} V4W_IMAGE_CONFIG;

typedef struct {
	WORD min_led_illum_time;
} V4W_IMAGE_CONFIG2;

#define V4W_IMAGE_SWAP_11_12   					0x01
#define V4W_IMAGE_SWAP_13_14   					0x02

#define V4W_IMAGE_ROTATE_0              		0x00
#define V4W_IMAGE_ROTATE_90            			0x01
#define V4W_IMAGE_ROTATE_180              		0x02
#define V4W_IMAGE_ROTATE_270           			0x03

typedef struct {
    BYTE swap;	// swap should always be set to 0
    BYTE rotate[7];
	BYTE use_dominant_cam;
	BYTE dominant_cam;
} V4W_IMAGE_CONFIG3;

#define V4W_EVENT_NUM_CYCLES					14
#define V4W_EVENT_NUM_TRIGGER_CYCLES			6
#define V4W_EVENT_NUM_GFORCE_CYCLES				8
#define V4W_EVENT_NUM_STAGES					3

#define V4W_EVENT_TRIG1							0x00
#define V4W_EVENT_TRIG2							0x01
#define V4W_EVENT_TRIG3							0x02
#define V4W_EVENT_TRIG4							0x03
#define V4W_EVENT_TRIG5							0x04
#define V4W_EVENT_TRIG6							0x05
#define V4W_EVENT_GFORCE_X_POS_STRONG			0x06
#define V4W_EVENT_GFORCE_X_POS_WEAK				0x07
#define V4W_EVENT_GFORCE_X_NEG_STRONG			0x08
#define V4W_EVENT_GFORCE_X_NEG_WEAK				0x09
#define V4W_EVENT_GFORCE_Y_POS_STRONG			0x0A
#define V4W_EVENT_GFORCE_Y_POS_WEAK				0x0B
#define V4W_EVENT_GFORCE_Y_NEG_STRONG			0x0C
#define V4W_EVENT_GFORCE_Y_NEG_WEAK				0x0D
#define V4W_EVENT_POWER							0x0E
#define V4W_EVENT_RESERVED						0x0F
#define V4W_EVENT_SPEED							0x10
#define V4W_EVENT_BACKGROUND					0x1F

#define V4W_EVENT_COPY_IMAGE					0x01
#define V4W_EVENT_COPY_AUDIO					0x02
#define V4W_EVENT_COPY_GFORCE					0x04

#define V4W_EVENT_CAMERAS_CAM1					0x01
#define V4W_EVENT_CAMERAS_CAM2					0x02
#define V4W_EVENT_CAMERAS_CAM3					0x04
#define V4W_EVENT_CAMERAS_CAM4					0x08
#define V4W_EVENT_CAMERAS_CAM1_2				0x10
#define V4W_EVENT_CAMERAS_CAM1_3				0x20
#define V4W_EVENT_CAMERAS_CAM1_4				0x40

#define V4W_EVENT_PRIORITY_INTERRUPT_EQUAL		0x80

typedef struct {
	BYTE copy;
	BYTE cameras;
	WORD num;
	WORD skip;
	WORD repeat;
} V4W_EVENT_STAGE;

typedef struct {
	BYTE priority;
	BYTE area;
	WORD jump;
	V4W_EVENT_STAGE stage[V4W_EVENT_NUM_STAGES];
} V4W_EVENT_CYCLE;

typedef struct {
	V4W_EVENT_STAGE background;
	V4W_EVENT_CYCLE cycle[V4W_EVENT_NUM_CYCLES];
	WORD shutdown_timeout;
} V4W_EVENT_CONFIG;

typedef struct {
	BYTE event_driven;
	BYTE reserved[3];
} V4W_EVENT_CONFIG2;

#define V4W_RTC_TYPE_INTERNAL			0
#define V4W_RTC_TYPE_EXTERNAL			1

#define V4W_GPS_TYPE_NONE					0
#define V4W_GPS_TYPE_RECORDER				1
#define V4W_GPS_TYPE_HEAD					2
#define V4W_GPS_TYPE_RECORDER_WITH_CHECK	3
#define V4W_GPS_TYPE_HEAD_WITH_CHECK		4

typedef struct {
	BYTE type;
	BYTE baudrate;
} V4W_GPS_CONFIG;

// only valid from v2.0.4 to 2.1.0, structure must always be set to zero
typedef struct {
	WORD threshold;
	WORD decrement_delta;
	WORD increment_delta;
	WORD reload_timer;
	WORD maximum_errors;
	BYTE reserved[1];
} V4W_IDE_ERROR_CONFIG;

typedef struct {
	BYTE error_times_per_day;
	BYTE error_persist_days;
	BYTE event_disabled;
	BYTE event_area;
} V4W_POWER_CONFIG;

typedef struct {
	BYTE awake_period;
	WORD sleep_period;
} V4W_WIFI_CONFIG;

//**************************************************
// Available protocols for auxiliary port or 
// external dual uart chip
//**************************************************

#define DUART_PROTOCOL_NONE 			0
#define DUART_PROTOCOL_MDT_MK 			1
#define DUART_PROTOCOL_GPS_NMEA 		2

typedef struct {
	BYTE baudrate[2];  		// refer to V4W_BAUDRATE for list of baudrate
	BYTE com_protocol[2];
} V4W_DUART;

#define V4W_FEATURES_DISABLED_AUDIO		0x01
#define V4W_FEATURES_DISABLED_GFORCE	0x02

#define V4W_RELAY_MODE_DISABLED			0x00
#define V4W_RELAY_MODE_GFORCE_WEAK		0x01
#define V4W_RELAY_MODE_GFORCE_STRONG	0x02
#define V4W_RELAY_MODE_HEARTBEAT		0x03

#define V4W_UNIT_TYPE_TAXICAM			0
#define V4W_UNIT_TYPE_STM				1

#define V4W_SYS_CONFIG_SIZE				512
#define V4W_SYS_CONFIG_LBA				0
#define V4W_SYS_CONFIG_LBA_OFFSET		1
#define V4W_SYS_CONFIG_INIT_CHAR		0x8E
#define V4W_SYS_CONFIG_VERSION			19

typedef struct {
	BYTE init;
	BYTE version;
	BYTE esn[V4W_ESN_LENGTH];
	BYTE auth_code[V4W_AUTH_CODE_LENGTH];
	V4W_IMAGE_CONFIG image;
	V4W_EVENT_CONFIG event;
	BYTE rtc_type;
	BYTE serial_baudrate;
	V4W_GPS_CONFIG gps;
	DWORD background_cancel_delay;
	V4W_IMAGE_CONFIG2 image2;
	V4W_EVENT_CONFIG2 event2;
	V4W_IDE_ERROR_CONFIG ide_error; // only valid from v2.0.4 to 2.1.0, should always be set to 0
	BYTE unit_type;   // only valid in v2.1.0, should always be set to 0
    V4W_IMAGE_CONFIG3 image3;	// swap parameter should always be set to 0
	V4W_POWER_CONFIG power;
	BYTE head_event_timeout_hr;
	V4W_WIFI_CONFIG wifi;
	V4W_DUART duart;
	BYTE features_disabled;
	BYTE relay_mode;
	BYTE triggers_monitored;
	BYTE image_ext_cam;			// same format as in V4W_EVENT_STAGE.cameras
	float moving_speed_thresh;	// in knots	
	BYTE iwv_error_timeout_hr;
} V4W_SYS_CONFIG;
// sizeof(V4W_SYS_CONFIG) must be below or equal to 512.

//***************************************
//     Card configuration structures
//***************************************

#define V4W_FILESYS_DATA_START_LBA		4
#define V4W_FILESYS_NUM_STORAGE_AREAS	4

typedef struct {
	DWORD start;
	DWORD end;
} V4W_FILESYS_BUFFER_CONFIG;

typedef struct {
	DWORD start;
	DWORD end;
	BYTE protect;
	DWORD unlock_timeout;
} V4W_FILESYS_STORAGE_AREA_CONFIG;

typedef struct {
	V4W_FILESYS_BUFFER_CONFIG buffer;
	V4W_FILESYS_STORAGE_AREA_CONFIG storage[V4W_FILESYS_NUM_STORAGE_AREAS];
} V4W_FILESYS_CONFIG;

typedef enum { c_violet, c_purple, c_blue, c_cyan, c_green, c_yellow, c_orange, c_red } V4W_LABEL_COLOR; 
typedef enum { s_circle, s_diamond, s_triangle, s_inverted_triangle } V4W_LABEL_SHAPE; 

// V4W_LABEL_CONFIG : Legend information used in software
// text			- an character array storing the name of the trigger
// color		- color of the trigger in the legend
// shape		- shape of the trigger in the legend
typedef struct {
	char text[14];
	BYTE color;
	BYTE shape;
} V4W_LABEL_CONFIG;

#define V4W_CARD_CONFIG_SIZE			512
#define V4W_CARD_CONFIG_LBA				1
#define V4W_CARD_CONFIG_LBA_OFFSET		0
#define V4W_CARD_CONFIG_INIT_CHAR		0xB7
#define V4W_CARD_CONFIG_VERSION			2

typedef struct {
	BYTE init;
	BYTE version;
	V4W_FILESYS_CONFIG filesys;
	BYTE reserved[2];
	V4W_LABEL_CONFIG trig[V4W_EVENT_NUM_TRIGGER_CYCLES];
} V4W_CARD_CONFIG;

//*********************************
//     System state structures
//*********************************

typedef struct {
	BYTE index;
	BYTE full;
} V4W_LOG_STATE;

typedef struct {
	struct {
		BYTE fpga;
		V4W_VERSION wsm;
		V4W_VERSION main;
		V4W_VERSION head;
	} version;
	BYTE unit_id[V4W_STRING_LENGTH];
	BYTE diag_error;
	V4W_LOG_STATE log;
} V4W_SYS_STATE;


//*********************************************
//	Image write verification state structure
//*********************************************

typedef struct{
	DWORD 	good_cnt;
	DWORD 	bad_cnt;
	WORD    consecutive_cnt;
	DWORD   ide_good_cnt;
	DWORD   ide_bad_cnt;
	DWORD	ide_timeout_cnt;
	DWORD   ide_drq_cnt;
	float   filter_output;
	BYTE	error_flagged;
	DWORD	seconds;
	BYTE	reserved[9];
} V4W_IWV_STATE;


//*******************************
//     Card state structures
//*******************************

typedef struct {
	BYTE model_num[V4W_IDE_ID_LENGTH];
	BYTE serial_num[V4W_IDE_ID_LENGTH];
	DWORD max_lba;
} V4W_IDE_ID;

#define V4W_FILESYS_STATUS_USE_BACKUP		0x01
#define V4W_FILESYS_STATUS_FULL				0x02

typedef struct {
	DWORD curr;
	DWORD prev_event;
	DWORD prev_data;
} V4W_FILESYS_PTR_STATE;

typedef struct {
	DWORD address;
	DWORD unlock_time;
	DWORD prev_event_time;
} V4W_FILESYS_PROT_STATE;

typedef struct {
	V4W_FILESYS_PTR_STATE ptr;
	V4W_FILESYS_PROT_STATE prot;
} V4W_FILESYS_STORAGE_STATE;

typedef struct {
	BYTE status;
	V4W_FILESYS_STORAGE_STATE orig;
	V4W_FILESYS_STORAGE_STATE backup;
} V4W_FILESYS_STORAGE_STATE_SET;

typedef struct {
	BYTE version;
	V4W_FILESYS_STORAGE_STATE_SET storage[V4W_FILESYS_NUM_STORAGE_AREAS];
} V4W_FILESYS_STATE;

typedef struct {
	DWORD curr;
	DWORD prev;
} V4W_FILESYS_BUFFER_PTR_STATE;

typedef struct {
	BYTE status;
	V4W_FILESYS_BUFFER_PTR_STATE orig;
	V4W_FILESYS_BUFFER_PTR_STATE backup;
} V4W_FILESYS_BUFFER_STATE_SET;

typedef struct {
	BYTE status;
	DWORD orig;
	DWORD backup;
} V4W_FILESYS_DATA_PTR_SET;

typedef struct {
	V4W_IDE_ID ide;
	V4W_FILESYS_STATE filesys;
	V4W_IWV_STATE iwv;
	V4W_FILESYS_BUFFER_STATE_SET buffer;
	V4W_FILESYS_DATA_PTR_SET prev_data[V4W_FILESYS_NUM_STORAGE_AREAS];
} V4W_CARD_STATE;

//******************************
//     Installer structures
//******************************

typedef struct {
#ifdef _AFXDLL
	CTime birthday;
	CTime installation;
#else
	BYTE birthday[4];
	BYTE installation[4];
#endif
	DWORD camera_id;
	BYTE installer_id[V4W_STRING_LENGTH];
	DWORD extra_camera_id[3];
	BYTE driver_id[8];
} V4W_CONTROLLER_INFO;

//*************************
//     FRAM structures
//*************************

#define V4W_FRAM_MAP_SIZE			512
#define V4W_FRAM_MAP_OFFSET			0
#define V4W_FRAM_MAP_LBA			2
#define V4W_FRAM_MAP_LBA_OFFSET		3
#define V4W_FRAM_MAP_INIT_CHAR		0xA9
#define V4W_FRAM_MAP_VERSION		8

typedef struct {
	BYTE init;
	BYTE version;
	V4W_SYS_STATE sys;
	V4W_CARD_STATE card;
        // N.B.: size of reserved field must be set so that controller starts at
        //       OFFSET 448. All previous data is located from offset 0 to 447 even though total size is 448.
	BYTE reserved[448-2-sizeof(V4W_SYS_STATE)-sizeof(V4W_CARD_STATE)]; // currently equals to 1 byte only
	V4W_CONTROLLER_INFO controller;
	BYTE reserved2[64-sizeof(V4W_CONTROLLER_INFO)];		// currently equals to 7 bytes only
} V4W_FRAM_MAP;

#define V4W_FRAM_MAP2_SIZE			512
#define V4W_FRAM_MAP2_OFFSET		512
#define V4W_FRAM_MAP2_LBA_OFFSET	4
#define V4W_FRAM_MAP2_INIT_CHAR		0xB9
#define V4W_FRAM_MAP2_VERSION		7

typedef struct {
	WORD trig[V4W_EVENT_NUM_TRIGGER_CYCLES];
	BYTE gforce[V4W_EVENT_NUM_GFORCE_CYCLES];
    V4W_TIME last_cleared;
} V4W_TRIGGER_COUNTER_STATE;

// V4W_FILESYS_CONFIG_COMPACT : compact version of V4W_FILESYS_CONFIG
// size_rt				- size of the real-time buffer in MB
// size_area			- size of each storage area in MB
// unlock_timeout_hr	- unlock timeout in hours
typedef struct {
	WORD size_rt;
	WORD size_area[V4W_FILESYS_NUM_STORAGE_AREAS];
	WORD unlock_timeout_hr[V4W_FILESYS_NUM_STORAGE_AREAS];
} V4W_FILESYS_CONFIG_COMPACT;

// V4W_FILESYS_CONFIG_COMPACT_DVR : compact version of V4W_FILESYS_CONFIG for DVR
// reserved				- reserved byte to maintain similar structure to the v4w
// size_area			- size of each storage area in MB
typedef struct {
	WORD reserved;
	DWORD size_area[V4W_FILESYS_NUM_STORAGE_AREAS];	
} V4W_FILESYS_CONFIG_COMPACT_DVR;

typedef struct {
	V4W_TIME weak_expire_time;
	V4W_TIME strong_expire_time;
	V4W_TIME unplugged_expire_time;
} V4W_HEAD_EVENT_STATE;

#define V4W_POWER_STATUS_USE_ORIG			0x00
#define V4W_POWER_STATUS_USE_BACKUP			0x01

typedef struct {
	BYTE last_available_status;
	V4W_TIME last_available_orig;
	V4W_TIME last_available_backup;
	BYTE reset_expected;
	V4W_TIME power_frequent_expire_time;
	BYTE reserved[3];
} V4W_POWER_STATE;

#define V4W_HW_REV_F      0x25
#define V4W_HW_REV_G      0x25
#define V4W_HW_REV_K      0x83

typedef struct {
	BYTE timed_out;
	DWORD reset_countdown_timer;
} V4W_EVENT_TIMER;

typedef struct {
	BYTE status;
	DWORD prev_marker_orig;
	DWORD prev_marker_backup;
} V4W_FILESYS_MARKER_STATE_SET;

typedef struct {
	V4W_FILESYS_MARKER_STATE_SET storage[V4W_FILESYS_NUM_STORAGE_AREAS];
} V4W_FILESYS_STATE2;

#define V4W_CAPTURE_STATE_RECORDING		0x01
#define V4W_CAPTURE_STATE_STOP_CAPTURE	0x02

typedef struct {
	float threshold;		// in knots
	BYTE timeout;			// in seconds
	V4W_EVENT_CYCLE cycle;
} V4W_SPEED_EVENT_CONFIG;

typedef struct {
	float max_speed;			// in knots
	V4W_TIME max_speed_time;
	char max_x_pos_gforce;
	V4W_TIME max_x_pos_time;
	char max_x_neg_gforce;
	V4W_TIME max_x_neg_time;
	char max_y_pos_gforce;
	V4W_TIME max_y_pos_time;
	char max_y_neg_gforce;
	V4W_TIME max_y_neg_time;
	V4W_TIME head_unplugged_time;
} V4W_MOTION_STATS;

typedef struct {
	BYTE area;
	WORD num;
} V4W_FILESYS_SEQ_LAST_IMG;

typedef struct {
	V4W_FILESYS_SEQ_LAST_IMG img;
	WORD img_rtb_num;
	WORD event_num;	
} V4W_FILESYS_SEQ_STATE;

typedef struct {
	BYTE status;
	V4W_FILESYS_SEQ_STATE orig;
	V4W_FILESYS_SEQ_STATE backup;
} V4W_FILESYS_SEQ_STATE_SET;

typedef struct {
	float filter_coef;
	float filter_coef_sum;
	float filter_threshold_multiple;
} V4W_IWV_FILTER_CONFIG;

typedef struct {
	WORD quadrant_size_limit;
	WORD total_size_limit;
	BYTE disable_camera_check;
} V4W_IMAGE_QUALITY;

typedef struct {
	BYTE pal_standard;
	BYTE blackwhite;
	BYTE image_full_resolution;
	BYTE fixed_image_field;
} V4W_FPGA_CONFIG;

typedef struct {
	BYTE init;
	BYTE version;
	BYTE init_test[2];
	union {
		V4W_FILESYS_CONFIG_COMPACT v4w;
		V4W_FILESYS_CONFIG_COMPACT_DVR dvr;
	} filesys;
	V4W_POWER_STATE power;
	V4W_LABEL_CONFIG trig[V4W_EVENT_NUM_TRIGGER_CYCLES];
	V4W_TRIGGER_COUNTER_STATE trig_count;
	V4W_HEAD_EVENT_STATE head_event;
	V4W_TIME gps_last_avail;
	BYTE ide_hw_reset_cnt;
	BYTE hw_rev;
	V4W_EVENT_TIMER event_timer;
	V4W_FILESYS_STATE2 filesys_state;
	BYTE capture_state;
	WORD trigger_timer[V4W_EVENT_NUM_TRIGGER_CYCLES];
	V4W_SPEED_EVENT_CONFIG speed_event;
	V4W_MOTION_STATS motion_stats;
	V4W_TIME gps_last_sync;
	V4W_FILESYS_SEQ_STATE_SET seq;	
	V4W_IWV_FILTER_CONFIG iwv_filter_config;
	BYTE sleep_when_no_activity;
	V4W_IMAGE_QUALITY image_quality;
	BYTE disable_orange_LED;
	V4W_FPGA_CONFIG fpga_config;
	BYTE maximum_frame_rate;
	WORD gps_minutes;
	BYTE reserved[126];
} V4W_FRAM_MAP2;

//***********************
//     Log constants
//***********************

#define V4W_LOG_SIZE			1024
#define V4W_LOG_ENTRIES			(V4W_LOG_SIZE/sizeof(V4W_TIME))
#define V4W_LOG_OFFSET			1024
#define V4W_LOG_LBA_OFFSET		5
#define V4W_SW_LOG_LBA_OFFSET	7

//**********************************
//     Diagnostic Log constants
//**********************************

#define V4W_DIAG_LOG_STATE_OFFSET		8192
#define V4W_DIAG_LOG_STATE_SIZE			16
#define V4W_DIAG_LOG_OFFSET				8208
#define V4W_DIAG_LOG_SIZE				8176

typedef struct {
	DWORD curr;
	BYTE full;
	BYTE reserved[11];
} V4W_DIAG_LOG_STATE;

#define V4W_DIAG_LOG_MODE_RECORDING		0x01
#define V4W_DIAG_LOG_MODE_POWERED_OFF	0x02
#define V4W_DIAG_LOG_MODE_FAULT			0x03
#define V4W_DIAG_LOG_MODE_NORMAL_SLEEP	0x04
#define V4W_DIAG_LOG_MODE_FORCE_SLEEP	0x05
#define V4W_DIAG_LOG_MODE_STOP_CAPTURE	0x06

typedef struct {
	BYTE mode;
	V4W_TIME time;
} V4W_DIAG_LOG_ENTRY;

typedef struct {
	V4W_DIAG_LOG_ENTRY entry;
	BYTE init;
	DWORD flags;
	BYTE reserved[3];	// the structure must be 8 bytes aligned
} V4W_DIAG_LOG_FAULT_ENTRY;

// FRAM MAP 3 is only available on V-4600 and V4W rev K+
#define V4W_FRAM_MAP3_SIZE			2048
#define V4W_FRAM_MAP3_OFFSET		16384
#define V4W_FRAM_MAP3_LBA_OFFSET	35
#define V4W_FRAM_MAP3_INIT_CHAR		0xAB
#define V4W_FRAM_MAP3_VERSION		1

typedef struct {
	char low[16];
	char medium[16];
	char high[16];
} V4W_LABEL_TRIGGER_STATES;

typedef struct {
	BYTE init;
	BYTE version;
	V4W_LABEL_TRIGGER_STATES trigger[V4W_EVENT_NUM_TRIGGER_CYCLES];
	BYTE reserved[1758];	// structure needs to add up to four sectors (2048 bytes)
} V4W_FRAM_MAP3;

#if ((defined (WIN32) || defined (linux)) && !defined (_LIB)) || defined (__ICCARM__)
#pragma pack(pop)
#endif

#endif // V4W_H

//********************************************************************
//		Copyright (c) VerifEye Technologies, All Rights Reserved
//********************************************************************
