//********************************************************************
//
//		VerifEye Technologies
//		7100 Warden Ave, Unit #3
//		Markham, Ontario, Canada, L3R 8B5
//
//		Project:		V4W Common Files
//		Filename:		v4whead.h
//		Author:			Jason Thomas
//						edited by Guy Gagnon
//						and Edmond Chan
//		Description:	Definition of smart head common structures,
//						constants, etc.
//
//********************************************************************

#ifndef V4WHEAD_H
#define V4WHEAD_H

#if ((defined (WIN32) || defined (linux)) && !defined (_LIB)) || defined (__ICCARM__)
#pragma pack(push, 1)
#endif

//***************************
//     Packet structures
//***************************

#define V4W_HEAD_PACKET_MAGIC_CHAR		  0x6C
#define V4W_HEAD_PACKET_MAX_DATA_LENGTH	  1024  

// READ commands (actual data is returned) should be from 0x01 to 0x6F //
// 0x00 cannot be used due to error response
#define V4W_HEAD_CMD_READ_I2C             0x01
#define V4W_HEAD_CMD_READ_ESN             0x02
#define V4W_HEAD_CMD_READ_FEATURES        0x03
#define V4W_HEAD_CMD_READ_BIRTHDAY        0x04
#define V4W_HEAD_CMD_READ_INSTALL_LOG     0x05
#define V4W_HEAD_CMD_READ_CONTROLLER_LOG  0x06
#define V4W_HEAD_CMD_READ_FRAM            0x07
#define V4W_HEAD_CMD_READ_AUDIO_STATUS    0x08
#define V4W_HEAD_CMD_READ_GFORCE_STATUS   0x09
#define V4W_HEAD_CMD_READ_GPS_STATUS      0x0A
#define V4W_HEAD_CMD_READ_AUDIO_DATA      0x0B
#define V4W_HEAD_CMD_READ_GFORCE_DATA     0x0C
#define V4W_HEAD_CMD_READ_GPS_DATA        0x0D
#define V4W_HEAD_CMD_READ_CONFIG          0x0E
#define V4W_HEAD_CMD_READ_PARTIAL_CONFIG  0x0F
#define V4W_HEAD_CMD_READ_STATE           0x10
#define V4W_HEAD_CMD_READ_PARTIAL_STATE   0x11       
#define V4W_HEAD_CMD_READ_GPS_GGA_DATA    0x12

// WRITE commands (default response is returned) should be from 0x70 to 0xEF //
#define V4W_HEAD_CMD_RESET                    0x70
#define V4W_HEAD_CMD_CAM_RST                  0x71
#define V4W_HEAD_CMD_FACTORY_DEFAULTS         0x72
#define V4W_HEAD_CMD_WRITE_I2C                0x73
#define V4W_HEAD_CMD_WRITE_CONFIG             0x74
#define V4W_HEAD_CMD_SELECT_CURR_CAM          0x75
#define V4W_HEAD_CMD_SET_EVENT_TRIGGERED      0x76
#define V4W_HEAD_CMD_SET_DATA_CAPTURE         0x77
#define V4W_HEAD_CMD_SET_LED_IR_CTRL          0x78
#define V4W_HEAD_CMD_SET_STATUS_LEDS          0x79
#define V4W_HEAD_CMD_TURN_BUZZER_ON           0x7A
#define V4W_HEAD_CMD_LOG_INSTALL              0x7B
#define V4W_HEAD_CMD_CLEAR_INSTALL_LOG        0x7C
#define V4W_HEAD_CMD_LOG_CONTROLLER           0x7D
#define V4W_HEAD_CMD_CLEAR_CONTROLLER_LOG     0x7E
#define V4W_HEAD_CMD_DETERMINE_ORIENTATION    0x7F

// packet header should be followed by data of length specified then 
// one byte of checksum which is computed with cmd, length and data
typedef struct {
	BYTE magic;
        BYTE cmd;     
        BYTE length_msb;
        BYTE length_lsb;
} V4W_HEAD_PACKET_HEADER;

//**************************
//     State structures
//**************************

#define V4W_HEAD_STATE_SIZE		        128
#define V4W_HEAD_STATE_VERSION		        2

// down_dir           - specifies which is the detected down direction. see V4W_HEAD_DIR
// right_dir          - the positive x-axis computed from fwd_dir and down_dir. see V4W_HEAD_DIR
typedef struct {
        BYTE down_dir;
        BYTE right_dir;
        BYTE reserved[6];
} V4W_HEAD_GFORCE_STATE;

#define V4W_HEAD_GPS_STATUS_AVAIL	        	0x01
#define V4W_HEAD_GPS_STATUS_VALID               0x02
#define V4W_HEAD_GPS_STATUS_VALID_LAST_HR       0x04
#define V4W_HEAD_GPS_STATUS_GGA_AVAIL	        0x08
#define V4W_HEAD_GPS_STATUS_GGA_VALID           0x10

// gforce, audio      - bitmask specifying blocks ready to be transferred to main MCU
// gps                - indicating avail, valid and valid last hour
typedef struct {
        DWORD audio;
        DWORD gforce;
        DWORD gps;
} V4W_HEAD_DATA_STATUS;

#define V4W_HEAD_CAPTURE_AUDIO		        0x01
#define V4W_HEAD_CAPTURE_GFORCE		        0x02

// capture            - specifies which data unit is set to capture
typedef struct {
        BYTE capture;
        BYTE reserved[3];
        V4W_HEAD_DATA_STATUS status;
} V4W_HEAD_DATA;

#define V4W_HEAD_MAX_CAMERA_CONNECTED           0x04

// curr               - current video channel selected
// connected          - bitmask specifying which camera is connected
typedef struct {
        BYTE curr;
        BYTE connected;
        BYTE reserved[6];
} V4W_HEAD_CAMERA_STATE;

#define V4W_HEAD_ERROR_INVALID_ESN              0x0001
#define V4W_HEAD_ERROR_CONTROLLER_MISMATCH      0x0002
#define V4W_HEAD_ERROR_BIRTHDAY_NOT_SET         0x0004
#define V4W_HEAD_ERROR_CONFIG                   0x0008
#define V4W_HEAD_ERROR_CONFIG_HEADER            0x0010
#define V4W_HEAD_ERROR_CONFIG_ORIENTATION       0x0020
#define V4W_HEAD_ERROR_NO_CAM                   0x0040
#define V4W_HEAD_ERROR_FRAM_TEST                0x0080
#define V4W_HEAD_ERROR_IR_LED                	0x0100	// to be implemented
#define V4W_HEAD_ERROR_NO_MIC                   0x0200

#define V4W_HEAD_EVENT_TRIGGERED_WEAK	        0x01
#define V4W_HEAD_EVENT_TRIGGERED_STRONG	        0x02
#define V4W_HEAD_EVENT_TRIGGERED_UNPLUGGED      0x04

// version            - version of state map
// fw[]               - firmware string label
// error              - system error log
// led_ir             - 0:off  1:on
// event_triggered    - indicating what type of event was triggered
typedef struct {
	BYTE version;
	BYTE fw[V4W_STRING_LENGTH];
        BYTE reserved[6];
        DWORD error;
        BYTE reserved2[4];
        V4W_HEAD_GFORCE_STATE gforce;
        V4W_HEAD_DATA data;
        V4W_HEAD_CAMERA_STATE camera;
        BYTE led_ir; 
        BYTE event_triggered;
} V4W_HEAD_STATE;

//**********************************
//     Configuration structures
//**********************************

#define V4W_HEAD_GFORCE_NUM_FILTER_COEFFS              2

// offset               - in samples, must be between 10 to 50
// duration             - in ms, must be between 1 to 10000
typedef struct {
    WORD offset;
	WORD duration;
} V4W_HEAD_GFORCE_THRESHOLD;

// strong.offset must be > weak.offset
typedef struct {
	V4W_HEAD_GFORCE_THRESHOLD strong;   
	V4W_HEAD_GFORCE_THRESHOLD weak;
} V4W_HEAD_GFORCE_THRESHOLDS;

// pos                  - x: right, y: acceleration
// neg                  - x: left, y: braking
typedef struct {
	V4W_HEAD_GFORCE_THRESHOLDS pos;   
	V4W_HEAD_GFORCE_THRESHOLDS neg;   
} V4W_HEAD_GFORCE_THRESHOLD_SET;

// x                    - lateral
// y                    - longtitudinal
typedef struct {
	V4W_HEAD_GFORCE_THRESHOLD_SET x; 
	V4W_HEAD_GFORCE_THRESHOLD_SET y; 
} V4W_HEAD_GFORCE_THRESHOLD_SETTINGS;

#define V4W_HEAD_DIR_BACK                               1
#define V4W_HEAD_DIR_FRONT                              2
#define V4W_HEAD_DIR_RIGHT                              3
#define V4W_HEAD_DIR_LEFT                               4
#define V4W_HEAD_DIR_TOP                                5
#define V4W_HEAD_DIR_BOTTOM                             6

#define DVR_HEAD_DIR_RIGHT                              1
#define DVR_HEAD_DIR_LEFT                              	2
#define DVR_HEAD_DIR_BACK                             	3
#define DVR_HEAD_DIR_FRONT	                            4
#define DVR_HEAD_DIR_BOTTOM                             5
#define DVR_HEAD_DIR_TOP	                            6

#define V4W_HEAD_GFORCE_BUZZER_CTRL_OFF                 0
#define V4W_HEAD_GFORCE_BUZZER_CTRL_WEAK                1
#define V4W_HEAD_GFORCE_BUZZER_CTRL_STRONG              2

// sample_filter_coeff  - must add up to 256
// bias_filter_coeff_x  - must add up to 65536
// bias_filter_coeff_y  - must add up to 65536
// fwd_dir				- specifies the positive y-axis
// right_dir			- specifies the positive x-axis. 
//						should be set to 0 and use calibration to set it
// output_raw_values    - outputs are raw sample values minus a fixed bias of 128
// buzzer_ctrl          - set to OFF, WEAK or STRONG
typedef struct {
	V4W_HEAD_GFORCE_THRESHOLD_SETTINGS thresh;
	float sample_filter_coeff[V4W_HEAD_GFORCE_NUM_FILTER_COEFFS];   
    float bias_filter_coeff_x[V4W_HEAD_GFORCE_NUM_FILTER_COEFFS];   
	float bias_filter_coeff_y[V4W_HEAD_GFORCE_NUM_FILTER_COEFFS];
    BYTE fwd_dir;
    BYTE right_dir;
    BYTE output_raw_values;         
	BYTE buzzer_ctrl;
    BYTE reserved[4];
} V4W_HEAD_GFORCE_CONFIG;

#define V4W_HEAD_CAMERA_FORMAT_PAL			0
#define V4W_HEAD_CAMERA_FORMAT_NTSC			1

#define V4W_HEAD_CAMERA_COLOR_MONOCHROME                0
#define V4W_HEAD_CAMERA_COLOR_COLOR                     1

#define V4W_HEAD_CAMERA_IR_LED_CTRL_AUTO    	        0
#define V4W_HEAD_CAMERA_IR_LED_CTRL_OFF    	        1
// This setting cannot be set in config, can only be used under SET_LED_IR_CTRL command
#define V4W_HEAD_CAMERA_IR_LED_CTRL_ON_5MIN    	        2

#define V4W_HEAD_CAMERA_MATCH_MAIN_CAMS_OFF              0
#define V4W_HEAD_CAMERA_MATCH_MAIN_CAMS_0           	1
#define V4W_HEAD_CAMERA_MATCH_MAIN_CAMS_1           	2

#define V4W_HEAD_CAMERA_MATCH_AUX_CAMS_OFF               0
#define V4W_HEAD_CAMERA_MATCH_AUX_CAMS_2           	1
#define V4W_HEAD_CAMERA_MATCH_AUX_CAMS_3           	2

#define V4W_HEAD_CAMERA_FLIP_CAM0                       0x01
#define V4W_HEAD_CAMERA_FLIP_CAM1                       0x02
#define V4W_HEAD_CAMERA_FLIP_CAM2                       0x04
#define V4W_HEAD_CAMERA_FLIP_CAM3                       0x08

typedef struct {
	BYTE format;
    BYTE color;
    BYTE ir_led_ctrl;  
    BYTE match_main_cams;
    BYTE match_aux_cams;
    BYTE flip;
    BYTE reserved[2];
    WORD gamma[5];
    BYTE expo_stability;
    BYTE expo_target;
    BYTE gain_upper_limit;
    BYTE gain_lower_limit;
} V4W_HEAD_CAMERA_CONFIG;

#define V4W_HEAD_CONFIG_SIZE			128
#define V4W_HEAD_CONFIG_INIT_VALUE		0xD32C3DC2
#define V4W_HEAD_CONFIG_VERSION			6

// status_leds_disable	- if set, leds are off unless error
typedef struct {
	DWORD init;
	DWORD version;
    BYTE gps_baudrate;
    BYTE status_leds_disable;     
    BYTE reserved[6];
    V4W_HEAD_GFORCE_CONFIG gforce;
    V4W_HEAD_CAMERA_CONFIG camera;
} V4W_HEAD_CONFIG;

//********************************
//     Address map structures
//********************************

#define V4W_HEAD_MAP_SIZE		256
#define V4W_HEAD_MAP_LBA		3
#define V4W_HEAD_MAP_LBA_OFFSET	2
#define V4W_HEAD_MAP_OFFSET		256

typedef struct {
	V4W_HEAD_STATE state;
// N.B.: size of reserved1 field must be set so that config starts at
//       offset 128
	BYTE reserved1[V4W_HEAD_STATE_SIZE-sizeof(V4W_HEAD_STATE)];
	V4W_HEAD_CONFIG config;
// N.B.: size of reserved2 field must be set so that config_save is
//       located at offset 255
        BYTE reserved2[V4W_HEAD_CONFIG_SIZE-sizeof(V4W_HEAD_CONFIG)];
} V4W_HEAD_MAP;

// This macro should only be used to determine addresses for
// reading/writing from the head.  There is no V4W_HEAD variable in RAM!
#define V4W_HEAD	((V4W_HEAD_MAP*)0)


//********************************
//     History log structures
//********************************

#define V4W_HEAD_LOG_ENTRIES    10

// aligned to 32bytes
typedef struct {
        BYTE id[19];
        BYTE esn[V4W_ESN_LENGTH];
        V4W_TIME time;
} V4W_HEAD_LOG_ENTRY;     

typedef struct {
        BYTE last_entry_index;
        BYTE reserved[15];
        V4W_HEAD_LOG_ENTRY entry[V4W_HEAD_LOG_ENTRIES];
} V4W_HEAD_LOG;

typedef struct {
		V4W_HEAD_LOG install_log;
        V4W_HEAD_LOG controller_log;
} V4W_HEAD_HISTORY_LOG;

//************************************
//     Factory info structures
//************************************

#define V4W_HEAD_FRAM_CONFIG_OFFSET				0
#define V4W_HEAD_FRAM_LOG_OFFSET				1024
#define V4W_HEAD_FRAM_FACTORY_INFO_OFFSET		2048
#define V4W_HEAD_FRAM_TEST_OFFSET               8188

typedef struct {
    V4W_HEAD_ESN esn;
    V4W_TIME birthday;
} V4W_HEAD_FACTORY_INFO;

#if ((defined (WIN32) || defined (linux)) && !defined (_LIB)) || defined (__ICCARM__)
#pragma pack(pop)
#endif

#endif // V4WHEAD_H

//********************************************************************
//		Copyright (c) VerifEye Technologies, All Rights Reserved
//********************************************************************
