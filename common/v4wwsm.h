//********************************************************************
//
//		VerifEye Technologies
//		7100 Warden Ave, Unit #3
//		Markham, Ontario, Canada, L3R 8B5
//
//		Project:		V4W Common Files
//		Filename:		v4wwsm.h
//		Author:			Jason Thomas
//						edited by Guy Gagnon
//						and Edmond Chan
//		Description:	Definition of WSM common structures,
//						constants, etc.
//
//********************************************************************

#ifndef V4WWSM_H
#define V4WWSM_H

#if ((defined (WIN32) || defined (linux)) && !defined (_LIB)) || defined (__ICCARM__)
#pragma pack(push, 1)
#endif

#define V4W_WSM_SLAVE_ADDRESS			0xE4

//**************************
//     State structures
//**************************

#define V4W_WSM_INTR_PING				0x80
#define V4W_WSM_INTR_TRIGGER_CHANGE		0x01

typedef struct {
	BYTE enable;
	BYTE status;
} V4W_WSM_INTERRUPT_STATE;

#define V4W_WSM_NUM_TRIGGERS			8
#define V4W_WSM_TRIGGER1				0x01
#define V4W_WSM_TRIGGER2				0x02
#define V4W_WSM_TRIGGER3				0x04
#define V4W_WSM_TRIGGER4				0x08
#define V4W_WSM_TRIGGER5				0x10
#define V4W_WSM_TRIGGER6				0x20
#define V4W_WSM_TRIGGER7				0x40
#define V4W_WSM_TRIGGER8				0x80

typedef struct {
	BYTE active;
	BYTE active_latch; 	// used only with software to detect triggers, only software clears this variable
	BYTE change;		// state change
	BYTE state[V4W_WSM_NUM_TRIGGERS];
	BYTE value[V4W_WSM_NUM_TRIGGERS];
} V4W_WSM_TRIGGER_STATE;

typedef struct {
	BYTE active_change;
	BYTE transmit_active_latch;	// this is the accumulation of asserted state
} V4W_WSM_TRIGGER_STATE2;

#define V4W_WSM_LED_COLOUR_NONE			0
#define V4W_WSM_LED_COLOUR_GREEN		1
#define V4W_WSM_LED_COLOUR_RED			2
#define V4W_WSM_LED_COLOUR_ORANGE		3
#define V4W_WSM_LED_NUM_STAGES			4
#define V4W_WSM_LED_UPDATE_FREQUENCY	50
#define V4W_WSM_LED_DURATION_INFINITE	0xFFFF
#define V4W_WSM_NUM_LED_STATES			2
#define V4W_WSM_LED_STATE_AWAKE			0
#define V4W_WSM_LED_STATE_ASLEEP		1

typedef struct {
	BYTE colour;
	WORD duration;
} V4W_WSM_LED_STAGE;

typedef struct {
	BYTE stage;
	BYTE colour;
	WORD count;
	V4W_WSM_LED_STAGE stages[V4W_WSM_LED_NUM_STAGES];
} V4W_WSM_LED_STATE;

#define V4W_WSM_POWER_CTRL_FORCE_POWER_OFF		0x01
#define V4W_WSM_POWER_CTRL_RESET				0x02
#define V4W_WSM_POWER_CTRL_ALLOW_POWER_OFF		0x04
#define V4W_WSM_POWER_STATUS_POR				0x01
#define V4W_WSM_POWER_STATUS_MOMENTARY_ON		0x02

typedef struct {
	BYTE control;
	BYTE status;
} V4W_WSM_POWER_STATE;

typedef struct {
	BYTE timer_update;
	WORD timer;
	BYTE turned_off;
} V4W_WSM_WIFI;

#define V4W_WSM_ERROR_THERMISTOR_FAILURE		0x01
#define V4W_WSM_ERROR_HEATER_FAILURE			0x02
#define V4W_WSM_ERROR_CAM_OVERCURRENT			0x04
#define V4W_WSM_ERROR_CONFIG_INVALID			0x08

#define V4W_WSM_FORCE_HEATER_ON_10MIN			0x01
#define V4W_WSM_FORCE_HEATER_OFF				0x02

#define V4W_WSM_STATE_SIZE			128
#define V4W_WSM_STATE_VERSION		7

typedef struct {
	BYTE version;
	BYTE fw[V4W_STRING_LENGTH];
	V4W_WSM_INTERRUPT_STATE intr;
	V4W_WSM_TRIGGER_STATE trigger;
	V4W_WSM_LED_STATE led[V4W_WSM_NUM_LED_STATES];
	V4W_WSM_POWER_STATE power;
	V4W_WSM_TRIGGER_STATE2 trigger2;
	V4W_WSM_WIFI wifi;
	BYTE error;
	BYTE force_heater;	
	BYTE drive_heated;
	BYTE download_mode;
	BYTE reserved[37];	// must be at least 16bytes for wsm_test() in main mcu
} V4W_WSM_STATE;

//**********************************
//     Configuration structures
//**********************************

typedef struct {
	BYTE low[V4W_WSM_NUM_TRIGGERS];
	BYTE high[V4W_WSM_NUM_TRIGGERS];
} V4W_WSM_TRIGGER_THRESH;

typedef struct {
	BYTE low;
	BYTE medium;
	BYTE high;
} V4W_WSM_TRIGGER_ACTIVE;

typedef struct {
	WORD reload_value[V4W_WSM_NUM_TRIGGERS];
} V4W_WSM_TRIGGER_SAMPLE_NUM;

typedef struct {
	BYTE thresh;
	BYTE sample_num_reload_value;
} V4W_WSM_TRIGGER_CUR_MON;

typedef struct {
	V4W_WSM_TRIGGER_THRESH thresh;
	V4W_WSM_TRIGGER_ACTIVE active;
	V4W_WSM_TRIGGER_SAMPLE_NUM sample_num;
	V4W_WSM_TRIGGER_CUR_MON cur_mon;
} V4W_WSM_TRIGGER_CONFIG;

#define V4W_WSM_CONFIG_SIZE			128		// maximum size is actually 127 due to config_save byte
#define V4W_WSM_CONFIG_INIT_CHAR	0xC2
#define V4W_WSM_CONFIG_VERSION		7

typedef struct {
	BYTE init;
	BYTE version;
	V4W_WSM_TRIGGER_CONFIG trigger;
	BYTE wifi_enable;
	BYTE drive_min_temp;
	BYTE drive_overheat_temp;
	BYTE cam_max_current;
	BYTE reserved[84];
} V4W_WSM_CONFIG;

//********************************
//     Address map structures
//********************************

#define V4W_WSM_MAP_SIZE		256
#define V4W_WSM_MAP_LBA			3
#define V4W_WSM_MAP_LBA_OFFSET	2
#define V4W_WSM_MAP_OFFSET		0

typedef struct {
	V4W_WSM_STATE state;
// N.B.: config must start at offset 128
	V4W_WSM_CONFIG config;
// N.B.: config_save must be located at offset 255
	BYTE config_save;
} V4W_WSM_MAP;

// This macro should only be used to determine addresses for
// reading/writing from the WSM
#define V4W_WSM	((V4W_WSM_MAP*)0)

#if ((defined (WIN32) || defined (linux)) && !defined (_LIB)) || defined (__ICCARM__)
#pragma pack(pop)
#endif

#endif // V4WWSM_H

//********************************************************************
//		Copyright (c) VerifEye Technologies, All Rights Reserved
//********************************************************************
