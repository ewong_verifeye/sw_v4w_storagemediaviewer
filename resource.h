//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by cfviewer.rc
//
#define IDD_CFVIEWER_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDB_PLAY                        129
#define IDB_REWIND                      130
#define IDB_PAUSE                       131
#define IDD_READER_DIALOG               131
#define IDB_NEXT                        132
#define IDD_JUMP_DIALOG                 132
#define IDB_PREV                        133
#define IDD_SECTOR_DIALOG               133
#define IDD_SEARCH_DIALOG               134
#define IDD_EXTRACT_DIALOG              137
#define IDD_DIALOG1                     138
#define IDD_SEARCH_DIALOG_V1            139
#define IDC_CURSOR1                     140
#define IDI_ICON_VERIFEYE               142
#define IDC_IMAGE0                      1000
#define IDC_IMAGE1                      1001
#define IDC_INFO0                       1002
#define IDC_INFO1                       1003
#define IDC_SOURCE1                     1004
#define IDC_SOURCE2                     1005
#define IDC_FILENAME                    1006
#define IDC_BROWSE                      1007
#define IDC_OPEN                        1008
#define IDC_EVENTS                      1009
#define IDC_SEARCH_EVENTS               1010
#define IDC_PLAY_MODE1                  1011
#define IDC_PLAY_MODE2                  1012
#define IDC_PREV                        1013
#define IDC_REWIND                      1014
#define IDC_PAUSE                       1015
#define IDC_PLAY                        1016
#define IDC_NEXT                        1017
#define IDC_STATUS                      1018
#define IDC_JUMP                        1019
#define IDC_DEVICE                      1020
#define IDC_VIEW_SECTOR                 1020
#define IDC_ADDRESS                     1021
#define IDC_EXTRACT_EVENT               1021
#define IDC_GOTO                        1022
#define IDC_IMAGE2                      1022
#define IDC_LBA                         1023
#define IDC_IMAGE3                      1023
#define IDC_LAST                        1024
#define IDC_DIRECTION1                  1025
#define IDC_INFO2                       1025
#define IDC_DIRECTION2                  1026
#define IDC_TEXT                        1026
#define IDC_INFO3                       1026
#define IDC_SECTOR                      1031
#define IDC_TYPE1                       1034
#define IDC_TYPE2                       1035
#define IDC_FOLDERS                     1036
#define IDC_TYPE3                       1038
#define IDC_FILTER                      1039
#define IDC_STATUS2                     1040
#define IDC_GRAPH                       1041
#define IDC_TYPE4                       1042
#define IDC_TYPE5                       1043
#define IDC_SLIDER                      1044
#define IDC_COMBO_YUV                   1046
#define IDC_COMBO_DISPLAY_OPTIONS       1048
#define IDC_PROGRESS1                   1049
#define IDC_PROGRESS_SEARCH             1049
#define IDSEARCH_STARTSTOP              1050
#define IDC_SECTOR_STATUS               1051
#define IDC_SECTOR_STATUS2              1052
#define IDC_SECTOR_INFO                 1053

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        147
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1055
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
