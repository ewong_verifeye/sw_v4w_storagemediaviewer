// wavelet.h : main header file for the WAVELET DLL
//

#if !defined(AFX_WAVELET_H__932E2F9B_EB48_4363_A94A_E6453203CFE9__INCLUDED_)
#define AFX_WAVELET_H__932E2F9B_EB48_4363_A94A_E6453203CFE9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CWaveletApp
// See wavelet.cpp for the implementation of this class
//

class CWaveletApp : public CWinApp
{
public:
	CWaveletApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWaveletApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CWaveletApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WAVELET_H__932E2F9B_EB48_4363_A94A_E6453203CFE9__INCLUDED_)
